<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::group(['middleware' => 'auth'], function () {
    Route::get('/home', function() {
        return redirect('/transaction');
    })->name('home');
    
    Route::resource('/user', 'UserController')->middleware(['permission:manage_user']);
    Route::post('/user/activation', 'UserController@activation')->middleware(['permission:manage_user']);
    Route::get('/user/list-user', 'UserController@listUser');
    
    Route::resource('/frame', 'FrameController')->middleware(['permission:manage_frame']);
    Route::get('/frame-by-merk/{id}/{team_id?}', 'FrameController@getByMerk')->middleware(['permission:transaction_show']);
    Route::get('/frame-download', 'FrameController@download')->name('frame.download');
    
    Route::resource('/merk-frame', 'MerkFrameController')->middleware(['permission:manage_frame']);
    Route::resource('/transaction', 'TransactionController')->middleware(['permission:transaction_show']);
    Route::get('/transaction/create/{other}', 'TransactionController@create')->middleware(['permission:transaction_show']);
    
    Route::post('/transaction-request-download', 'TransactionController@downloadRequest')->middleware(['permission:transaction_download']);
    Route::get('/transaction-download', 'TransactionController@download')->middleware(['permission:transaction_download']);
    
    Route::get('/deposit/bill-list', 'DepositController@billList')->middleware(['permission:main_deposit']);
    Route::get('/deposit/form-edit-bill/{id}', 'DepositController@formBill')->middleware(['permission:main_deposit']);
    Route::put('/deposit/update-bill/{id}', 'DepositController@updateBill')->middleware(['permission:main_deposit']);
    Route::post('/deposit/process', 'DepositController@depositProcess')->middleware(['permission:main_deposit']);
    
    Route::get('/deposit/payment-list', 'DepositController@paymentList')->middleware(['permission:manage_deposit']);
    Route::get('/deposit/payment-list/cetak_pdf', 'DepositController@cetak_pdf');
    Route::post('/deposit/payment-form', 'DepositController@paymentForm')->middleware(['permission:manage_deposit']);
    Route::post('/deposit/get-payment/{id}', 'DepositController@getPayment')->middleware(['permission:manage_deposit']);
    Route::post('/deposit/get-transaction/{id}', 'DepositController@getTransaction')->middleware(['permission:manage_deposit']);
    Route::post('/deposit/add-payment', 'DepositController@addPayment')->middleware(['permission:manage_deposit']);
    Route::delete('/deposit/delete-payment/{id}', 'DepositController@deletePayment')->middleware(['permission:manage_deposit']);
    Route::get('/deposit/payment-list/create', 'DepositController@createIncome')->name('deposit.create-income');
    Route::post('/deposit/payment-list/input', 'DepositController@inputIncome')->name('deposit.input-income');

    Route::get('/omset/omset-report', 'omsetController@omsetReport')->name('omset.omset-report');
    Route::get('/omset/omset-report/create', 'omsetController@createOmset')->name('omset.createomset');
    Route::post('/omset/omset-report/input', 'omsetController@inputOmset')->name('omset.inputomset');
    Route::get('/omset/omset-report-download', 'omsetController@omsetDownload')->name('omset.downloadomset');
    Route::delete('/omset/delete-omset/{id}', 'omsetController@deleteOmset')->name('omset.deleteomset');
    Route::get('/omset/omset-report/edit/{id}', 'omsetController@editOmset')->name('omset.edit-omset');
    Route::post('/omset/omset-report/update/{id}', 'omsetController@updateOmset')->name('omset.update-omset');



    Route::get('/financial/financial-report', 'financialController@financialReport')->name('financial.financial-report');
    Route::get('/financial/financial-report/cetak_pdf', 'financialController@cetak_pdf');
    Route::get('/financial/income-report', 'financialController@incomeReport')->name('financial.income-report');
    Route::get('/financial/income-report/edit/{id}', 'financialController@editIncome')->name('financial.edit-income');
    Route::post('/financial/income-report/update/{id}', 'financialController@updateIncome')->name('financial.update-income');
    Route::get('/financial/income-report/create', 'financialController@createIncome')->name('financial.create-income');
    Route::post('/financial/income-report/input', 'financialController@inputIncome')->name('financial.input-income');
    Route::get('/financial/out-report', 'financialController@outReport')->name('financial.out-report');
    Route::get('/financial/out-report/edit/{id}', 'financialController@editoutReport')->name('financial.edit-out-report');
    Route::post('/financial/out-report/update/{id}', 'financialController@updateoutReport')->name('financial.update-out-report');
    Route::get('/financial/out-report/create', 'financialController@createOutcome')->name('financial.createoutcome');
    Route::post('/financial/out-report/input', 'financialController@inputOutcome')->name('financial.inputoutcome');
    Route::delete('/financial/delete-income/{id}', 'financialController@deleteIncome')->name('financial.deleteincome');
    Route::delete('/financial/delete-outcome/{id}', 'financialController@deleteOutcome')->name('financial.deleteoutcome');
    // Route::get('/financial/daily-financial-download', 'financialController@dailyfinancialDownload');
    // Route::get('/financial/financial-report', 'financialController@financialReport')->name('financial.index');
    // Route::get('/financial/financial-report/edit/{id}', 'financialController@editfinancialReport')->name('financial.editfinancialReport');
    // Route::post('/financial/financial-report/update/{id}', 'financialController@updatefinancialReport')->name('financial.updatefinancialReport');
    Route::get('/financial/income-report-download', 'financialController@incomeDownload')->name('finacial.downloadincome');
    Route::get('/financial/outcome-report-download', 'financialController@outcomeDownload')->name('finacial.downloadoutcome');
    Route::get('/financial/financial-report-download', 'financialController@financialDownload')->name('finacial.downloadfinancial');

    Route::get('/payment/daily-payment', 'PaymentController@dailyPayment')->middleware(['permission:acc_payment']);
    Route::get('/payment/daily-payment-download', 'PaymentController@dailyPaymentDownload')->middleware(['permission:acc_payment']);
    Route::get('/payment/payment-report', 'PaymentController@paymentReport')->name('payment.index')->middleware(['permission:manage_payment']);
    Route::get('/payment/payment-report/edit/{id}', 'PaymentController@editpaymentReport')->name('payment.editpaymentReport');
    Route::post('/payment/payment-report/update/{id}', 'PaymentController@updatepaymentReport')->name('payment.updatepaymentReport');
    Route::get('/payment/payment-report-download', 'PaymentController@paymentReportDownload')->middleware(['permission:acc_payment']);
    Route::get('/payment/payment-info', 'PaymentController@paymentInfo')->name('payment.paymentinfo')->middleware(['permission:manage_payment']);
    Route::get('/payment/payment-info/edit/{id}', 'PaymentController@editInfo')->name('payment.editinfo');
    Route::post('/payment/payment-info/update/{id}', 'PaymentController@updateInfo')->name('payment.updateinfo');
    Route::delete('/payment/payment-report/delete-payment/{id}', 'PaymentController@deletePayment')->name('payment.deletepayment');
    Route::get('/payment/payment-info/create', 'PaymentController@createPembayaran')->name('payment.createpembayaran');
    Route::post('/payment/payment-info/input', 'PaymentController@inputPembayaran')->name('payment.inputpembayaran');
    Route::delete('/payment/payment-info/delete-info/{id}', 'PaymentController@deleteInfo')->name('payment.deleteinfo');
    Route::post('/payment/get-transaction/{id}', 'PaymentController@getTransaction')->middleware(['permission:manage_payment']);
    Route::post('/payment/get-payment/{id}', 'PaymentController@getPayment')->middleware(['permission:manage_deposit']);

    Route::get('/debt-list', 'DebtController@index')->middleware(['permission:debt_show']);
    Route::get('/debt-details/{id}', 'DebtController@details');
    Route::post('/debt-request-download', 'DebtController@downloadRequest')->middleware(['permission:debt_download']);
    Route::get('/debt-download', 'DebtController@download')->middleware(['permission:debt_download']);

    Route::resource('/complaint', 'ComplaintController')->middleware(['permission:manage_complaint']);
    Route::post('/complaint-search-invoice', 'ComplaintController@searchInvoice')->middleware(['permission:manage_complaint']);
    Route::post('/complaint-get-invoice', 'ComplaintController@getInvoice')->middleware(['permission:manage_complaint']);
    Route::get('/complaint/{id}/action', 'ComplaintController@action')->middleware(['permission:action_complaint']);
    Route::put('/complaint/{id}/updateAction', 'ComplaintController@updateAction')->middleware(['permission:action_complaint']);

    Route::resource('/status-management', 'StatusController')->middleware(['permission:show_status']);
});

