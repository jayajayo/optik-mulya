<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Spatie\Permission\Models\Permission;
use Spatie\Permission\Models\Role;
use App\Enums\PermissionEnum;
use Illuminate\Support\Facades\Log;
use App\User;

class Permissions extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'permission:deploy';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'deploying list of permission from enums list permission';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        foreach (PermissionEnum::permissions as $permission){
            $insert = Permission::firstOrNew(['name' => $permission]);
            $insert->guard_name = 'web';
            $insert->save();
        }

        $insert = Role::firstOrNew(['name' => 'superadmin']);
        $insert->guard_name = 'web';
        $insert->save();

        foreach (PermissionEnum::roles as $role){
            $insert = Role::firstOrNew(['name' => $role]);
            $insert->guard_name = 'web';
            $insert->save();
        }

        $roles = Role::where('name','superadmin')->get();
        foreach ($roles as $role) {
            $role->syncPermissions(PermissionEnum::permissions);
        }

        $roles = Role::where('name','owner')->get();
        foreach ($roles as $role) {
            $role->syncPermissions(PermissionEnum::owner);
        }

        $roles = Role::where('name','admin')->get();
        foreach ($roles as $role) {
            $role->syncPermissions(PermissionEnum::admin);
        }

        $roles = Role::where('name','market')->get();
        foreach ($roles as $role) {
            $role->syncPermissions(PermissionEnum::market);
        }

        $roles = Role::where('name','penagih')->get();
        foreach ($roles as $role) {
            $role->syncPermissions(PermissionEnum::penagih);
        }

        $roles = Role::where('name','komplain')->get();
        foreach ($roles as $role) {
            $role->syncPermissions(PermissionEnum::komplain);
        }

        $superadmin = User::find(1);
        $superadmin->syncRoles(['superadmin']);
        $superadmin->syncPermissions(PermissionEnum::permissions);
    }
}
