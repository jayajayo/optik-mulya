<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Income extends Model
{
    protected $table = 'incomes';
    protected $fillable = ['code','information','nomine','memo','payment_id'];
    
    use SoftDeletes;
    public function financial()
    {
        return $this->hasMany('App\Financial')->withTrashed();
    }
    public function payment()
    {
        return $this->hasMany('App\Payment')->withTrashed();
    }
}
