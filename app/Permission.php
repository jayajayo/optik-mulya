<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Permission extends Model
{
    protected $guard = [];
    protected $fillable = ['name'];
}
