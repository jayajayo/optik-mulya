<?php

namespace App\Libraries;

use Illuminate\Http\Request;

trait SortHelper 
{
    public static function header(Request $request, $by='name')
    {
        $url = '';
        $i = 0;

        foreach ($request->query() as $key => $qstring) {
            if (!in_array($key,['order', 'order_by'])){
                $url .=  '&'.$key.'='.$qstring;
                $i++;
            }
        }

        $fas = '<span class="fas fa-sort-down"></span>';
        if ($request->order_by != $by) {
            $fas = '<span class="fas fa-sort"></span>';
        }
        
        if ($request->order_by == $by && $request->order == 'asc') {
            $url .= '&order_by='.$by.'&order=desc';
            $fas = '<span class="fas fa-sort-up" ></span>';
        } else {
            $url .= '&order_by='.$by.'&order=asc';
        }

        return  '<a class="text-right" href='.$request->url().'?' . $url.'>'.$fas.'</a>';
    }
}