<?php

namespace App\Exceptions;

interface ViewableErrors
{
    function getMessages();
}
