<?php

namespace App\Exceptions;

use Exception;
use App\Exceptions\ViewableErrors;

class NoStockException extends Exception implements ViewableErrors
{
    public function getMessages()
    {
        return 'Tidak ada stock untuk produk ini';
    }
}
