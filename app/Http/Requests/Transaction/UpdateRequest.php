<?php

namespace App\Http\Requests\Transaction;

use Illuminate\Foundation\Http\FormRequest;

class UpdateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            __('name') => 'required',
            __('address') => 'required',
            //__('phone') => 'required',
            __('tempo') => 'required',
            __('frame_type') => 'numeric',
            __('order_date') => 'required|date',
            __('finish_date') => 'required|date',
            __('charge_date') => 'required|date',
            __('frame_price') => 'numeric',
            __('information') => 'required_with:'.__('down_payment'),
            __('lens_price') => 'numeric',
            __('rx_price') => 'numeric',
            __('down_payment') => 'numeric',
            __('total_payment') => 'numeric',
            __('debt') => 'numeric',
            __('finish_payment') => 'required|numeric',
        ];
    }
}
