<?php

namespace App\Http\Requests\Complaint;

use Illuminate\Foundation\Http\FormRequest;

class StoreRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            __('date') => 'required|date',
            __('name') => 'required',
            __('invoice_number') => 'required',
            __('address') => 'required',
            __('phone') => 'required',
            __('complaint') => 'required',
        ];
    }
}
