<?php

namespace App\Http\Requests\User;

use Illuminate\Foundation\Http\FormRequest;

class StoreRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            __('fullname') => 'required',
            __('nickname') => 'required',
            __('username') => 'required|regex:/^\S*$/u|unique:users,username',
            __('address') => 'required',
            __('password') => 'required|min:6|confirmed',
            __('mobile_phone') => 'required',
            __('role') => 'required',
            __('team') => 'required_if:role,market',
        ];
    }

    public function messages()
    {
        return [
            __('username').".regex" => __('Input')." ".__('username')." ".__("must be not containt whitespace"),
        ];
    }
}
