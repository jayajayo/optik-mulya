<?php

namespace App\Http\Requests\User;

use Illuminate\Foundation\Http\FormRequest;

class UpdateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            __('fullname') => 'required',
            __('nickname') => 'required',
            __('address') => 'required',
            __('password') => 'nullable|min:6|confirmed',
            __('mobile_phone') => 'required',
            __('role') => 'required',
            __('team') => 'required_if:role,market',
        ];
    }
}
