<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Frame;

class TestingController extends Controller
{
    public function index()
    {
        $frame = Frame::find(1);
        dd($frame->teams()->where('id', 5)->first()->pivot->quantity);
    }
}
