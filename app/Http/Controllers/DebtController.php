<?php

namespace App\Http\Controllers;

use App\Transaction;
use Illuminate\Http\Request;
use App\Traits\PageTrait;
use App\Enums\TransactionEnum;
use Illuminate\Support\Carbon;

class DebtController extends Controller
{
    use PageTrait;

    public function index(Request $request)
    {
        $keyword = $request->keyword ? $request->keyword : null;
        $per_page = $request->per_page ? $request->per_page : 25;

        $order_list = ['order_date', 'code', 'name', 'address', 'total_payment', 'down_payment', 'remain'];
        $payments = TransactionEnum::payments;
        $order_list = array_merge($order_list, $payments);
        $orderby = in_array($request->order_by, $order_list) ? $request->order_by : 'order_date';
        $order = $request->order == 'asc' ? 'asc' : 'desc';

        try {
            $parsedate = Carbon::parse($keyword);
        } catch (\Exception $e) {
            $parsedate = null;
        }

        $select = '';
        $minus = '';
        foreach($payments as $ins) {
            $string_query = 'coalesce(sum(case when payments.name = "'.$ins.'" and payments.status = "accepted" then payments.nomine end), 0)'; 
            $select .=  ' , '.$string_query.' as '.$ins;
            $minus .= '-'.$string_query;
        }

        $transactions = Transaction::selectRaw('
            transactions.* '.$select.',
            (transactions.total_payment) '.$minus.' as remain
        ')
            ->groupBy('transactions.id')
            ->groupBy('transactions.frame_team_id')
            ->groupBy('transactions.code')
            ->groupBy('transactions.sph_r')
            ->groupBy('transactions.sph_l')
            ->groupBy('transactions.cyl_r')
            ->groupBy('transactions.cyl_l')
            ->groupBy('transactions.axis_r')
            ->groupBy('transactions.axis_l')
            ->groupBy('transactions.add_r')
            ->groupBy('transactions.add_l')
            ->groupBy('transactions.pd')
            ->groupBy('transactions.name')
            ->groupBy('transactions.address')
            ->groupBy('transactions.phone')
            ->groupBy('transactions.lens')
            ->groupBy('transactions.memo')
            ->groupBy('transactions.frame_id')
            ->groupBy('transactions.order_date')
            ->groupBy('transactions.finish_date')
            ->groupBy('transactions.charge_date')
            ->groupBy('transactions.tempo')
            ->groupBy('transactions.frame_price')
            ->groupBy('transactions.lens_price')
            ->groupBy('transactions.rx_price')
            ->groupBy('transactions.down_payment')
            ->groupBy('transactions.debt')
            ->groupBy('transactions.total_payment')
            ->groupBy('transactions.finish_payment')
            ->groupBy('transactions.order_status')
            ->groupBy('transactions.faset_status')
            ->groupBy('transactions.packing_status')
            ->groupBy('transactions.paid_off')
            ->groupBy('transactions.created_at')
            ->groupBy('transactions.updated_at')
            ->groupBy('transactions.deleted_at')
            ->leftjoin('payments','transactions.id','payments.transaction_id')
            ->when(!empty($keyword), function($q) use ($keyword, $parsedate){
            $q->where(function ($orq) use ($keyword, $parsedate) {
                $orq->where('transactions.name', 'like', '%'.$keyword.'%')
                    ->orWhere('transactions.code', 'like', '%'.$keyword.'%')
                    ->orWhere('transactions.address', 'like', '%'.$keyword.'%')
                    ->when(!empty($parsedate), function($d) use ($parsedate) {
                        $d->orWhere('transactions.order_date', $parsedate);
                });
                    
            });
        })
            ->orderBy($orderby, $order)
            ->where('paid_off', 0)
            ->paginate($per_page);
        
        
        $links = $this->serializeLink($request, ['keyword', 'per_page','order_by','order']);
        $transactions->appends($links)->links();
        return view('debt.index', compact('transactions','payments'));
    }

    public function details($id)
    {
        $transaction = Transaction::findOrFail($id);
        $payments = TransactionEnum::payments;
        return view('debt.details', compact('transaction','payments'));
    }

    public function downloadRequest(Request $request)
    {
        $start = Carbon::createFromFormat('d-m-Y', $request->startdate)->startOfDay();
        $end = Carbon::createFromFormat('d-m-Y', $request->enddate)->startOfDay();
       
        $transactions = Transaction::whereBetween('order_date', [$start, $end])
            ->where('paid_off', 0)
            ->get();
        
        if ($transactions->count()) {
            return response()->json(['status'=>'ok',  $transactions]);
        } else {
            return response()->json([
                'errors' => ['message' => [__('data not found')]]
            ], 404);
        }
    }

    public function download(Request $request)
    {
        $start = Carbon::createFromFormat('d-m-Y', $request->startdate)->startOfDay();
        $end = Carbon::createFromFormat('d-m-Y', $request->enddate)->startOfDay();
       
        $transactions = Transaction::whereBetween('order_date', [$start, $end])
            ->where('paid_off', 0)
            ->orderBy('order_date', 'asc')
            ->get();

        $payments = TransactionEnum::payments;

        return \Excel::download(new \App\Exports\DebtExport($transactions, $start, $end, $payments), __('Transaction').'_'.time().'.xlsx');
    }

}
