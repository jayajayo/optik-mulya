<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\MerkFrame;
use App\Traits\PageTrait;
use App\Http\Requests\MerkFrame\StoreRequest;

class MerkFrameController extends Controller
{
    use PageTrait;
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $keyword = !empty($request->keyword) ? $request->keyword : null;
        $merkframes = MerkFrame::when($keyword, function ($q) use ($keyword){
            $q->where('name', $keyword);
        })
            ->paginate(20);
        
        $links = $this->serializeLink($request, ['keyword']);
        $merkframes->appends($links)->links();
        return view('merk-frame.index', compact('merkframes','keyword'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreRequest $request)
    {
        $merkframe = new MerkFrame;
        $merkframe->name = $request->{__('merk_name')};
        $merkframe->save();

        $request->session()->flash('status', __('Success'));
        return response()->json(['message'=> __('Success')]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $id)
    {
        $merkframe = MerkFrame::find($id);
        $merkframe->delete();

        $request->session()->flash('status', __('Success'));
        return response()->json(['message'=> __('Success')]);
    }
}
