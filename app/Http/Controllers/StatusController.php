<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Carbon;
use App\Transaction;
use App\Traits\PageTrait;

class StatusController extends Controller
{
    use PageTrait;

    public function __construct()
    {
        $this->middleware('permission:manage_status')->only(['edit','update']);
    }

    public function index(Request $request)
    {
        $keyword = $request->keyword ? $request->keyword : null;
        $per_page = $request->per_page ? $request->per_page : 25;
        
        $order_list = ['order_date', 'code','name', 'address', 'phone'];
        $orderby = in_array($request->order_by, $order_list) ? $request->order_by : 'order_date';
        $order = $request->order == 'asc' ? 'asc' : 'desc';
        
        try {
            $parsedate = Carbon::parse($keyword);
        } catch (\Exception $e) {
            $parsedate = null;
        }

        $transactions = Transaction::with(['payments','user'])
            ->when(!empty($keyword), function($q) use ($keyword, $parsedate){
                $q->where(function ($orq) use ($keyword, $parsedate) {
                    $orq->where('name', 'like', '%'.$keyword.'%')
                        ->orWhere('code', 'like', '%'.$keyword.'%')
                        ->orWhere('address', 'like', '%'.$keyword.'%')
                        ->orWhere('phone', 'like', '%'.$keyword.'%')
                        ->orWhere('memo', 'like', '%'.$keyword.'%')
                        ->when(!empty($parsedate), function($d) use ($parsedate) {
                            $d->orWhere('order_date', $parsedate);
                    })
                        ->orWhereHas('frame', function ($nq) use ($keyword) {
                            $nq->where('name', 'like', '%'.$keyword.'%');
                    });
                });
            })
            ->orderBy($orderby, $order)
            ->paginate($per_page);

        $links = $this->serializeLink($request, ['keyword','per_page', 'order_by', 'order']);
        $transactions->appends($links)->links();

        return view('status.index', compact('transactions'));
    }

    public function edit($id)
    {
        $transaction = Transaction::find($id);
        return view('status.edit', compact('transaction'));
    }

    public function update(Request $request, $id)
    {
        $transaction = Transaction::find($id);
        $transaction->order_status = $request->{__('order_status')} ? $request->{__('order_status')} : 0;
        $transaction->faset_status = $request->{__('faset_status')} ? $request->{__('faset_status')} : 0;
        $transaction->packing_status = $request->{__('packing_status')} ? $request->{__('packing_status')} : 0;
        $transaction->save();

        $request->session()->flash('status', __('Success'));
        return response()->json(['message'=> __('Success')]);
    }
}
