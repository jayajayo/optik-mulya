<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Financial;
use App\Outcome;
use App\Income;
use App\Payment;
use Illuminate\Support\Carbon;

class FinancialController extends Controller
{
    public function financialReport(Request $request){
        $start = !empty($request->start_date) ? 
            Carbon::createFromFormat('d-m-Y', $request->start_date)->startOfDay()->format('Y-m-d'):
            Carbon::today()->startOfDay()->format('Y-m-d');
            
            $next_mount_in_number = Carbon::today()->format('m')+1;
            $next_mount = (int) Carbon::today()->addMonth()->format('m');
            $default_end = $next_mount_in_number != $next_mount ? 
                Carbon::today()
                    ->addMonth()->startOfMonth()
                    ->subDay()->format('Y-m-d'):
                Carbon::today()
                    ->addMonth()->format('Y-m-d');
    
            $end = !empty($request->end_date) ? 
                Carbon::createFromFormat('d-m-Y', $request->end_date)->startOfDay()->format('Y-m-d'):
                $default_end;

        $income = Income::get(); //jaddin array
        $outcome = Outcome::get();  //jaddin array
        $payment = Payment::where('status', 'accepted')->where('payment_date', '>=', $start)
        ->where('payment_date', '<=', $end)->groupBy('payment_date')->selectRaw('sum(nomine) as nomine,payment_date')->get();
        

        //gabungin array outcome dan income
        $arrayincome = $income->toArray();
        $arrayoutcome = $outcome->toArray();
        $arraypayment = $payment->toArray();
        $outcomes = [];
        $incomes = [];
        $payments = [];
        
        foreach($arrayincome as $key){
            $key['jenis'] = 'income';
            $incomes[] = $key;
        }

        foreach($arraypayment as $key){
            $key['jenis'] = 'income';
            $payments[] = $key;
        }

        foreach($arrayoutcome as $key){
            $key['jenis'] = 'outcome';
            $outcomes[] = $key;
        }
        // foreach($arraypayment as $key){
        //     $key['status'] = 'accepted';
        // }
        $tunaipay = Payment::where('status', 'accepted')->where('information', 'Cash')->where('payment_date', '>=', $start)
        ->where('payment_date', '<=', $end)->sum('nomine');
        $transpay = Payment::where('status', 'accepted')->where('information','!=', 'Cash')->where('payment_date', '>=', $start)
        ->where('payment_date', '<=', $end)->sum('nomine');
        $tunai = Income::where('information', 'Cash')->where('payment_date', '>=', $start)
        ->where('payment_date', '<=', $end)->sum('nomine');
        $intrans = Income::where('information', 'Transfer')->where('payment_date', '>=', $start)
        ->where('payment_date', '<=', $end)->sum('nomine');
        $transfer = Outcome::where('information', 'Transfer')->where('payment_date', '>=', $start)
        ->where('payment_date', '<=', $end)->sum('nomine');
        $outtunai = Outcome::where('information', 'Cash')->where('payment_date', '>=', $start)
        ->where('payment_date', '<=', $end)->sum('nomine');

        //semua transaksi tanpa range date
        $alltunaipay = Payment::where('status', 'accepted')->where('information', 'Cash')->sum('nomine');
        $alltranspay = Payment::where('status', 'accepted')->where('information','!=', 'Cash')->sum('nomine');
        $alltunai = Income::where('information', 'Cash')->sum('nomine');
        $allintrans = Income::where('information', 'Transfer')->sum('nomine');
        $alltransfer = Outcome::where('information', 'Transfer')->sum('nomine');
        $allouttunai = Outcome::where('information', 'Cash')->sum('nomine');

        $totaltunai = $tunaipay + $tunai;
        $totaltrans = $intrans + $transpay;
        $total_cash = $totaltunai - $outtunai;
        $total_tf = $totaltrans - $transfer;

        //semua transaksi tanpa range date
        $alltotaltunai = $alltunaipay + $alltunai;
        $alltotaltrans = $allintrans + $alltranspay;
        $alltotal_cash = $alltotaltunai - $allouttunai;
        $alltotal_tf = $alltotaltrans - $alltransfer;

        $financial = array_merge($incomes, $outcomes);
        $mergeincome = array_merge($financial, $payments);
        $data = collect($mergeincome)->where('payment_date', '>=', $start)
        ->where('payment_date', '<=', $end);;
        $sorted = $data->sortBy('payment_date');

        //array gabungan dijadiin collection biar bisa di sort pake laravel collect
        // dd($sorted);
        
        return view('financial.financial-report')->withData($sorted)->withCash($total_cash)->withTransfer($total_tf)->withStart($start)->withEnd($end)->withAllCash($alltotal_cash)->withAllTransfer($alltotal_tf);
    }

    public function incomeReport(Request $request){
            $start = !empty($request->start_date) ? 
                Carbon::createFromFormat('d-m-Y', $request->start_date)->startOfDay()->format('Y-m-d'):
                Carbon::today()->startOfDay()->format('Y-m-d');
                
                $next_mount_in_number = Carbon::today()->format('m')+1;
                $next_mount = (int) Carbon::today()->addMonth()->format('m');
                $default_end = $next_mount_in_number != $next_mount ? 
                    Carbon::today()
                        ->addMonth()->startOfMonth()
                        ->subDay()->format('Y-m-d'):
                    Carbon::today()
                        ->addMonth()->format('Y-m-d');
        
                $end = !empty($request->end_date) ? 
                    Carbon::createFromFormat('d-m-Y', $request->end_date)->startOfDay()->format('Y-m-d'):
                    $default_end;

            $income = Income::get()
                ->where('payment_date', '>=', $start)
                ->where('payment_date', '<=', $end);
            $payment = Payment::where('status', 'accepted')->where('payment_date', '>=', $start)
            ->where('payment_date', '<=', $end)->groupBy('payment_date')->selectRaw('sum(nomine) as nomine,payment_date')->get();
                    // dd($payment);

        $arrayincome = $income->toArray();
        $arraypayment = $payment->toArray();
        $totalincome = array_merge($arrayincome, $arraypayment);
        $data = collect($totalincome)->where('payment_date', '>=', $start)
        ->where('payment_date', '<=', $end);;
        $sortedgroup = $data->sortBy('payment_date')->groupBy('payment_date');
        $sorted = $data->sortBy('payment_date');
        return view('financial.income-report')->withData($sorted)->withDatagroup($sortedgroup)->withStart($start)->withEnd($end);
    }

    public function createIncome(){
        $incomes = Income::get();
        return view('financial.create-income')->withData($incomes);
    }

    public function inputIncome(Request $request){
            $incomes = new Income;
            $incomes->code = $request->{__('code')};
            $incomes->information = $request->{__('information')};
            $incomes->nomine = $request->{__('nomine')};
            $incomes->memo = $request->{__('memo')};
            $incomes->payment_date = $request->payment_date;
            $incomes->save();
            // dd($payment);
    
            $request->session()->flash('status', __('Success'));
            return response()->json(['message'=> __('Success')]);
    }

    public function editIncome($id, Request $request)
    {
        // $title = 'Expertise';
        // $data = Expertise::where('id', $id)->first();
        // return view('expertise::ubah')->withTitle($title)->withData($data);

        // dd($payments);
        $data = Income::where('id', $id)->first();
        // $users = User::where('id','!=',1)->get();
        // $transac = Transaction::all();
        return view('financial.edit-income')->withData($data);
    }

    public function updateIncome($id, Request $request)
    {
        $data = Income::find($id);
        $data->code = $request->{__('code')};
        $data->information = $request->{__('information')};
        $data->nomine = $request->{__('nominal')};
        $data->memo = $request->{__('memo')};
        $data->save();
        // return redirect()->back();
        $request->session()->flash('status', __('Success'));
        return response()->json(['message'=> __('Success')]);
    }

    public function deleteIncome(Request $request, $id)
    {
        $data = Income::find($id);
        $data->delete();

        $request->session()->flash('status', __('Success'));
        return redirect()->back();
    }

    public function deleteOutcome(Request $request, $id)
    {
        $data = Outcome::find($id);
        $data->delete();

        $request->session()->flash('status', __('Success'));
        return redirect()->back();
    }

    public function outReport(Request $request){
        $start = !empty($request->start_date) ? 
            Carbon::createFromFormat('d-m-Y', $request->start_date)->startOfDay()->format('Y-m-d'):
            Carbon::today()->startOfDay()->format('Y-m-d');
            
            $next_mount_in_number = Carbon::today()->format('m')+1;
            $next_mount = (int) Carbon::today()->addMonth()->format('m');
            $default_end = $next_mount_in_number != $next_mount ? 
                Carbon::today()
                    ->addMonth()->startOfMonth()
                    ->subDay()->format('Y-m-d'):
                Carbon::today()
                    ->addMonth()->format('Y-m-d');
    
            $end = !empty($request->end_date) ? 
                Carbon::createFromFormat('d-m-Y', $request->end_date)->startOfDay()->format('Y-m-d'):
                $default_end;

            $outcomes = Outcome::get()
                ->where('payment_date', '>=', $start)
                ->where('payment_date', '<=', $end);

        return view('financial.out-report')->withData($outcomes)->withStart($start)->withEnd($end);
    }

    public function createOutcome(){
        $outcomes = Outcome::get();
        return view('financial.create-outcome')->withData($outcomes);
    }

    public function inputOutcome(Request $request){
            $outcomes = new Outcome;

            $outcomes->code = $request->{__('code')};
            $outcomes->information = $request->{__('information')};
            $outcomes->nomine = $request->{__('nomine')};
            $outcomes->memo = $request->{__('memo')};
            $outcomes->payment_date = Carbon::createFromFormat('d-m-Y', $request->payment_date);
            $outcomes->save();
            // dd($payment);
    
            $request->session()->flash('status', __('Success'));
            return response()->json(['message'=> __('Success')]);
    }

    public function editoutReport($id, Request $request)
    {
        // $title = 'Expertise';
        // $data = Expertise::where('id', $id)->first();
        // return view('expertise::ubah')->withTitle($title)->withData($data);

        // dd($payments);
        $data = Outcome::where('id', $id)->first();
        // $users = User::where('id','!=',1)->get();
        // $transac = Transaction::all();
        return view('financial.edit-outcome')->withData($data);
    }

    public function updateoutReport($id, Request $request)
    {
        $data = Outcome::find($id);
        $data->payment_date = Carbon::createFromFormat('d-m-Y', $request->payment_date);
        $data->code = $request->{__('code')};
        $data->information = $request->{__('information')};
        $data->nomine = $request->{__('nominal')};
        $data->memo = $request->{__('memo')};
        $data->save();
        // return redirect()->back();
        $request->session()->flash('status', __('Success'));
        return response()->json(['message'=> __('Success')]);
    }

    public function cetak_pdf(Request $request){
        $start = !empty($request->start_date) ? 
            Carbon::createFromFormat('Y-m-d', $request->start_date)->startOfDay()->format('Y-m-d'):
            Carbon::today()->startOfDay()->format('Y-m-d');
            
            $next_mount_in_number = Carbon::today()->format('m')+1;
            $next_mount = (int) Carbon::today()->addMonth()->format('m');
            $default_end = $next_mount_in_number != $next_mount ? 
                Carbon::today()
                    ->addMonth()->startOfMonth()
                    ->subDay()->format('Y-m-d'):
                Carbon::today()
                    ->addMonth()->format('Y-m-d');
    
            $end = !empty($request->end_date) ? 
                Carbon::createFromFormat('Y-m-d', $request->end_date)->startOfDay()->format('Y-m-d'):
                $default_end;

        $income = Income::get(); //jaddin array
        $outcome = Outcome::get();  //jaddin array
        $payment = Payment::where('status', 'accepted')->where('payment_date', '>=', $start)
        ->where('payment_date', '<=', $end)->groupBy('payment_date')->selectRaw('sum(nomine) as nomine,payment_date')->get();
        

        //gabungin array outcome dan income
        $arrayincome = $income->toArray();
        $arrayoutcome = $outcome->toArray();
        $arraypayment = $payment->toArray();
        $outcomes = [];
        $incomes = [];
        $payments = [];
        
        foreach($arrayincome as $key){
            $key['jenis'] = 'income';
            $incomes[] = $key;
        }

        foreach($arraypayment as $key){
            $key['jenis'] = 'income';
            $payments[] = $key;
        }

        foreach($arrayoutcome as $key){
            $key['jenis'] = 'outcome';
            $outcomes[] = $key;
        }
        // foreach($arraypayment as $key){
        //     $key['status'] = 'accepted';
        // }
        $tunaipay = Payment::where('status', 'accepted')->where('information', 'Cash')->where('payment_date', '>=', $start)
        ->where('payment_date', '<=', $end)->sum('nomine');
        $transpay = Payment::where('status', 'accepted')->where('information','!=', 'Cash')->where('payment_date', '>=', $start)
        ->where('payment_date', '<=', $end)->sum('nomine');
        $tunai = Income::where('information', 'Cash')->where('payment_date', '>=', $start)
        ->where('payment_date', '<=', $end)->sum('nomine');
        $intrans = Income::where('information', 'Transfer')->where('payment_date', '>=', $start)
        ->where('payment_date', '<=', $end)->sum('nomine');
        $transfer = Outcome::where('information', 'Transfer')->where('payment_date', '>=', $start)
        ->where('payment_date', '<=', $end)->sum('nomine');
        $outtunai = Outcome::where('information', 'Cash')->where('payment_date', '>=', $start)
        ->where('payment_date', '<=', $end)->sum('nomine');
        $totaltunai = $tunaipay + $tunai;
        $totaltrans = $intrans + $transpay;
        $total_cash = $totaltunai - $outtunai;
        $total_tf = $totaltrans - $transfer;
        $financial = array_merge($incomes, $outcomes);
        $mergeincome = array_merge($financial, $payments);
        $data = collect($mergeincome)->where('payment_date', '>=', $start)
        ->where('payment_date', '<=', $end);;
        $sorted = $data->sortBy('payment_date');
        
        return view('financial.financial-pdf')->withData($sorted)->withCash($total_cash)->withTransfer($total_tf)->withStart($start)->withEnd($end);
        // return view('financial.financial-pdf',['payments'=>$payments]);
    }

    public function incomeDownload(Request $request)
    {
        $start = !empty($request->start_date) ? 
        Carbon::createFromFormat('d-m-Y', $request->start_date)->startOfDay()->format('Y-m-d'):
        Carbon::today()->startOfDay()->format('Y-m-d');
        
        $next_mount_in_number = Carbon::today()->format('m')+1;
        $next_mount = (int) Carbon::today()->addMonth()->format('m');
        $default_end = $next_mount_in_number != $next_mount ? 
            Carbon::today()
                ->addMonth()->startOfMonth()
                ->subDay()->format('Y-m-d'):
            Carbon::today()
                ->addMonth()->format('Y-m-d');

        $end = !empty($request->end_date) ? 
            Carbon::createFromFormat('d-m-Y', $request->end_date)->startOfDay()->format('Y-m-d'):
            $default_end;

        $income = Income::get()
            ->where('payment_date', '>=', $start)
            ->where('payment_date', '<=', $end);
        $payment = Payment::where('status', 'accepted')->where('payment_date', '>=', $start)
            ->where('payment_date', '<=', $end)->groupBy('payment_date')->selectRaw('sum(nomine) as nomine,payment_date')->get();
            // dd($payment);

        $arrayincome = $income->toArray();
        $arraypayment = $payment->toArray();
        $totalincome = array_merge($arrayincome, $arraypayment);
        $data = collect($totalincome)->where('payment_date', '>=', $start)
                ->where('payment_date', '<=', $end);;
        $sortedgroup = $data->sortBy('payment_date')->groupBy('payment_date');
        $sorted = $data->sortBy('payment_date');

        return \Excel::download(new \App\Exports\IncomeReportExport($sorted, $start, $end), __('Laporan Pemasukan').'_'.time().'.xlsx');
    }

    public function outcomeDownload(Request $request)
    {
        $start = !empty($request->start_date) ? 
            Carbon::createFromFormat('d-m-Y', $request->start_date)->startOfDay()->format('Y-m-d'):
            Carbon::today()->startOfDay()->format('Y-m-d');
            
            $next_mount_in_number = Carbon::today()->format('m')+1;
            $next_mount = (int) Carbon::today()->addMonth()->format('m');
            $default_end = $next_mount_in_number != $next_mount ? 
                Carbon::today()
                    ->addMonth()->startOfMonth()
                    ->subDay()->format('Y-m-d'):
                Carbon::today()
                    ->addMonth()->format('Y-m-d');
    
            $end = !empty($request->end_date) ? 
                Carbon::createFromFormat('d-m-Y', $request->end_date)->startOfDay()->format('Y-m-d'):
                $default_end;

            $outcome = Outcome::get()
                ->where('payment_date', '>=', $start)
                ->where('payment_date', '<=', $end);

        return \Excel::download(new \App\Exports\OutcomeReportExport($outcome, $start, $end), __('Laporan Pengeluaran').'_'.time().'.xlsx');
    }

    public function financialDownload(Request $request)
    {
        $start = !empty($request->start_date) ? 
            Carbon::createFromFormat('d-m-Y', $request->start_date)->startOfDay()->format('Y-m-d'):
            Carbon::today()->startOfDay()->format('Y-m-d');
            
            $next_mount_in_number = Carbon::today()->format('m')+1;
            $next_mount = (int) Carbon::today()->addMonth()->format('m');
            $default_end = $next_mount_in_number != $next_mount ? 
                Carbon::today()
                    ->addMonth()->startOfMonth()
                    ->subDay()->format('Y-m-d'):
                Carbon::today()
                    ->addMonth()->format('Y-m-d');
    
            $end = !empty($request->end_date) ? 
                Carbon::createFromFormat('d-m-Y', $request->end_date)->startOfDay()->format('Y-m-d'):
                $default_end;

        $income = Income::get(); //jaddin array
        $outcome = Outcome::get();  //jaddin array
        $payment = Payment::where('status', 'accepted')->where('payment_date', '>=', $start)
        ->where('payment_date', '<=', $end)->groupBy('payment_date')->selectRaw('sum(nomine) as nomine,payment_date')->get();
        

        //gabungin array outcome dan income
        $arrayincome = $income->toArray();
        $arrayoutcome = $outcome->toArray();
        $arraypayment = $payment->toArray();
        $outcomes = [];
        $incomes = [];
        $payments = [];
        
        foreach($arrayincome as $key){
            $key['jenis'] = 'income';
            $incomes[] = $key;
        }

        foreach($arraypayment as $key){
            $key['jenis'] = 'income';
            $payments[] = $key;
        }

        foreach($arrayoutcome as $key){
            $key['jenis'] = 'outcome';
            $outcomes[] = $key;
        }
        // foreach($arraypayment as $key){
        //     $key['status'] = 'accepted';
        // }
        $tunaipay = Payment::where('status', 'accepted')->where('information', 'Cash')->where('payment_date', '>=', $start)
        ->where('payment_date', '<=', $end)->sum('nomine');
        $transpay = Payment::where('status', 'accepted')->where('information','!=', 'Cash')->where('payment_date', '>=', $start)
        ->where('payment_date', '<=', $end)->sum('nomine');
        $tunai = Income::where('information', 'Cash')->where('payment_date', '>=', $start)
        ->where('payment_date', '<=', $end)->sum('nomine');
        $intrans = Income::where('information', 'Transfer')->where('payment_date', '>=', $start)
        ->where('payment_date', '<=', $end)->sum('nomine');
        $transfer = Outcome::where('information', 'Transfer')->where('payment_date', '>=', $start)
        ->where('payment_date', '<=', $end)->sum('nomine');
        $outtunai = Outcome::where('information', 'Cash')->where('payment_date', '>=', $start)
        ->where('payment_date', '<=', $end)->sum('nomine');

        //semua transaksi tanpa range date
        $alltunaipay = Payment::where('status', 'accepted')->where('information', 'Cash')->sum('nomine');
        $alltranspay = Payment::where('status', 'accepted')->where('information','!=', 'Cash')->sum('nomine');
        $alltunai = Income::where('information', 'Cash')->sum('nomine');
        $allintrans = Income::where('information', 'Transfer')->sum('nomine');
        $alltransfer = Outcome::where('information', 'Transfer')->sum('nomine');
        $allouttunai = Outcome::where('information', 'Cash')->sum('nomine');

        $totaltunai = $tunaipay + $tunai;
        $totaltrans = $intrans + $transpay;
        $total_cash = $totaltunai - $outtunai;
        $total_tf = $totaltrans - $transfer;

        //semua transaksi tanpa range date
        $alltotaltunai = $alltunaipay + $alltunai;
        $alltotaltrans = $allintrans + $alltranspay;
        $alltotal_cash = $alltotaltunai - $allouttunai;
        $alltotal_tf = $alltotaltrans - $alltransfer;

        $financial = array_merge($incomes, $outcomes);
        $mergeincome = array_merge($financial, $payments);
        $data = collect($mergeincome)->where('payment_date', '>=', $start)
        ->where('payment_date', '<=', $end);;
        $sorted = $data->sortBy('payment_date');

        return \Excel::download(new \App\Exports\FinancialReportExport($sorted, $start, $end), __('Laporan Keuangan').'_'.time().'.xlsx');
    }
}
