<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Complaint;
use App\Traits\PageTrait;
use Illuminate\Support\Carbon;
use App\Http\Requests\Complaint\StoreRequest;
use App\User;
use Auth;
use App\Transaction;

class ComplaintController extends Controller
{
    use PageTrait;

    public function index(Request $request)
    {
        $keyword = $request->keyword ? $request->keyword : null;
        $per_page = $request->per_page ? $request->per_page : 25;

        $order_list = ['date', 'name', 'invoice_number', 'address', 'phone', 'complaint', 'recipient', 'action'];
        $orderby = in_array($request->order_by, $order_list) ? $request->order_by : 'id';
        $order = $request->order == 'asc' ? 'asc' : 'desc';

        try {
            $parsedate = Carbon::parse($keyword);
        } catch (\Exception $e) {
            $parsedate = null;
        }

        $complaints = Complaint::selectRaw('
            complaints.*,
            users.name as recipient
        ')
            ->leftjoin('users', 'complaints.user_id', 'users.id')
            ->when(!empty($keyword), function($q) use ($keyword, $parsedate){
            $q->where(function ($orq) use ($keyword, $parsedate) {
                $orq->where('complaints.name', 'like', '%'.$keyword.'%')
                    ->orWhere('complaints.invoice_number', 'like', '%'.$keyword.'%')
                    ->orWhere('complaints.address', 'like', '%'.$keyword.'%')
                    ->orWhere('complaints.phone', 'like', '%'.$keyword.'%')
                    ->orWhere('complaints.action', 'like', '%'.$keyword.'%')
                    ->orWhere('complaints.complaint', 'like', '%'.$keyword.'%')
                    ->orWhere('users.name', 'like', '%'.$keyword.'%')
                    ->when(!empty($parsedate), function($d) use ($parsedate) {
                        $d->orWhere('date', $parsedate);
                });
            });
        })
            ->orderBy($orderby, $order)
            ->paginate($per_page);

        $links = $this->serializeLink($request, ['keyword', 'per_page','order_by','order']);
        $complaints->appends($links)->links();
        return view('complaint.index', compact('complaints'));
    }

    public function create()
    {
        return view('complaint.create');
    }

    public function store(StoreRequest $request)
    {
        $complaint = new Complaint;
        $complaint->creator_id = Auth::user()->id;
        $complaint->date = Carbon::createFromFormat('d-m-Y',$request->{__('date')});
        $complaint->name = $request->{__('name')};
        $complaint->invoice_number = Transaction::find($request->{__('invoice_number')})->code;
        $complaint->address = $request->{__('address')};
        $complaint->phone = $request->{__('phone')};
        $complaint->complaint = $request->{__('complaint')};
        $complaint->waiting_status = 1;
        $complaint->save();

        $request->session()->flash('status', __('Success'));
        return response()->json(['message'=> __('Success')]);
    }

    public function edit($id)
    {
        $complaint = Complaint::find($id);
        $users = User::where('id','!=', 1)->get();
        return view('complaint.edit', compact('complaint', 'users'));
    }

    public function action($id)
    {
        $complaint = Complaint::find($id);
        $users = User::where('id','!=', 1)->get();
        return view('complaint.action', compact('complaint', 'users'));
    }

    public function update(StoreRequest $request, $id)
    {
        $complaint = Complaint::find($id);
        $complaint->date = Carbon::createFromFormat('d-m-Y',$request->{__('date')});
        $complaint->name = $request->{__('name')};
        $complaint->invoice_number = $request->{__('invoice_number')};
        $complaint->address = $request->{__('address')};
        $complaint->phone = $request->{__('phone')};
        $complaint->complaint = $request->{__('complaint')};
        $complaint->save();

        $request->session()->flash('status', __('Success'));
        return response()->json(['message'=> __('Success')]);
    }

    public function updateAction(StoreRequest $request, $id)
    {
        $complaint = Complaint::find($id);
        $complaint->date = Carbon::createFromFormat('d-m-Y',$request->{__('date')});
        $complaint->name = $request->{__('name')};
        $complaint->invoice_number = $request->{__('invoice_number')};
        $complaint->address = $request->{__('address')};
        $complaint->phone = $request->{__('phone')};
        $complaint->complaint = $request->{__('complaint')};
        $complaint->user_id = Auth::user()->id;
        $complaint->action = $request->{__('action_response')};
        $complaint->waiting_status = $request->{__('waiting_status')} ? $request->{__('waiting_status')} : 0;
        $complaint->finish_status = $request->{__('finish_status')} ? $request->{__('finish_status')} : 0;
        $complaint->save();

        $request->session()->flash('status', __('Success'));
        return response()->json(['message'=> __('Success')]);
    }

    public function destroy(Request $request, $id)
    {
        $complaint = Complaint::find($id);
        $complaint->delete();

        $request->session()->flash('status', __('Success'));
        return response()->json(['message'=> __('Success')]);
    }

    public function searchInvoice(Request $request)
    {
        $keyword = !empty($request->keyword) ? $request->keyword : null;
        $transactions = Transaction::select('id','code')
            ->when(!empty($keyword), function ($q) use ($keyword){
            $q  ->where('code', 'like', '%'.$keyword.'%');
        })  ->get();

        return response()->json($transactions);
    }

    public function getInvoice(Request $request)
    {
        $transaction = Transaction::find($request->id);
        return response()->json($transaction);
    }
}
