<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Transaction;
use App\Team;
use App\Omset;
use Illuminate\Support\Carbon;

class OmsetController extends Controller
{
    public function omsetReport(Request $request){
        $start = !empty($request->start_date) ? 
        Carbon::createFromFormat('d-m-Y', $request->start_date)->startOfDay()->format('Y-m-d'):
        Carbon::today()->startOfDay()->format('Y-m-d');

        $next_mount_in_number = Carbon::today()->format('m')+1;
        $next_mount = (int) Carbon::today()->addMonth()->format('m');
        $default_end = $next_mount_in_number != $next_mount ? 
            Carbon::today()
                ->addMonth()->startOfMonth()
                ->subDay()->format('Y-m-d'):
            Carbon::today()
                ->addMonth()->format('Y-m-d');

        $end = !empty($request->end_date) ? 
            Carbon::createFromFormat('d-m-Y', $request->end_date)->startOfDay()->format('Y-m-d'):
            $default_end;

        $teams = Team::orderBy('order','asc')->get();
        $team_id = !empty($request->team_id) ? $request->team_id : null;

        $omset = Omset::where('created_at', '>=', $start)
        ->where('created_at', '<=', $end)
        ->when(!empty($team_id), function($q) use ($team_id){
            $q->where('team_id', $team_id);
        })->get();

        $totalA = 0.00;
        $totalB = 0.00;
        $totalC = 0.00;
        $totalD = 0.00;
        $totalT = 0.00;

        $omsets = Omset::get()->where('created_at', '>=', $start)
        ->where('created_at', '<=', $end)->where('transaction_id',0);

        foreach($omsets as $key){
            
            if($key->team_id == 1){
                $totalA = $totalA + $key->minus;
            }elseif($key->team_id == 2){
                $totalB = $totalB + $key->minus;
            }elseif($key->team_id == 3){
                $totalC = $totalC + $key->minus;
            }elseif($key->team_id == 4){
                $totalD = $totalD + $key->minus;
            }else{
                $totalT = $totalT + $key->minus;
            }
        }
        $dataminus = [
            'A' => $totalA,
            'B' => $totalB,
            'C' => $totalC,
            'D' => $totalD,
            'Toko' => $totalT,
        ];

        $teams = Team::orderBy('order','asc')->get();

        $team_id = !empty($request->team_id) ? $request->team_id : null;

        foreach ($teams as $team) {
            $getOmset = Transaction::selectRaw('
                    (transactions.total_payment-transactions.rx_price) as total'
                )
                ->leftjoin('frames','transactions.frame_id', '=', 'frames.id')
                ->leftjoin('payments','payments.transaction_id', '=', 'transactions.id')
                ->leftjoin('users', 'payments.user_id', '=', 'users.id')
                ->leftjoin('teams', 'teams.id', '=', 'users.team_id')
                ->where('payments.name', '=', 'dp')
                ->where('teams.name', $team->name)
                ->where('order_date', '>=', $start)
                ->where('order_date', '<=', $end)
                ->get();
            $totalomset[$team->name] = $getOmset->count() ? $getOmset->sum->total : 0;         
        }

        
        return view('omset.omset-report')->withData($omset)->withStart($start)->withEnd($end)->withTotal($totalomset)->withTeams($teams)->withMinus($dataminus);
    }
    
    public function createOmset(){
        $omset = Omset::get();
        $teams = Team::get();
        return view('omset.create-omset')->withData($omset)->withTeams($teams);
    }

    public function inputOmset(Request $request){
        $omset = new Omset;
        $omset->transaction_id = $request->trans_id;
        $omset->team_id = $request->{__('team')};
        $omset->minus = $request->minus;
        $omset->information = $request->{__('memo')};
        $omset->save();

        $request->session()->flash('status', __('Success'));
        return redirect()->back();
    }

    public function omsetDownload(Request $request){
        $start = !empty($request->start_date) ? 
        Carbon::createFromFormat('d-m-Y', $request->start_date)->startOfDay()->format('Y-m-d'):
        Carbon::today()->startOfDay()->format('Y-m-d');

        $next_mount_in_number = Carbon::today()->format('m')+1;
        $next_mount = (int) Carbon::today()->addMonth()->format('m');
        $default_end = $next_mount_in_number != $next_mount ? 
            Carbon::today()
                ->addMonth()->startOfMonth()
                ->subDay()->format('Y-m-d'):
            Carbon::today()
                ->addMonth()->format('Y-m-d');

        $end = !empty($request->end_date) ? 
            Carbon::createFromFormat('d-m-Y', $request->end_date)->startOfDay()->format('Y-m-d'):
            $default_end;

        $teams = Team::orderBy('order','asc')->get();
        $team_id = !empty($request->team_id) ? $request->team_id : null;

        $omset = Omset::where('created_at', '>=', $start)
        ->where('created_at', '<=', $end)
        ->when(!empty($team_id), function($q) use ($team_id){
            $q->where('team_id', $team_id);
        })->get();

        return \Excel::download(new \App\Exports\OmsetReportExport($omset, $start, $end), __('Omset').'_'.time().'.xlsx');
    }

    public function deleteOmset(Request $request, $id){
        $data = Omset::find($id);
        $data->delete();

        $request->session()->flash('status', __('Success'));
        return redirect()->back();
    }

    public function editOmset($id){
        $data = Omset::where('id',$id)->first();
        $team = Team::all();

        return view('omset.edit-omset')->withData($data)->withTeam($team);
    }

    public function updateOmset(Request $request, $id){
        $data = Omset::find($id);
        $data->team_id = $request->{__('team')};
        $data->minus = $request->minus;
        $data->information = $request->{__('memo')};
        $data->save();

        $request->session()->flash('status', __('Success'));
        return redirect()->back();

    }

}
