<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Frame;
use App\Http\Requests\Transaction\StoreRequest;
use App\MerkFrame;
use App\Payment;
use App\Omset;
use App\Team;
use App\Type;
use App\Traits\PageTrait;
use App\Transaction;
use Auth;
use App\Enums\TransactionEnum;
use App\Http\Requests\Transaction\UpdateRequest;
use Illuminate\Support\Carbon;
use App\Exceptions\NoStockException;

class TransactionController extends Controller
{
    use PageTrait;
    
    public function __construct()
    {
        $this->middleware('permission:transaction_create', ['only' => ['create', 'store']]);
        $this->middleware('permission:transaction_download', ['only' => ['download']]);
        $this->middleware('permission:transaction_edit', ['only' => ['edit', 'update']]);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $start = !empty($request->start_date) ? 
            Carbon::createFromFormat('d-m-Y', $request->start_date)->startOfDay()->format('Y-m-d'):
            Carbon::today()->startOfDay()->format('Y-m-d');

        $next_mount_in_number = Carbon::today()->format('m')+1;
        $next_mount = (int) Carbon::today()->addMonth()->format('m');
        $default_end = $next_mount_in_number != $next_mount ? 
            Carbon::today()
                ->addMonth()->startOfMonth()
                ->subDay()->format('Y-m-d'):
            Carbon::today()
                ->addMonth()->format('Y-m-d');

        $end = !empty($request->end_date) ? 
            Carbon::createFromFormat('d-m-Y', $request->end_date)->startOfDay()->format('Y-m-d'):
            $default_end;
        $keyword = $request->keyword ? $request->keyword : null;
        $per_page = $request->per_page ? $request->per_page : 25;
        $order_list = ['order_date', 'charge_date', 'code', 'name', 'address', 'phone', 'frame', 'lens', 'rx_price', 'pd','total_payment', 'memo', 'paid_off', 'status'];
        $orderby = in_array($request->order_by, $order_list) ? $request->order_by : 'transactions.id';
        $order = $request->order == 'asc' ? 'asc' : 'desc';

        try {
            $parsedate = Carbon::parse($keyword);
        } catch (\Exception $e) {
            $parsedate = null;
        }

        $teams = Team::orderBy('order','asc')->get();

        foreach ($teams as $team) {
            $getOmset = Transaction::selectRaw('
                    (transactions.total_payment-transactions.rx_price) as total'
                )
                ->leftjoin('frames','transactions.frame_id', '=', 'frames.id')
                ->leftjoin('payments','payments.transaction_id', '=', 'transactions.id')
                ->leftjoin('users', 'payments.user_id', '=', 'users.id')
                ->leftjoin('teams', 'teams.id', '=', 'users.team_id')
                ->where('payments.name', '=', 'dp')
                    ->when(!empty($keyword), function($q) use ($keyword, $parsedate){
                    $q->where(function ($orq) use ($keyword, $parsedate) {
                        $orq->where('transactions.name', 'like', '%'.$keyword.'%')
                            ->orWhere('transactions.code', 'like', '%'.$keyword.'%')
                            ->orWhere('transactions.address', 'like', '%'.$keyword.'%')
                            ->orWhere('transactions.phone', 'like', '%'.$keyword.'%')
                            ->orWhere('transactions.memo', 'like', '%'.$keyword.'%')
                            ->orWhere('transactions.tempo', 'like', '%'.$keyword.'%')
                            ->when(!empty($parsedate), function($d) use ($parsedate) {
                                $d->orWhere('transactions.order_date', $parsedate);
                            })
                            ->orWhere('frames.name', 'like', '%'.$keyword.'%')
                            ->when(in_array(strtolower($keyword), ['lunas', 'belum lunas']) , function($d) use ($keyword) {
                                if ($keyword == 'lunas') {
                                    $search = 1;
                                } else {
                                    $search = 0;
                                }
                                $d->orWhere('paid_off', $search);
                            });
                    });
                })
                ->where('teams.name', $team->name)
                ->where('order_date', '>=', $start)
                ->where('order_date', '<=', $end)
                ->get();
            $totalomset[$team->name] = $getOmset->count() ? $getOmset->sum->total : 0;
        }

        $totalA = 0.00;
        $totalB = 0.00;
        $totalC = 0.00;
        $totalD = 0.00;
        $totalT = 0.00;

        $omsets = Omset::get()->where('created_at', '>=', $start)
        ->where('created_at', '<=', $end)->where('transaction_id',0);
        foreach($omsets as $key){
            
            if($key->team_id == 1){
                $totalA = $totalA + $key->minus;
            }elseif($key->team_id == 2){
                $totalB = $totalB + $key->minus;
            }elseif($key->team_id == 3){
                $totalC = $totalC + $key->minus;
            }elseif($key->team_id == 4){
                $totalD = $totalD + $key->minus;
            }else{
                $totalT = $totalT + $key->minus;
            }
        }
        $dataminus = [
            'A' => $totalA,
            'B' => $totalB,
            'C' => $totalC,
            'D' => $totalD,
            'Toko' => $totalT,
        ];

        $teams = Team::orderBy('order','asc')->get();

        foreach ($teams as $team) {
            $getOmset = Transaction::selectRaw('
                    (transactions.total_payment-transactions.rx_price) as total'
                )
                ->leftjoin('frames','transactions.frame_id', '=', 'frames.id')
                ->leftjoin('payments','payments.transaction_id', '=', 'transactions.id')
                ->leftjoin('users', 'payments.user_id', '=', 'users.id')
                ->leftjoin('teams', 'teams.id', '=', 'users.team_id')
                ->where('payments.name', '=', 'dp')
                ->where('teams.name', $team->name)
                ->where('order_date', '>=', $start)
                ->where('order_date', '<=', $end)
                ->get();
            $totalomset[$team->name] = $getOmset->count() ? $getOmset->sum->total : 0;
        }

        $transactions = Transaction::selectRaw('
            transactions.*,
            frames.name as frame,
            CASE 
                WHEN transactions.paid_off = 0 THEN "Not paid off" 
                WHEN transactions.paid_off = 1 THEN "Paid off"     
            END AS paid_off
        ')
            ->leftjoin('frames','transactions.frame_id', '=', 'frames.id')
            ->leftjoin('merk_frames', 'frames.merk_frame_id', '=', 'merk_frames.id')
            ->when(!empty($keyword), function($q) use ($keyword, $parsedate){
                $q->where(function ($orq) use ($keyword, $parsedate) {
                    $orq->where('transactions.name', 'like', '%'.$keyword.'%')
                        ->orWhere('transactions.code', 'like', '%'.$keyword.'%')
                        ->orWhere('transactions.address', 'like', '%'.$keyword.'%')
                        ->orWhere('transactions.phone', 'like', '%'.$keyword.'%')
                        ->orWhere('transactions.memo', 'like', '%'.$keyword.'%')
                        ->orWhere('transactions.status', 'like', '%'.$keyword.'%')
                        ->orWhere('transactions.tempo', 'like', '%'.$keyword.'%')
                        ->orWhere('transactions.lens', 'like', '%'.$keyword.'%')
                        ->orWhere('merk_frames.name',  'like', '%'.$keyword.'%')
                        ->orWhere('frames.name',  'like', '%'.$keyword.'%')
                        ->when(!empty($parsedate), function($d) use ($parsedate) {
                            $d->orWhere('transactions.order_date', $parsedate);
                        })
                        
                        ->when(in_array(strtolower($keyword), ['lunas', 'belum lunas']) , function($d) use ($keyword) {
                            if ($keyword == 'lunas') {
                                $search = 1;
                            } else {
                                $search = 0;
                            }
                            $d->orWhere('paid_off', $search);
                        });
                });
            })
            ->where('order_date', '>=', $start)
            ->where('order_date', '<=', $end)
            ->orderBy($orderby, $order)
            ->paginate($per_page);

            $types = Type::get();
        
        $links = $this->serializeLink($request, ['keyword','per_page','order_by','order','start_date','end_date']);
        $transactions->appends($links)->links();
        return view('transaction.index',compact('totalomset','teams','start','end','transactions','dataminus','types'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create($other=false)
    {
        $team = Auth::user()->team->code;
        $count = Transaction::where('code', 'like', $team.'%')->withTrashed()->count()+1;
        $newcode =  $team.'1'.sprintf("%04s", $count); 
        $merkframes = MerkFrame::all();
        $types = Type::all();
        if ($other) {
            return view('transaction.other', compact('merkframes','newcode','types'));
        }
        return view('transaction.create', compact('merkframes','newcode','types'));
    }
    
    public function createOther()
    {
        return '';
        // $team = Auth::user()->team->code;
        // $count = Transaction::where('code', 'like', $team.'%')->withTrashed()->count()+1;
        // $newcode =  $team.'1'.sprintf("%04s", $count); 
        // $merkframes = MerkFrame::all();
        // return view('transaction.create_other', compact('merkframes','newcode'));
    }
    
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreRequest $request)
    {
        $team = Auth::user()->team->code;
        $count = Transaction::where('code', 'like', $team.'%')->withTrashed()->count()+1;
        $newcode =  $team.'1'.sprintf("%04s", $count); 

        $trans = new Transaction;
        $trans->code = $newcode;
        $trans->sph_r = $request->{__('sph_r')};
        $trans->sph_l = $request->{__('sph_l')};
        $trans->cyl_r = $request->{__('cyl_r')};
        $trans->cyl_l = $request->{__('cyl_l')};
        $trans->axis_r = $request->{__('axis_r')};
        $trans->axis_l = $request->{__('axis_l')};
        $trans->add_r = $request->{__('add_r')};
        $trans->add_l = $request->{__('add_l')};
        $trans->pd = $request->{__('pd')};
        $trans->name = $request->{__('name')};
        $trans->address = $request->{__('address')};
        $trans->phone = $request->{__('phone')};
        $trans->lens = $request->{__('lens')};
        $trans->memo = $request->{__('memo')};
        $trans->frame_id = $request->{__('frame_type')};
        $trans->status = $request->status_customer;
        //validaate stock
        if (!$request->validateStock($trans->frame_id)){
            return response()->json([
                'errors' => ['stock' => ['stock frame tidak tersedia']]
            ],422);
        };
        $trans->tempo = $request->{__('tempo')};
        $trans->order_date = Carbon::createFromFormat('d-m-Y', $request->{__('order_date')});
        $trans->finish_date = Carbon::createFromFormat('d-m-Y', $request->{__('finish_date')});
        $trans->charge_date = Carbon::createFromFormat('d-m-Y', $request->{__('charge_date')});
        $trans->frame_price = empty($request->{__('frame_price')}) ? 0 : $request->{__('frame_price')};
        $trans->lens_price = empty($request->{__('lens_price')}) ? 0 : $request->{__('lens_price')};
        $trans->rx_price = empty($request->{__('rx_price')}) ? 0 : $request->{__('rx_price')};
        $trans->total_payment = $trans->frame_price + $trans->lens_price + $trans->rx_price;
        $trans->down_payment = empty($request->{__('down_payment')}) ? 0 : $request->{__('down_payment')};
        $trans->debt = $trans->total_payment - $trans->down_payment;
        $trans->finish_payment = empty($request->{__('finish_payment')}) ? 0 : $request->{__('finish_payment')};
        $trans->frame_team_id = Auth::user()->team_id;
        $trans->save();


        $omset = new Omset;
        $omset->transaction_id=$trans->id;
        $omset->team_id=Auth::user()->team->id;

        $omset->save();

        foreach (TransactionEnum::payments as $i => $val) {
            if ($val == 'dp') {
                $chargedate = Carbon::today();
            } else {
                $chargedate = Carbon::createFromFormat('d-m-Y', $request->{__('charge_date')});
                $nowday = $chargedate->day;
                if ($i != 1 && $i != 13) {
                    $chargedate->addMonth($i-1);
                    $check = new Carbon('first day of '.$chargedate);
                    if ($chargedate->day != $nowday) {
                        $chargedate = $check->subDay();
                    } 
                }
            }

            $pay = new Payment;
            $pay->name = $val;
            $pay->transaction_id = $trans->id;
            $pay->user_id = Auth::user()->id;
            $pay->charge_date = $chargedate;
            $pay->status = 'unpaid';
            if ($val == 'dp') {
                $pay->nomine = $trans->down_payment;
                $pay->recipient_id = Auth::user()->id;
                $pay->payment_date = Carbon::today();
                $pay->information = $request->{__('information')};
                if ($trans->down_payment == 0) {
                    $pay->status = 'accepted';
                } else {
                    $pay->status = 'paid';
                }
            }
            $pay->save();
        }

        if (!is_null($trans->frame_id)) {
            $frame = Frame::find($trans->frame_id);
            $quantity = ($frame->teams()->where('id', Auth::user()->team_id)->first()->pivot->quantity) - 1;
            $frame->teams()->updateExistingPivot(Auth::user()->team_id,['quantity' => $quantity < 0 ? 0 : $quantity]);
        }

        $request->session()->flash('status', __('Success'));
        return response()->json(['message'=> __('Success')]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return redirect('/transaction');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $transaction = Transaction::findOrFail($id);
        $team = Auth::user()->team->code;
        $merkframes = MerkFrame::all();
        $types = Type::all();
        $frame_team = $transaction->frame_team_id ?? ($transaction->payments()->first()->recipient()->first()->team()->first()->id ?? null);
        $teams = Team::orderBy('order','asc')->get();
        return view('transaction.edit', compact('transaction', 'team', 'teams', 'merkframes', 'frame_team','types'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateRequest $request, $id)
    {
        try {
            $trans = Transaction::findOrFail($id);
            $trans->sph_r = $request->{__('sph_r')};
            $trans->sph_l = $request->{__('sph_l')};
            $trans->cyl_r = $request->{__('cyl_r')};
            $trans->cyl_l = $request->{__('cyl_l')};
            $trans->axis_r = $request->{__('axis_r')};
            $trans->axis_l = $request->{__('axis_l')};
            $trans->add_r = $request->{__('add_r')};
            $trans->add_l = $request->{__('add_l')};
            $trans->pd = $request->{__('pd')};
            $trans->name = $request->{__('name')};
            $trans->address = $request->{__('address')};
            $trans->phone = $request->{__('phone')};
            $trans->lens = $request->{__('lens')};
            $trans->memo = $request->{__('memo')};
            $oldframe = $trans->frame_id;
            $trans->frame_id = $request->{__('frame_type')};
            $trans->tempo = $request->{__('tempo')};
            $trans->order_date = Carbon::createFromFormat('d-m-Y', $request->{__('order_date')});
            $trans->finish_date = Carbon::createFromFormat('d-m-Y', $request->{__('finish_date')});
            $chargedatechange = $trans->charge_date->ne(Carbon::createFromFormat('d-m-Y', $request->{__('charge_date')})->startOfDay()) ? : false;
            $trans->charge_date = Carbon::createFromFormat('d-m-Y', $request->{__('charge_date')});
            $trans->frame_price = empty($request->{__('frame_price')}) ? 0 : $request->{__('frame_price')};
            $trans->lens_price = empty($request->{__('lens_price')}) ? 0 : $request->{__('lens_price')};
            $trans->rx_price = empty($request->{__('rx_price')}) ? 0 : $request->{__('rx_price')};
            $trans->total_payment = $trans->frame_price + $trans->lens_price + $trans->rx_price;
            $trans->down_payment = empty($request->{__('down_payment')}) ? 0 : $request->{__('down_payment')};
            $trans->debt = $trans->total_payment - $trans->down_payment;
            $trans->finish_payment = empty($request->{__('finish_payment')}) ? 0 : $request->{__('finish_payment')};
            $trans->status = $request->status_customer;
            $trans->save();

            foreach (TransactionEnum::payments as $i => $val) {
                if ($val == 'dp') {
                    $chargedate = Carbon::today();
                } else {
                    $chargedate = Carbon::createFromFormat('d-m-Y', $request->{__('charge_date')});
                    $nowday = $chargedate->day;
                    if ($i != 1 ) {
                        $chargedate->addMonth($i-1);
                        $check = new Carbon('first day of '.$chargedate);
                        if ($chargedate->day != $nowday) {
                            $chargedate = $check->subDay();
                        } 
                    }
                }
                
                $pay = Payment::where('transaction_id', $trans->id)->where('name', $val)->first();
                //$pay->user_id = Auth::user()->id;
                
                if ( $chargedatechange ) {
                    $pay->charge_date = $chargedate;
                }

                if ($val == 'dp') {
                    $pay->nomine = $trans->down_payment;
                    $pay->information = $request->{__('information')};
                }
                $pay->save();
            }

            if ($oldframe != $trans->frame_id) {
                $getoldframe = Frame::find($oldframe);
                
                $payment = $trans->payments()->where('name','dp')->first();

                $olduser = $trans->frame_team_id ?? $payment->recipient->team_id;

                if ($getoldframe){
                    $quantityold = ($getoldframe->teams()->where('id', $olduser)->first()->pivot->quantity) + 1;
                    $getoldframe->teams()->updateExistingPivot($olduser,['quantity' => $quantityold]);
                }
                
                if (!is_null($trans->frame_id)) {
                    $frame = Frame::find($trans->frame_id);
                    $active_team = $request->frame_team ?? $olduser;
                    $quantity = ($frame->teams()->where('id', $active_team)->first()->pivot->quantity) - 1;
                    $trans->frame_team_id = $active_team;
                    $frame->teams()->updateExistingPivot($active_team, ['quantity' => $quantity < 0 ? 0 : $quantity]);
                }
            }

            //check paid off status 
            $totalsettlement = $trans->payments()->where('status','accepted')->sum('nomine');

            $trans->paid_off = ($totalsettlement >= $trans->total_payment) ? 1 : 0;
            if($trans->status=='Macet'){
                $trans->paid_off = 1;
            }else{
                $trans->paid_off = 0;
            }
            $trans->save(); 

            $request->session()->flash('status', __('Success'));
            return redirect()->route('transaction.index');
        } catch (\ValidationException $e) {
            return redirect()->back()->withErrors($e->errors);
        } catch (Exception $e) {
            \Log::error($e->getMessage());
            return redirect()->back();
        }

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        return redirect('/transaction');
    }

    public function downloadRequest(Request $request)
    {
        $start = Carbon::createFromFormat('d-m-Y', $request->startdate)->startOfDay();
        $end = Carbon::createFromFormat('d-m-Y', $request->enddate)->startOfDay();
        $transactions = Transaction::selectRaw('
            transactions.*,
            frames.name as frame,
            CASE 
                WHEN transactions.paid_off = 0 THEN "Not paid off" 
                WHEN transactions.paid_off = 1 THEN "Paid off"     
            END AS paid_off
        ')
            ->leftjoin('frames','transactions.frame_id', '=', 'frames.id')
            ->whereBetween('order_date', [$start, $end])
            ->orderBy('order_date', 'asc')
            ->get();

        if ($transactions->count()) {
            return response()->json(['status'=>'ok',  $transactions]);
        } else {
            return response()->json([
                'errors' => ['message' => [__('data not found')]]
            ], 404);
        }
    }

    public function download(Request $request)
    {   
        $start = Carbon::createFromFormat('d-m-Y', $request->startdate)->startOfDay();
        $end = Carbon::createFromFormat('d-m-Y', $request->enddate)->startOfDay();
        $transactions = Transaction::selectRaw('
            transactions.*,
            frames.name as frame,
            CASE 
                WHEN transactions.paid_off = 0 THEN "Not paid off" 
                WHEN transactions.paid_off = 1 THEN "Paid off"     
            END AS paid_off
        ')
            ->leftjoin('frames','transactions.frame_id', '=', 'frames.id')
            ->whereBetween('order_date', [$start, $end])
            ->orderBy('order_date', 'asc')
            ->get();

        return \Excel::download(new \App\Exports\TransactionExport($transactions, $start, $end), __('DataCustomer').'_'.time().'.xlsx');
    }
}
