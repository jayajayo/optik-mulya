<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\Traits\PageTrait;
use App\Http\Requests\User\StoreRequest;
use App\Http\Requests\User\UpdateRequest;
use Spatie\Permission\Models\Role;
use App\Team;
use Auth;

class UserController extends Controller
{
    use PageTrait;

    public function __construct()
    {
        $this->middleware('permission:delete_user', ['only' => ['destroy']]);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $keyword = $request->keyword ? $request->keyword : null;
        $per_page = $request->per_page ? $request->per_page : 10;

        $order_list = ['name', 'nickname', 'username', 'address', 'role', 'team', 'status'];
        $orderby = in_array($request->order_by, $order_list) ? $request->order_by : 'name';
        $order = $request->order == 'desc' ? 'desc' : 'asc';

        $users = User::selectRaw('
            users.*,
            teams.name as team,
            roles.name as role,
            CASE 
                WHEN users.status = 0 THEN "Inactive" 
                WHEN users.status = 1 THEN "Active"     
            END AS status
        ')
            ->where('users.id', '!=', 1)
            ->leftjoin('teams','teams.id','=','users.team_id')
            ->leftjoin('model_has_roles','users.id','=','model_has_roles.model_id')
            ->leftjoin('roles','model_has_roles.role_id','=','roles.id')
            ->when(!(Auth::user()->hasRole('owner') || Auth::user()->hasRole('superadmin') ), function($q) {
                $q->where(function ($orq) {
                    $orq->where('team_id', '!=', 5)
                        ->orWhereNull('team_id');
                });
        })
            ->when(!empty($keyword), function($q) use ($keyword){
                $q->where(function ($orq) use ($keyword) {
                    $orq->where('users.name', 'like', '%'.$keyword.'%')
                        ->orWhere('nickname', 'like', '%'.$keyword.'%')
                        ->orWhere('address', 'like', '%'.$keyword.'%')
                        ->orWhere('phone', 'like', '%'.$keyword.'%')
                        ->orWhere('username', 'like', '%'.$keyword.'%')
                        ->orWhere('teams.name', 'like', '%'.$keyword.'%')
                        ->orWhere('roles.name', 'like', '%'.$keyword.'%');
                });
        })
            ->orderBy($orderby, $order)
            ->paginate($per_page);

        $links = $this->serializeLink($request, ['keyword', 'per_page', 'order_by', 'order']);
        $users->appends($links)->links();
        return view('user.index', compact('users','keyword'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $roles = Role::get();
        $teams = Team::where('id','!=',5)->get();
        return view('user.create', compact('roles','teams'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreRequest $request)
    {
        $user = new User;
        $user->name = $request->{__('fullname')};
        $user->nickname = $request->{__('nickname')};
        $user->username = $request->{__('username')};
        $user->address = $request->{__('address')};
        $user->phone = $request->{__('mobile_phone')};
        $user->password = bcrypt($request->{__('password')});
        $role = $request->{__('role')};
        switch($role){
            case 'market' : $user->team_id = $request->{__('team')};break;
            case 'owner' : $user->team_id = 5;break;
            case 'admin' : $user->team_id = 5;break;
            default:break;
        }
        $user->email = $user->username.'@optik.com';
        $user->status = 0;
        $user->save();
        $user->syncRoles([$role]);

        $request->session()->flash('status', __('Success'));
        return response()->json(['message'=> __('Success')]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $roles = Role::when(!(Auth::user()->hasRole('owner') || Auth::user()->hasRole('superadmin') ), function($q){
            $q->where('name', '!=', 'owner');
        })->get();
        $teams = Team::where('id','!=',5)->get();
        $user = User::where('id',$id)
            ->when(!(Auth::user()->hasRole('owner') || Auth::user()->hasRole('superadmin') ), function($q) {
                $q->where(function ($orq){
                    $orq->where('team_id', '!=', 5)
                        ->orWhereNull('team_id');
                });
            })
            ->first();
        return view('user.edit', compact('user', 'roles','teams'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateRequest $request, $id)
    {
        $user = User::find($id);
        $user->name = $request->{__('fullname')};
        $user->nickname = $request->{__('nickname')};
        $user->address = $request->{__('address')};
        $user->phone = $request->{__('mobile_phone')};
        $newPassword = $request->get('password');
        if (!empty($newPassword)) {
            $user->password = bcrypt($request->{__('password')});
        }
        $role = $request->{__('role')};
        switch($role){
            case 'market' : $user->team_id = $request->{__('team')}; break;
            case 'owner' : $user->team_id = 5; break;
            case 'admin' : $user->team_id = 5; break;
            default: $user->team_id = null; break;
        }
        $user->save();
        $user->syncRoles([$role]);

        $request->session()->flash('status', __('Success'));
        return response()->json(['message'=> __('Success')]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $user = User::find($id);
        $user->delete();

        return redirect()->route('user.index')->with('status',__('Success'));
    }

    public function activation(Request $request)
    {
        $user = User::find($request->user_activation);
        $status = $request->activation == 'active' ? 1 : 0;
        $user->status = $status;
        $user->save();

        return redirect()->route('user.index')->with('status',__('Success'));
    }

    public function listUser()
    {
        $users = User::all();
        return response()->json($users);
    }
}
