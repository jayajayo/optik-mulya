<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Payment;
use Illuminate\Support\Carbon;
use App\Traits\PageTrait;
use App\Transaction;
use App\Type;
use App\Income;
use Auth;
use App\Http\Requests\Deposit\UpdateBillRequest;
use PDF;

class DepositController extends Controller
{
    use PageTrait;

    public function billList(Request $request)
    {
        $keyword = $request->keyword ? $request->keyword : null;
        $per_page = $request->per_page ? $request->per_page : 10;
        $tomorrow = Carbon::today()->addDays(2);

        $payments = Payment::whereHas('transaction', function($q) {
            $q->where('paid_off', 0);
        })  ->when(!empty($keyword), function($q) use ($keyword){
            $q->where(function ($orq) use ($keyword) {
                $orq->where('name', 'like', '%'.$keyword.'%')
                    ->orWhereHas('transaction', function ($nq) use ($keyword) {
                        $nq->where('name', 'like', '%'.$keyword.'%');
                        $nq->orWhere('address', 'like', '%'.$keyword.'%');
                        $nq->orWhere('tempo', 'like', '%'.$keyword.'%');
                        $nq->orWhere('code', 'like', '%'.$keyword.'%');
                });
            });
        })
            ->where('status','unpaid')
            ->whereNull('recipient_id')
            ->where('charge_date', '<', $tomorrow)
            ->paginate($per_page);

        $links = $this->serializeLink($request, ['keyword', 'per_page']);
        $payments->appends($links)->links();
        return view('deposit.bill-list', compact('payments','keyword'));
    }

    public function formBill($id)
    {
        $payment = Payment::find($id);
        $types = Type::all();
        return view('deposit.form-edit-bill', compact('payment','types'));
    }

    public function updateBill(UpdateBillRequest $request, $id)
    {
        $payment = Payment::find($id);
        $payment->nomine = $request->{__('nomine')};
        $payment->information = $request->{__('information')};
        $payment->save();

        $request->session()->flash('status', __('Success'));
        return response()->json(['message'=> __('Success')]);
    }

    public function depositProcess(Request $request)
    {
        $user = Auth::user();
        $tomorrow = Carbon::today()->addDays(2);

        $payments = Payment::whereHas('transaction', function($q) {
            $q->where('paid_off', 0);
        })
            ->where('status','unpaid')
            ->whereNull('recipient_id')
            ->where('charge_date', '<', $tomorrow)
            ->get();
        
        $count = 0;

        foreach ($payments as $payment){
            if ($payment->nomine != 0) {
                $count++;
                $payment->recipient_id = $user->id;
                $payment->payment_date = Carbon::today();
                $payment->status = "paid";
                $payment->save();
            }
        }

        if ($count) {
            $request->session()->flash('status', __('Success'));
            return response()->json(['message'=> __('Success')]);
        }

        return response('error', 400);
    }

    public function paymentList(Request $request)
    {
        $keyword = $request->keyword ? $request->keyword : null;
        $per_page = $request->per_page ? $request->per_page : 2000;
        $tomorrow = Carbon::tomorrow();

        $payments = Payment::whereHas('transaction', function($q) {
            $q->where('paid_off', 0);
        })  ->when(!empty($keyword), function($q) use ($keyword){
            $q->where(function ($orq) use ($keyword) {
                $orq->where('name', 'like', '%'.$keyword.'%')
                    ->orWhereHas('transaction', function ($nq) use ($keyword) {
                        $nq->where('name', 'like', '%'.$keyword.'%');
                        $nq->orWhere('address', 'like', '%'.$keyword.'%');
                        $nq->orWhere('tempo', 'like', '%'.$keyword.'%');
                        $nq->orWhere('code', 'like', '%'.$keyword.'%');
                });
            });
        })
            ->where('status','paid')
            ->where('recipient_id', Auth::user()->id)
            ->where('payment_date', '<', $tomorrow)
            ->orderBy('updated_at','desc')
            ->paginate($per_page);

        
        // $tunai = Payment::where('information','Cash')->where('status','paid')->where('recipient_id', Auth::user()->id)
        // ->where('payment_date', '<', $tomorrow)
        // ->orderBy('updated_at','desc')
        // ->paginate($per_page)->sum('nomine');
        $tunai = Payment::whereHas('transaction', function($q) {
            $q->where('paid_off', 0);
        })  ->when(!empty($keyword), function($q) use ($keyword){
            $q->where(function ($orq) use ($keyword) {
                $orq->where('name', 'like', '%'.$keyword.'%')
                    ->orWhereHas('transaction', function ($nq) use ($keyword) {
                        $nq->where('name', 'like', '%'.$keyword.'%');
                        $nq->orWhere('address', 'like', '%'.$keyword.'%');
                        $nq->orWhere('tempo', 'like', '%'.$keyword.'%');
                        $nq->orWhere('code', 'like', '%'.$keyword.'%');
                });
            });
        })
            ->where('status','paid')
            ->where('recipient_id', Auth::user()->id)
            ->where('payment_date', '<', $tomorrow)
            ->where('information', 'Cash')
            ->sum('nomine');
        $transfer = Payment::whereHas('transaction', function($q) {
            $q->where('paid_off', 0);
        })  ->when(!empty($keyword), function($q) use ($keyword){
            $q->where(function ($orq) use ($keyword) {
                $orq->where('name', 'like', '%'.$keyword.'%')
                    ->orWhereHas('transaction', function ($nq) use ($keyword) {
                        $nq->where('name', 'like', '%'.$keyword.'%');
                        $nq->orWhere('address', 'like', '%'.$keyword.'%');
                        $nq->orWhere('tempo', 'like', '%'.$keyword.'%');
                        $nq->orWhere('code', 'like', '%'.$keyword.'%');
                });
            });
        })
            ->where('status','paid')
            ->where('recipient_id', Auth::user()->id)
            ->where('payment_date', '<', $tomorrow)
            ->where('information','!=', 'Cash')
            ->sum('nomine');

        $links = $this->serializeLink($request, ['keyword', 'per_page']);
        $payments->appends($links)->links();
        return view('deposit.payment-list', compact('payments','keyword','tunai','transfer'));
    }

    public function cetak_pdf(Request $request){
        $keyword = $request->keyword ? $request->keyword : null;
        $per_page = $request->per_page ? $request->per_page : 2000;
        $tomorrow = Carbon::tomorrow();

        $payments = Payment::whereHas('transaction', function($q) {
            $q->where('paid_off', 0);
        })  ->when(!empty($keyword), function($q) use ($keyword){
            $q->where(function ($orq) use ($keyword) {
                $orq->where('name', 'like', '%'.$keyword.'%')
                    ->orWhereHas('transaction', function ($nq) use ($keyword) {
                        $nq->where('name', 'like', '%'.$keyword.'%');
                        $nq->orWhere('address', 'like', '%'.$keyword.'%');
                        $nq->orWhere('tempo', 'like', '%'.$keyword.'%');
                        $nq->orWhere('code', 'like', '%'.$keyword.'%');
                });
            });
        })
            ->where('status','paid')
            ->where('recipient_id', Auth::user()->id)
            ->where('payment_date', '<', $tomorrow)
            ->orderBy('name','asc')
            ->paginate($per_page);

        $links = $this->serializeLink($request, ['keyword', 'per_page']);
        $payments->appends($links)->links();
        // $pdf = PDF::loadView('payment_pdf',['payments'=>$payments]);
        
        // return $pdf->download('daftar-terima-pdf');
        return view('deposit.payment_pdf',['payments'=>$payments]);
    }

    public function paymentForm()
    {
        $transactions = Transaction::where('paid_off', 0)->get();
        $types = Type::all();
        // $types = Type::find($id);
        return view('deposit.add-payment', compact('transactions','types'));
    }

    public function deletePayment(Request $request, $id)
    {
        $payment = Payment::find($id);
        $payment->recipient_id = null;
        $payment->nomine = 0;
        $payment->payment_date = null;
        $payment->information = null;
        $payment->status = "unpaid";
        $payment->save();

        $request->session()->flash('status', __('Success'));
        return response()->json(['message'=> __('Success')]);
    }

    public function getPayment($id)
    {
        $payments = Payment::where('transaction_id', $id)->where('status','unpaid')->orderBy('id','asc')->get();
        return response()->json($payments);
    }

    public function getTransaction($id)
    {
        $trans = Transaction::where('id', $id)->first();
        return response()->json($trans);
    }

    public function addPayment(Request $request)
    {
        $payment = Payment::find($request->{__('code')});
        // $types = Type::where();
        $info = 'Cash';
        $code = '1M';
        // foreach($types as $type){
            if($request->{ __('information')} != 'cash') {
                $info='transfer';
            }
        // }
        
        // if($request->{__('information')} == )
       
        $payment->nomine = $request->{__('nomine')};
        $payment->information = $request->{__('information')};
        $payment->recipient_id = Auth::user()->id;
        $payment->payment_date = Carbon::today();
        $payment->status = "paid";
        $payment->memo = $request->{__('memo')};
        $payment->save();

        // $income = Income::create([
        //     'information' => $info,
        //     'code' => $code,
        //     'payment_id' => $payment->id,
        //     'nomine' => $request->{__('nomine')},
        //     'memo' => $request->{__('memo')}
        // ]);
        // dd($payment);

        $request->session()->flash('status', __('Success'));
        return response()->json(['message'=> __('Success')]);
    }

    public function createIncome(){
        $incomes = Income::get();
        return view('deposit.create-income')->withData($incomes);
    }

    public function inputIncome(Request $request){
            $incomes = new Income;
            $incomes->code = $request->{__('code')};
            $incomes->information = $request->{__('information')};
            $incomes->nomine = $request->{__('nomine')};
            $incomes->memo = $request->{__('memo')};
            $incomes->payment_date = $request->payment_date;
            $incomes->save();
            // dd($payment);
    
            $request->session()->flash('status', __('Success'));
            return response()->json(['message'=> __('Success')]);
    }
}
