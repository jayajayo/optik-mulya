<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Traits\PageTrait;
use App\Frame;
use App\Team;
use App\MerkFrame;
use App\Http\Requests\Frame\StoreRequest;
use App\Http\Requests\Frame\UpdateRequest;

class FrameController extends Controller
{
    use PageTrait;

    public function __construct()
    {
        $this->middleware('role:superadmin|owner|admin', ['only' => ['destroy']]);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $keyword = $request->keyword ? $request->keyword : null;
        $per_page = $request->per_page ? $request->per_page : 10;

        $order_list = ['merk', 'name', 'a', 'b', 'c', 'd', 'e', 'stock', 'total'];
        $orderby = in_array($request->order_by, $order_list) ? $request->order_by : 'merk';
        $order = $request->order == 'desc' ? 'desc' : 'asc';

        $get_teams = Team::all();
        $team_query = '';
        $team_inventory = '';
        foreach ($get_teams as $team){                
            $string_query = 'coalesce(sum(case when team_id = '.$team->id.' then quantity end), 0)'; 
            $team_query .=  $string_query.'as '.$team->code.' , ';
            $team_inventory .= $string_query.'+ '; 
        }
        
        $frames = Frame::selectRaw('
                frames.*,
                merk_frames.name as merk,
                '.$team_query.'
                (   frames.stock + '.$team_inventory.' 0
                ) as total
            ')
            ->groupBy('frames.id')
            ->groupBy('frames.merk_frame_id')
            ->groupBy('frames.name')
            ->groupBy('frames.stock')
            ->groupBy('frames.created_at')
            ->groupBy('frames.updated_at')
            ->groupBy('frames.deleted_at')
            ->groupBy('merk_frames.name')
            ->leftjoin('frame_team','frame_team.frame_id', '=', 'frames.id')
            ->leftjoin('merk_frames','merk_frames.id' , '=', 'frames.merk_frame_id')
            ->when(!empty($keyword), function($q) use ($keyword){
                $q->where(function ($orq) use ($keyword) {
                    $orq->where('frames.name', 'like', '%'.$keyword.'%')
                        ->orWhere('merk_frames.name', 'like', '%'.$keyword.'%');
                });
            })  
            ->orderBy($orderby, $order)
            ->orderBy('name', 'asc')
            ->paginate($per_page);

        $teams = Team::orderBy('order','asc')->get();
        
        $links = $this->serializeLink($request, ['keyword', 'per_page', 'order_by', 'order']);
        $frames->appends($links)->links();
        return view('frame.index', compact('frames','teams','keyword'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $merkframes = MerkFrame::all();
        $teams = Team::orderBy('order','asc')->get();
        
        return view('frame.create', compact('merkframes','teams'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreRequest $request)
    {
        $frame = new Frame;
        $frame->merk_frame_id = $request->{__('merk')};
        $frame->name = $request->{__('code_frame')};
        $frame->stock = $request->stock;
        $frame->save();

        $assignvalue = [];
        foreach ($request->team as $kteam => $vteam){
            $assignvalue[$kteam] = [ 'quantity' => $vteam ];
        }

        $frame->teams()->sync($assignvalue);

        $request->session()->flash('status', __('Success'));
        return response()->json(['message'=> __('Success')]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $merkframes = MerkFrame::all();
        $teams = Team::orderBy('order','asc')->get();
        $frame = Frame::find($id);

        return view('frame.edit', compact('frame','merkframes','teams'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateRequest $request, $id)
    {
        $frame = Frame::find($id);
        $frame->merk_frame_id = $request->{__('merk')};
        $frame->name = $request->{__('code_frame')};
        $frame->stock = $request->stock;
        $frame->save();

        $assignvalue = [];
        foreach ($request->team as $kteam => $vteam){
            $assignvalue[$kteam] = [ 'quantity' => $vteam ];
        }

        $frame->teams()->sync($assignvalue);

        $request->session()->flash('status', __('Success'));
        return response()->json(['message'=> __('Success')]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $id)
    {
        $frame = Frame::find($id);
        $frame->delete();

        $request->session()->flash('status', __('Success'));
        return redirect()->back();
    }

    public function getByMerk($id, $team_id = null)
    {
        $frames = Frame::with(['userteam'])->where('merk_frame_id', $id)->get();
        $return = [];
        foreach ($frames as $frame){
            $stock_user = isset($frame->userteam[0]->pivot->quantity) ? $frame->userteam[0]->pivot->quantity : 0;
            $stock_id = ($team_id && $frame->userteamid($team_id)[0]->pivot->quantity) ? $frame->userteamid($team_id)[0]->pivot->quantity : 0;
            $return[] = [
                'id' => $frame->id,
                'name' => $frame->name,
                'stock' => $team_id ? $stock_id : $stock_user,
            ];
        }
        return response()->json($return);
    }


    public function download(Request $request)
    {     
        $get_teams = Team::all();
        $team_query = '';
        $team_inventory = '';
        foreach ($get_teams as $team){                
            $string_query = 'coalesce(sum(case when team_id = '.$team->id.' then quantity end), 0)'; 
            $team_query .=  $string_query.'as '.$team->code.' , ';
            $team_inventory .= $string_query.'+ '; 
        }

        $frames = Frame::selectRaw('
                frames.*,
                merk_frames.name as merk,
                '.$team_query.'
                (   frames.stock + '.$team_inventory.' 0
                ) as total
            ')
            ->groupBy('frames.id')
            ->groupBy('frames.merk_frame_id')
            ->groupBy('frames.name')
            ->groupBy('frames.stock')
            ->groupBy('frames.created_at')
            ->groupBy('frames.updated_at')
            ->groupBy('frames.deleted_at')
            ->groupBy('merk_frames.name')
            ->leftjoin('frame_team','frame_team.frame_id', '=', 'frames.id')
            ->leftjoin('merk_frames','merk_frames.id' , '=', 'frames.merk_frame_id')
            ->orderBy('merk', 'asc')
            ->orderBy('name', 'asc')
            ->get();
        
        $teams = Team::orderBy('order','asc')->get();
        return \Excel::download(new \App\Exports\FrameExport($frames, $teams), __('Daftar-frame').'-'.time().'.xlsx');
    }
}
