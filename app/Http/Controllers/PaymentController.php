<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Enums\TransactionEnum;
use App\User;
use App\Payment;
use App\Type;
use App\Transaction;
use Carbon\Carbon;
use App\Http\Requests\Payment\UpdateReportRequest;

class PaymentController extends Controller
{
    public function dailyPayment(Request $request)
    {
        $users = User::where('id','!=',1)->get();
        $types = Type::get();
        //$methods = TransactionEnum::paymethods;
        
        $user = !empty($request->user) ? $request->user : null;
        $payment_date = !empty($request->payment_date) ? Carbon::createFromFormat('d-m-Y', $request->payment_date)->startOfDay() : Carbon::today();
        $payment_method = !empty($request->payment_method) ? $request->payment_method : null;
        
        $payments = Payment::where('status','paid')
            ->when(!empty($user), function($q) use ($user){
            $q->where('recipient_id', $user);
        })  ->when(!empty($payment_date), function($q) use ($payment_date){
            $q->whereDate('payment_date', $payment_date);
        })  ->when(!empty($payment_method), function($q) use ($payment_method){
            $q->where('information', $payment_method);
        })  ->get();

        if ($payments->count() && $request->accept == "accept") {
            foreach ($payments as $payment) {
                $payment->status = "accepted";
                $payment->save();
                $paid_transaction = $payment->transaction;

                if ($paid_transaction->payments()->where('status','accepted')->sum('nomine') >= $payment->transaction->total_payment ){
                    $paid_transaction = $payment->transaction;
                    $paid_transaction->paid_off = 1;
                    $paid_transaction->save();
                }
            }

            $request->session()->flash('status', __('Success'));
            return redirect('/payment/payment-report?payment_date='.$payment_date->format('d-m-Y'));
        }

        return view('payment.daily-payment', compact('payments','users','types','payment_method'));
    }    

    public function dailyPaymentDownload(Request $request)
    {
        $users = User::where('id','!=',1)->get();
        //$methods = TransactionEnum::paymethods;
        
        $user = !empty($request->user) ? $request->user : null;
        $payment_date = !empty($request->payment_date) ? Carbon::createFromFormat('d-m-Y', $request->payment_date)->startOfDay() : Carbon::today();
        $payment_method = !empty($request->payment_method) ? $request->payment_method : null;
        
        $payments = Payment::where('status','paid')
            ->when(!empty($user), function($q) use ($user){
            $q->where('recipient_id', $user);
        })  ->when(!empty($payment_date), function($q) use ($payment_date){
            $q->whereDate('payment_date', $payment_date);
        })  ->when(!empty($payment_method), function($q) use ($payment_method){
            $q->where('information', $payment_method);
        })  ->get();

        return \Excel::download(new \App\Exports\DailyPaymentExport($payments, $payment_date), __('Payment').'_'.time().'.xlsx');
    }    

    public function getTransaction($id)
    {
        $trans = Transaction::where('id', $id)->first();
        return response()->json($trans);
    }

    public function getPayment($id)
    {
        $payments = Payment::where('transaction_id', $id)->where('status','unpaid')->orderBy('id','asc')->get();
        return response()->json($payments);
    }

    public function paymentReport(Request $request)
    {
        $users = User::where('id','!=',1)->get();
        //$methods = TransactionEnum::paymethods;
        
        $user = !empty($request->user) ? $request->user : null;
        $payment_id = !empty($request->payment_id) ? $request->payment_id : null;
        $payment_date_start = !empty($request->payment_date_start) ? Carbon::createFromFormat('d-m-Y', $request->payment_date_start)->startOfDay() : Carbon::today();
        $payment_date_end = !empty($request->payment_date_end) ? Carbon::createFromFormat('d-m-Y', $request->payment_date_end)->startOfDay() : Carbon::today();
        $payment_method = !empty($request->payment_method) ? $request->payment_method : null;
        
        $types = Type::all();
        $payments = Payment::where('status','accepted')
            ->when(!empty($payment_id), function($q) use ($payment_id){
            $q->where('name', $payment_id);
        })  ->when(!empty($user), function($q) use ($user){
            $q->where('recipient_id', $user);
        })  ->when(!empty($payment_date_start), function($q) use ($payment_date_start){
            $q->whereDate('payment_date', '>=', $payment_date_start);
        })  ->when(!empty($payment_date_end), function($q) use ($payment_date_end){
            $q->whereDate('payment_date', '<=', $payment_date_end);
        })  ->when(!empty($payment_method), function($q) use ($payment_method){
            $q->where('information', $payment_method);
        })  ->get();



        // dd($payments);
        return view('payment.payment-report', compact('payments','users','payment_method','types'));
    }

    public function deletePayment(Request $request, $id)
    {
        $data = Payment::find($id);
        $data->recipient_id = null;
        $data->nomine = null;
        $data->payment_date = null;
        $data->information = null;
        $data->status = 'unpaid';
        $data->save();
        //$data->delete();

        $request->session()->flash('status', __('Success'));
        return redirect()->back();
    }

    public function editpaymentReport($id, Request $request)
    {
        // $title = 'Expertise';
        // $data = Expertise::where('id', $id)->first();
        // return view('expertise::ubah')->withTitle($title)->withData($data);

        // dd($payments);
        $data = Payment::where('id', $id)->first();
        $payments = Payment::where('transaction_id',$data->transaction_id)->where('status', 'unpaid');
        $payment = Payment::where('id', $id)->union($payments)->get();
        $users = User::where('id','!=',1)->get();
        $transac = Transaction::all();
        return view('payment.edit-payment')->withData($data)->withUser($users)->withTransaction($transac)->withPayment($payment);
    }

    public function updatepaymentReport($id, Request $request)
    {

        //dd($request->{__('code')}." ".$request->{__('no_nota')});
        $query = Payment::where('name', $request->{__('code')})
        ->where('transaction_id', $request->{__('no_nota')})
        ->get();
        $getvalid = Payment::where('id', $id)->first();
        //dd($getvalid);
        $id_ = $query[0]->id;

        $datas = Payment::find($id_);
        //$datas->transaction_id = $request->{__('no_nota')};
        //$datas->name = $request->{__('code')};
        $datas->recipient_id = $request->{__('petugas')};
        $datas->nomine = $request->{__('nominal')};
        $datas->payment_date = $getvalid->payment_date;
        $datas->information = $getvalid->information;
        $datas->status = 'accepted';
        $datas->save();

        if($id_ != $id)
        {
            $data = Payment::find($id);
            $data->recipient_id = null;
            $data->nomine = null;
            $data->payment_date = null;
            $data->information = null;
            $data->status = 'unpaid';
            $data->save();
        };
        // dd($data);
        $request->session()->flash('status', __('Success'));
        //return redirect()->back();
    }

    public function paymentInfo()
    {
        $types = Type::all();

        // $payment_method = !empty($request->payment_method) ? $request->payment_method : null;
        
        // $payments = Payment::where('status','accepted')
        //     ->when(!empty($payment_method), function($q) use ($payment_method){
        //     $q->where('information', $payment_method);
        // })  ->get();

        // dd($payments);
        return view('payment.payment-info', compact('types'));
    }

    public function createPembayaran(){
        $types = Type::get();
        
        return view('payment.create-pembayaran')->withData($types);
    }

    public function inputPembayaran(Request $request){
        $types = new Type;
        $types->code = $request->code;
        $types->save();
        $request->session()->flash('status', __('Success'));
        return redirect()->back();
    }

    public function deleteInfo(Request $request, $id)
    {
        $types = Type::find($id);
        
        $types->delete();

        $request->session()->flash('status', __('Success'));
        return redirect()->back();
    }

    public function editInfo($id, Request $request)
    {
        $types = Type::where('id', $id)->first();
        return view('payment.edit-info')->withData($types);
    }

    public function updateInfo(Request $request, $id)
    {
        $types = Type::find($id);
        $types->code = $request->{__('information')};
        $types->save();
        // return redirect()->back();
        $request->session()->flash('status', __('Success'));
        return redirect()->back();
    }


    public function paymentReportDownload(Request $request)
    {
        // dd($request->all());
        $users = User::where('id','!=',1)->get();
        //$methods = TransactionEnum::paymethods;
        
        $user = !empty($request->user) ? $request->user : null;
        $payment_id = !empty($request->payment_id) ? $request->payment_id : null;
        $payment_date_start = !empty($request->payment_date_start) ? Carbon::createFromFormat('d-m-Y', $request->payment_date_start)->startOfDay() : Carbon::today();
        $payment_date_end = !empty($request->payment_date_end) ? Carbon::createFromFormat('d-m-Y', $request->payment_date_end)->startOfDay() : Carbon::today();
        $payment_method = !empty($request->payment_method) ? $request->payment_method : null;
        
        $payments = Payment::where('status','accepted')
            ->when(!empty($payment_id), function($q) use ($payment_id){
            $q->where('name', $payment_id);
        })  ->when(!empty($user), function($q) use ($user){
            $q->where('recipient_id', $user);
        })  ->when(!empty($payment_date_start), function($q) use ($payment_date_start){
            $q->whereDate('payment_date', '>=', $payment_date_start);
        })  ->when(!empty($payment_date_end), function($q) use ($payment_date_end){
            $q->whereDate('payment_date', '<=', $payment_date_end);
        })  ->when(!empty($payment_method), function($q) use ($payment_method){
            $q->where('information', $payment_method);
        })  ->get();

        // dd($payments);
        return \Excel::download(new \App\Exports\PaymentReportExport($payments, $payment_date_start, $payment_date_end), __('Payment').'_'.time().'.xlsx');
    }    
}
