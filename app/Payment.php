<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Payment extends Model
{
    use SoftDeletes;
    
    protected $dates = [
        'payment_date',
        'charge_date',
        'created_at',
        'updated_at',
        'deleted_at',
    ];

    public function transaction()
    {
        return $this->belongsTo('App\Transaction');
    }

    public function income()
    {
        return $this->belongsTo('App\Income')->withTrashed();
    }

    public function user()
    {
        return $this->belongsTo('App\User');
    }

    public function type()
    {
        return $this->belongsTo('App\Type');
    }

    public function recipient()
    {
        return $this->belongsTo('App\User', 'recipient_id');
    }
}
