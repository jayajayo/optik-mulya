<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Omset extends Model
{
    use SoftDeletes;
    
    protected $table = 'omsets';

    protected $fillable = [
        'transaction_id','team_id','plus','minus'];

    

    protected $dates = ['deleted_at'];

    public function transaction()
    {
        return $this->belongsTo(Transaction::class);
    }

    public function team()
    {
        return $this->belongsTo(Team::class);
    }
}
