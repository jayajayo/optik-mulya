<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class MerkFrame extends Model
{
    use SoftDeletes;

    public function frames()
    {
        return $this->hasMany('App\Frame');
    }

}
