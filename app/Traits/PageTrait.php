<?php

namespace App\Traits;

use Illuminate\Http\Request;

trait PageTrait 
{
    public function serializeLink(Request $request, $list)
    {
        $return = [];
        foreach ($list as $item) {
            if (!empty($request->{$item}))
            $return[$item] = $request->{$item};  
        }
        return $return;
    }
}