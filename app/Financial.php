<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Income;

class Financial extends Model
{
    protected $table = 'financials';
    use SoftDeletes;
    public function income()
    {
        return $this->belongsTo(Income::class)->withTrashed();
    }
}
