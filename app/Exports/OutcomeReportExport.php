<?php 

namespace App\Exports;

use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Illuminate\Support\Carbon;

class OutcomeReportExport implements FromView, ShouldAutoSize
{
    public function __construct($outcome, $outcomes, $start=null, $end=null)
    {
        $this->start = is_null($start) ? '' : $start;
        $this->end = is_null($end) ? '' : $end;
        // $this->omsets = $omsets;
        $this->outcome = $outcome;
    }

    public function view(): View
    {
        setlocale (LC_TIME, 'id_ID');
        Carbon::setLocale('id');

        return view('exports.outcome-report', [
            'outcome' => $this->outcome,
            // 'omsets' => $this->omsets,
            'start' => $this->start,
            'end' => $this->end ,
        ]);
    }
}