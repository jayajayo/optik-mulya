<?php 

namespace App\Exports;

use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Illuminate\Support\Carbon;

class TransactionExport implements FromView, ShouldAutoSize
{
    public function __construct($transactions, $start=null, $end=null)
    {
        $this->start = is_null($start) ? '' : $start;
        $this->end = is_null($end) ? '' : $end;
        $this->transaction = $transactions;
    }

    public function view(): View
    {
        setlocale (LC_TIME, 'id_ID');
        Carbon::setLocale('id');

        return view('exports.transaction', [
            'transactions' => $this->transaction,
            'start' => $this->start,
            'end' => $this->end ,
        ]);
    }
}