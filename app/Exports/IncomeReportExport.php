<?php 

namespace App\Exports;

use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Illuminate\Support\Carbon;

class IncomeReportExport implements FromView, ShouldAutoSize
{
    public function __construct($sorted, $incomes, $start=null, $end=null)
    {
        $this->start = is_null($start) ? '' : $start;
        $this->end = is_null($end) ? '' : $end;
        // $this->omsets = $omsets;
        $this->sorted = $sorted;
    }

    public function view(): View
    {
        setlocale (LC_TIME, 'id_ID');
        Carbon::setLocale('id');

        return view('exports.income-report', [
            'sorted' => $this->sorted,
            // 'omsets' => $this->omsets,
            'start' => $this->start,
            'end' => $this->end ,
        ]);
    }
}