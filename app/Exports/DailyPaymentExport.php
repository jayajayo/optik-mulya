<?php 

namespace App\Exports;

use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Illuminate\Support\Carbon;

class DailyPaymentExport implements FromView, ShouldAutoSize
{
    public function __construct($payments, $date)
    {
        $this->date = is_null($date) ? '' : $date;
        $this->payments = $payments;
    }

    public function view(): View
    {
        setlocale (LC_TIME, 'id_ID');
        Carbon::setLocale('id');
        
        return view('exports.daily-payment', [
            'payments' => $this->payments,
            'date' => $this->date,
        ]);
    }
}