<?php 

namespace App\Exports;

use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Illuminate\Support\Carbon;

class FrameExport implements FromView, ShouldAutoSize
{
    public function __construct($frames, $teams)
    {
        $this->frames = $frames;
        $this->teams = $teams;
    }

    public function view(): View
    {
        return view('exports.frame', [
            'frames' => $this->frames,
            'teams' => $this->teams    
        ]);
    }
}