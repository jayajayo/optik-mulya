<?php 

namespace App\Exports;

use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Illuminate\Support\Carbon;

class PaymentReportExport implements FromView, ShouldAutoSize
{
    public function __construct($payments, $start=null, $end=null)
    {
        $this->start = is_null($start) ? '' : $start;
        $this->end = is_null($end) ? '' : $end;
        $this->payments = $payments;
    }

    public function view(): View
    {
        setlocale (LC_TIME, 'id_ID');
        Carbon::setLocale('id');

        return view('exports.payment-report', [
            'payments' => $this->payments,
            'start' => $this->start,
            'end' => $this->end ,
        ]);
    }
}