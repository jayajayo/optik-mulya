<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Transaction extends Model
{
    use SoftDeletes;
    
    protected $dates = [
        'order_date',
        'finish_date',
        'charge_date',
        'created_at',
        'updated_at',
        'deleted_at',
    ];

    public function user()
    {
        return $this->belongsTo('App\User')->withTrashed();
    }

    public function frame()
    {
        return $this->belongsTo('App\Frame')->withTrashed();
    }

    public function type()
    {
        return $this->belongsTo('App\Type')->withTrashed();
    }

    public function payments()
    {
        return $this->hasMany('App\Payment');
    }

    public function omset()
    {
        return $this->hasMany(Omset::Class);
    }
}
