<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Auth;

class Frame extends Model
{
    use SoftDeletes;

    public function merkFrame()
    {
        return $this->belongsTo('App\MerkFrame')->withTrashed();
    }

    public function transactions()
    {
        return $this->hasMany('App\Transaction');
    }

    public function teams()
    {
        return $this->belongsToMany('App\Team')->withPivot('quantity')->orderBy('order','asc');
    }

    public function userteam()
    {
        return $this->belongsToMany('App\Team')->withPivot('quantity')->where('team_id', Auth::user()->team_id);
    }

    public function userteamid($id)
    {
        return $this->belongsToMany('App\Team')->withPivot('quantity')->where('team_id', $id)->get();
    }
}
