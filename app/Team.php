<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Team extends Model
{
    public function users()
    {
        return $this->hasMany('App\User');
    }

    public function omset()
    {
        return $this->hasMany(Omset::class);
    }

    public function frames()
    {
        return $this->belongsToMany('App\Frame')->withPivot('quantity')->orderBy('order','asc');
    }
}
