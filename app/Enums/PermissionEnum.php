<?php

namespace App\Enums;

use MabeEnum\Enum;

class PermissionEnum extends enum
{
    const permissions = [
        'debt_show',
        'debt_download',
        'action_complaint',
        'manage_complaint',
        'acc_payment',
        'manage_payment',
        'manage_status',
        'show_status',
        'manage_deposit',
        'main_deposit',
        'manage_user',
        'delete_user',
        'manage_frame',
        'transaction_create',
        'transaction_edit',
        'transaction_show',
        'transaction_download',
        'add_income',
    ];

    const owner = [
        'debt_show',
        'debt_download',
        'manage_complaint',
        'acc_payment',
        'manage_payment',
        'manage_status',
        'show_status',
        'manage_deposit',
        'manage_user',
        'delete_user',
        'manage_frame',
        'transaction_edit',
        'transaction_create',
        'transaction_show',
        'transaction_download',
        'main_deposit',
    ];

    const admin = [
        'manage_status',
        'show_status',
        'debt_show',
        'manage_complaint',
        'debt_download',
        'acc_payment',
        'manage_payment',
        'manage_deposit',
        'manage_frame',
        'manage_user',
        'transaction_create',
        'transaction_show',
        'transaction_download',
        'main_deposit',
        'add_income',
    ];

    const penagih = [
        'show_status',
        'manage_complaint',
        'debt_show',
        'transaction_show',
        'manage_deposit',
        'main_deposit',
    ];

    const market = [
        'show_status',
        'acc_payment',
        'manage_complaint',
        'debt_show',
        'manage_deposit',
        'transaction_create',
        'transaction_show',
    ];

    const komplain = [
        'show_status',
        'action_complaint',
        'manage_complaint',
        'debt_show',
        'manage_deposit',
        'transaction_show',
    ];
 
    const roles = [
        'owner',
        'admin',
        'penagih',
        'market',
        'komplain'
    ];
}
