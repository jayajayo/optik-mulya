<?php

namespace App\Enums;

use MabeEnum\Enum;

class UserEnum extends enum
{
    const status = [
        0 => 'Inactive',
        1 => 'Active',
    ];

    static function getStatus($key)
    {
        $status = self::status;
        return isset($status[$key]) ? $status[$key] : '';
    }
}
