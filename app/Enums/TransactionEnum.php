<?php

namespace App\Enums;

use MabeEnum\Enum;

class TransactionEnum extends enum
{
    const status = [
        'Not paid off',
        'Paid off'
    ];

    const payments = ['dp','bj','ang2','ang3','ang4','ang5', 'ang6', 'ang7', 'ang8', 'ang9', 'ang10', 'ang11', 'ang12'];

    const paymethods = ['cash','bca','mandiri','bank jateng'];

    const tempo = ['cash', '2', '3', '4', '5', '6', '7', '8', '9', '10', '11', '12'];

    public static function getStatus($key){
        return isset(self::status[$key]) ? self::status[$key] : '' ;
    }
}
