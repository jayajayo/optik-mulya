<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Outcome extends Model
{
    public function financial()
    {
        return $this->hasMany('App\Financial')->withTrashed();
    }
}
