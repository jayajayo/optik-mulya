<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Spatie\Permission\Traits\HasRoles;
use Illuminate\Database\Eloquent\SoftDeletes;

class User extends Authenticatable
{
    use Notifiable, HasRoles, SoftDeletes;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];
    
    public function transactions()
    {
        return $this->hasMany('App\Transaction');
    }

    public function payments()
    {
        return $this->hasMany('App\Payment');
    }

    public function team()
    {
        return $this->belongsTo('App\Team');
    }

    public function complaints()
    {
        return $this->hasMany('App\Complaint');
    }
}
