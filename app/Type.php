<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Type extends Model
{
    //
    public function payments()
    {
        return $this->hasMany('App\Payment');
    }

    public function transaction()
    {
        return $this->hasMany('App\Transaction');
    }
}
