@extends('layouts.blank')
@section('content')
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.12.4/css/bootstrap-select.min.css">
<link href="{{ asset('vendor/datepicker/datepicker.min.css') }}" rel="stylesheet">
<script src="{{ asset('vendor/datepicker/datepicker.min.js') }}"></script>

<div class="alert-complaint-form">
</div>
<form class="form-horizontal" id="create-complaint" role="form">
    <div class="form-group">
        <label class="control-label col-sm-2">{{__('Date')}}</label>
        <div class="col-sm-10">
            <input class="form-control" readonly="true" data-toggle="datepicker" name="{{__('date')}}" value="{{\Carbon\Carbon::today()->format('d-m-Y')}}">
        </div>
    </div>
    <div class="form-group">
        <label class="control-label col-sm-2">{{__('Cari nota')}}</label>
        <div class="col-sm-10">
            <div class="input-group">
                <input type="text" name="keyword" class="form-control keyword"  placeholder="{{__('Cari nota')}}" value="">
                <span class="input-group-btn">
                    <button class="btn btn-default search-invoice"><i class="fa fa-search"></i></button>
                </span>
            </div>
        </div>
    </div>
    <div class="form-group">
        <label class="control-label col-sm-2">{{__('Invoice number')}}</label>
        <div class="col-sm-10 ins-select-invoice">
            <select class="selectpickers form-control select-invoice" data-live-search="true" name="{{__('invoice_number')}}"></select>
        </div>
    </div>
    <div class="form-group">
        <label class="control-label col-sm-2">{{__('Name')}}</label>
        <div class="col-sm-10">
            <input type="text" readonly="true" class="form-control auto-input name" name="{{__('name')}}">
        </div>
    </div>
    <div class="form-group">
        <label class="control-label col-sm-2">{{__('Address')}}</label>
        <div class="col-sm-10">
            <textarea readonly="true" class="form-control auto-input address" rows="3" name="{{__('address')}}"></textarea>
        </div>
    </div>
    <div class="form-group">
        <label class="control-label col-sm-2">{{__('Phone')}}</label>
        <div class="col-sm-10">
            <input readonly="true" type="text" class="form-control auto-input phone" name="{{__('phone')}}">
        </div>
    </div>
    <div class="form-group">
        <label class="control-label col-sm-2">{{__('Complaint')}}</label>
        <div class="col-sm-10">
            <textarea type="text" class="form-control" rows="3" name="{{__('complaint')}}"></textarea>
        </div>
    </div>
    <div class="form-group">
        <label class="control-label col-sm-2">{{__('Action')}}</label>
        <div class="col-sm-10">
            <button type="submit" class="btn btn-primary">Submit</button>
            <button type="reset" class="btn btn-default">Reset</button>
        </div>
    </div>
</form>
<script>
$.ajaxSetup({
    headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    }
});

$('.search-invoice').click(function(e) {
    e.preventDefault(e);
    $.post('complaint-search-invoice', { keyword : $('.keyword').val() }, function(data){
        $('.auto-input').val('');
        var count = 0;
        var i;

        for (i in data) {
            if (data.hasOwnProperty(i)) {
                count++;
            }
        }

        if (count < 1) {
            $('.keyword').val('').attr('placeholder', "{{__('Data not found')}}");
            return;
        }
        $('.selectpickers, .form-control.select-invoice').off().remove();
        
        $('.ins-select-invoice').append('<select title="{{__('select')}}" class="selectpickers form-control select-invoice" data-live-search="true" name="{{__('invoice_number')}}"></select>');
       
        $(data).each(function(key, val){
            $('.select-invoice').append('<option value="'+val.id+'">'+val.code+'</option>')
        })
      
       $('.selectpickers').selectpicker();
        initselectinvoice();
    }).fail(function(){
        $('.keyword').val('').attr('placeholder', "{{__('Data not found')}}");
    });
});

function initselectinvoice() {
    $('.select-invoice').change(function(e){
        $.post('complaint-get-invoice', {id : $('.selectpickers').selectpicker('val')}, function(data){
            $('.auto-input.name').val(data.name);
            $('.auto-input.address').val(data.address);
            $('.auto-input.phone').val(data.phone);
        });
    });
}

$('#create-complaint').submit(function(e){
    e.preventDefault(e);
    $('.alert-complaint-form').html("");

    $.post('/complaint',$(this).serialize(), function(data){
        window.location.replace('/complaint');
    }).fail(function(data) {
        var response = JSON.parse(data.responseText);
        if (typeof(response.errors) != "undefined") {
            var errorString = '<div class="alert alert-danger alert-dismissable">'+
                '<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button><ul>';
            $.each(response.errors, function(key, val){
                errorString += '<li>' + val[0] + '</li>';
            });
            errorString += '</ul></div>';
            $('.alert-complaint-form').html(errorString);
            $("#createModal").animate({ scrollTop: 0 }, "slow");
        }
    });
});

initselectinvoice();
</script>
@include('scripts.datepicker')
@endsection