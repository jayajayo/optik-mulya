@extends('layouts.admin')

@section('content')
@include('components.navbar', ['active' => 'complaint'])

<div id="page-wrapper">
    @component('components.headerpage')
        {{__('Complaint management')}}
    @endcomponent
    
    <div class="row">
        <div class="col-md-9" style="padding-bottom:10px">
            @can('manage_complaint')
            <button class="btn btn-primary" id="add-complaint">{{__('Add')}}</button>
            @endcan
        </div>
        <div class="col-md-3" style="padding-bottom:10px">
            <form class="form-inline" action="" >
                <div class="form-group input-group">
                <input type="text" name="keyword" class="form-control" placeholder="{{__('Search')}}" value="{{request()->keyword}}">
                    <span class="input-group-btn">
                        <button class="btn btn-primary" type="submit"><i class="fa fa-search"></i></button>
                    </span>
                </div>
            </form>                
        </div>
        <div class="col-md-12">
            @if(session()->has('status'))
                @component('components.alertsuccess')
                    {{ session()->get('status') }}
                @endcomponent
            @endif
            <div class="panel panel-default">
                <div class="panel-heading dt-bootstrap">
                    {{__('Complaint')}}
                </div>
                <div class="panel-body">
                    <div class="row">
                        <div class="col-md-12 table-responsive">
                            <table id="users-table" class="table table-striped table-bordered table-hover dataTable">
                                <thead>
                                    <tr>
                                        <th><span class="nowrap-t">{{__('Date')}} {!! \App\Libraries\SortHelper::header(request(), 'date') !!}</span></th>
                                        <th><span class="nowrap-t">{{__('Name')}} {!! \App\Libraries\SortHelper::header(request(), 'name') !!}</span></th>
                                        <th><span class="nowrap-t">{{__('Invoice number')}} {!! \App\Libraries\SortHelper::header(request(), 'code') !!}</span></th>
                                        <th><span class="nowrap-t">{{__('Address')}} {!! \App\Libraries\SortHelper::header(request(), 'address') !!}</span></th>
                                        <th><span class="nowrap-t">{{__('Phone')}} {!! \App\Libraries\SortHelper::header(request(), 'phone') !!}</span></th>
                                        <th><span class="nowrap-t">{{__('Complaint')}} {!! \App\Libraries\SortHelper::header(request(), 'complaint') !!}</span></th>
                                        <th><span class="nowrap-t">{{__('Complaint recipient')}} {!! \App\Libraries\SortHelper::header(request(), 'action') !!}</span></th>
                                        <th><span class="nowrap-t">{{__('Action response')}} {!! \App\Libraries\SortHelper::header(request(), 'action') !!}</span></th>
                                        <th>{{__('Status')}}</th>
                                        <th>{{__('Action')}}</th>
                                    </tr>
                                </thead>
                                <tbody id="row-data-table">
                                    @if($complaints->count())
                                    @foreach($complaints as $complaint)
                                    <tr>
                                        <td><span class="nowrap-t">{{$complaint->date->format('d-m-Y')}}</span></td>
                                        <td>{{$complaint->name}}</td>
                                        <td>{{$complaint->invoice_number}}</td>
                                        <td>{{$complaint->address}}</td>
                                        <td>{{$complaint->phone}}</td>
                                        <td>{{$complaint->complaint}}</td>
                                        <td>{{$complaint->creator->name or '-'}}</td>
                                        <td>{{$complaint->action or '-'}}</td>
                                        <td>
                                            <span class="nowrap-t">{!! ($complaint->waiting_status) ? '<i class="fas fa-check"></i>' : '<i class="fas fa-times"></i>&nbsp;' !!} {{__('Waiting')}}</span>
                                            <span class="nowrap-t">{!! ($complaint->finish_status) ? '<i class="fas fa-check"></i>' : '<i class="fas fa-times"></i>&nbsp;' !!} {{__('Finish')}}</span>
                                        </td>
                                        <td> 
                                            @can('manage_complaint')
                                            @if($complaint->creator_id == \Auth::user()->id)
                                            <button class="btn btn-default edit-complaint" data-id="{{$complaint->id}}">{{__('Edit')}}</button>
                                            @endif
                                            @endcan
                                            @can('action_complaint')
                                            <button class="btn btn-default action-complaint" data-id="{{$complaint->id}}">{{__('Action response')}}</button>
                                            @endcan
                                            @if($complaint->creator_id == \Auth::user()->id)
                                            @can('manage_complaint')
                                            <button class="btn btn-danger delete-complaint" data-id="{{$complaint->id}}">{{__('Delete')}}</button>
                                            @endif
                                            @endcan
                                        </td>
                                    </tr>
                                    @endforeach
                                    @else
                                    <tr>
                                        <td colspan="11">{{__('Data not found')}}</td>
                                    </tr>
                                    @endif
                                </tbody>
                            </table>
                            {{ $complaints->links() }}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="createModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title" id="createModalLabel">{{__('New Complaint')}}</h4>
            </div>
            <div class="modal-body" id="complaint-create">
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">{{__('Close')}}</button>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="editModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title" id="editModalLabel">{{__('Edit complaint')}}</h4>
            </div>
            <div class="modal-body" id="complaint-edit">
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">{{__('Close')}}</button>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="actionModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title" id="actionModalLabel">{{__('Action response')}}</h4>
            </div>
            <div class="modal-body" id="complaint-action">
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">{{__('Close')}}</button>
            </div>
        </div>
    </div>
</div>
@endsection

@push('scripts')
<script>
$.ajaxSetup({
    headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    }
});

$('#add-complaint').click(function (e){
    e.preventDefault(e);
    $.get('/complaint/create', function(data) {
        $('#complaint-create').html(data);
        $('#createModal').modal('show');
    });
});

$('.edit-complaint').click(function (e){
    e.preventDefault(e);
    var id = $(this).data('id')
    $.get('/complaint/'+id+'/edit/', function(data) {
        $('#complaint-edit').html(data);
        $('#editModal').modal('show');
    });
});

$('.action-complaint').click(function (e){
    e.preventDefault(e);
    var id = $(this).data('id')
    $.get('/complaint/'+id+'/action/', function(data) {
        $('#complaint-action').html(data);
        $('#actionModal').modal('show');
    });
});

$('.delete-complaint').click(function (e){
    e.preventDefault(e);
    var id = $(this).data('id')

    if (!confirm("{{__('Are you sure?')}}") ) return;

    $.post('/complaint/'+id, {"_method" : "DELETE"}, function(data) {
        window.location.replace('/complaint');
    });
});
</script>
@endpush
