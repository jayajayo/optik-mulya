@extends('layouts.blank')
@section('content')
<link href="{{ asset('vendor/datepicker/datepicker.min.css') }}" rel="stylesheet">
<script src="{{ asset('vendor/datepicker/datepicker.min.js') }}"></script>

<div class="alert-complaint-form">
</div>
<form class="form-horizontal" id="edit-complaint" role="form">
    @method('PUT')
    <div class="form-group">
        <label class="control-label col-sm-2">{{__('Date')}}</label>
        <div class="col-sm-10">
            <input class="form-control" readonly="true" name="{{__('date')}}" value="{{$complaint->date->format('d-m-Y')}}">
        </div>
    </div>
    <div class="form-group">
        <label class="control-label col-sm-2">{{__('Invoice number')}}</label>
        <div class="col-sm-10">
            <input type="text" readonly="true" class="form-control" name="{{__('invoice_number')}}" value="{{$complaint->invoice_number}}">
        </div>
    </div>
    <div class="form-group">
        <label class="control-label col-sm-2">{{__('Name')}}</label>
        <div class="col-sm-10">
            <input type="text" readonly="true" class="form-control" name="{{__('name')}}" value="{{$complaint->name}}">
        </div>
    </div>
    <div class="form-group">
        <label class="control-label col-sm-2">{{__('Address')}}</label>
        <div class="col-sm-10">
            <textarea readonly="true" class="form-control" rows="3" name="{{__('address')}}">{{$complaint->address}}</textarea>
        </div>
    </div>
    <div class="form-group">
        <label class="control-label col-sm-2">{{__('Phone')}}</label>
        <div class="col-sm-10">
            <input type="text" readonly="true" class="form-control" name="{{__('phone')}}" value="{{$complaint->phone}}">
        </div>
    </div>
    <div class="form-group">
        <label class="control-label col-sm-2">{{__('Complaint')}}</label>
        <div class="col-sm-10">
            <textarea type="text" readonly="true" class="form-control" rows="3" name="{{__('complaint')}}">{{$complaint->complaint}}</textarea>
        </div>
    </div>
    <div class="form-group">
        <label class="control-label col-sm-2">{{__('Action response')}}</label>
        <div class="col-sm-10">
            <textarea type="text" class="form-control" rows="3" name="{{__('action_response')}}">{{$complaint->action}}</textarea>
        </div>
    </div>
    <div class="form-group">
        <label class="control-label col-sm-2">{{__('Status')}}</label>
        <div class="col-sm-10">
            <label><input type="checkbox" name="{{__('waiting_status')}}" value="1" {{ ($complaint->waiting_status) ? 'checked' : '' }}> {{__('Waiting')}}</label>
            <label><input type="checkbox" name="{{__('finish_status')}}" value="1" {{ ($complaint->finish_status) ? 'checked' : '' }}> {{__('Finish')}}</label>
        </div>
    </div>
    <div class="form-group">
        <label class="control-label col-sm-2">{{__('Action')}}</label>
        <div class="col-sm-10">
            <button type="submit" class="btn btn-primary">Submit</button>
            <button type="reset" class="btn btn-default">Reset</button>
        </div>
    </div>
</form>
<script>
$.ajaxSetup({
    headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    }
});

$('#edit-complaint').submit(function(e){
    e.preventDefault(e);
    $('.alert-complaint-form').html("");

    $.post('/complaint/{{$complaint->id}}/updateAction', $(this).serialize(), function(data){
        window.location.replace('/complaint');
    }).fail(function(data) {
        var response = JSON.parse(data.responseText);
        if (typeof(response.errors) != "undefined") {
            var errorString = '<div class="alert alert-danger alert-dismissable">'+
                '<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button><ul>';
            $.each(response.errors, function(key, val){
                errorString += '<li>' + val[0] + '</li>';
            });
            errorString += '</ul></div>';
            $('.alert-complaint-form').html(errorString);
            $("#actionModal").animate({ scrollTop: 0 }, "slow");
        }
    });
});
</script>
@include('scripts.datepicker')
@endsection