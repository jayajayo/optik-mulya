@extends('layouts.admin')

@section('content')
    @include('components.navbar', ['active' => 'user'])

    <div id="page-wrapper">
        @component('components.headerpage')
            {{__('User management')}}
        @endcomponent
        
        <div class="row">
            <div class="col-md-9" style="padding-bottom:10px">
                <button class="btn btn-primary" id="new-user">{{__('Add user')}}</button>
            </div>
            <div class="col-md-3" style="padding-bottom:10px">
                <form class="form-inline" action="" >
                    <div class="form-group input-group">
                        <input type="text" name="keyword" class="form-control" placeholder="{{__('Search')}}" value="{{$keyword}}">
                        <span class="input-group-btn">
                            <button class="btn btn-default" type="submit"><i class="fa fa-search"></i></button>
                        </span>
                    </div>
                </form>                
            </div>
            <div class="col-md-12">
                @if(session()->has('status'))
                    @component('components.alertsuccess')
                        {{ session()->get('status') }}
                    @endcomponent
                @endif
                <div class="panel panel-default">
                    <div class="panel-heading dt-bootstrap">
                        {{__('List user')}}
                    </div>
                    <div class="panel-body">
                        <div class="row">
                            <div class="col-md-12 table-responsive">
                                <table id="users-table" class="table table-striped table-bordered table-hover dataTable">
                                    <thead>
                                        <tr>
                                            <th>Nama Lengkap {!! \App\Libraries\SortHelper::header(request(), 'name') !!}</th>
                                            <th>Panggilan {!! \App\Libraries\SortHelper::header(request(), 'nickname') !!}</th>
                                            <th>Username {!! \App\Libraries\SortHelper::header(request(), 'username') !!}</th>
                                            <th>Alamat {!! \App\Libraries\SortHelper::header(request(), 'address') !!}</th>
                                            <th>Jabatan {!! \App\Libraries\SortHelper::header(request(), 'role') !!}</th>
                                            <th>Team {!! \App\Libraries\SortHelper::header(request(), 'team') !!}</th>
                                            <th>Status {!! \App\Libraries\SortHelper::header(request(), 'status') !!}</th>
                                            <th>Action</th>
                                        </tr>
                                    </thead>
                                    <tbody id="row-data-table">
                                        @if($users->count())
                                            @foreach($users as $user)
                                                <tr>
                                                    <td>{{$user->name}}</td>
                                                    <td>{{$user->nickname}}</td>
                                                    <td>{{$user->username}}</td>
                                                    <td>{{$user->address}}</td>
                                                    <td>{{$user->role or ''}}</td>
                                                    <td>{{$user->team or ''}}</td>
                                                    <td>{{($user->status)}}</td>
                                                    <td>
                                                        <button class="btn btn-default user-edit" data-id="{{$user->id}}">{{__('Edit')}}</button> 
                                                        <button class="btn btn-primary user-activation" data-id="{{$user->id}}">{{__('Activation')}}</button>
                                                        @can('delete_user')
                                                        <button class="btn btn-danger user-delete" data-id="{{$user->id}}">{{__('Delete')}}</button>
                                                        @endcan
                                                    </td>
                                                </tr>
                                            @endforeach
                                        @else
                                            <tr>
                                                <td colspan="8">{{__('Data not found')}}</td>
                                            </tr>
                                        @endif
                                    </tbody>
                                </table>
                                {{ $users->links() }}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="modal fade" id="createModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title" id="createModalLabel">{{__('New user')}}</h4>
                </div>
                <div class="modal-body" id="form-create">
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">{{__('Close')}}</button>
                </div>
            </div>
        </div>
    </div>
    <div class="modal fade" id="activationModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title" id="activationModalLabel">{{__('User activation')}}</h4>
                </div>
                <div class="modal-body">
                    <form method="post" action="{{ url('user/activation') }}">
                        @csrf
                        <input type="hidden" name="user_activation" value="">
                        <input class="btn btn-success" type="submit" name="activation" value="active" >
                        <input class="btn btn-danger" type="submit" name="activation" value="deactive" >
                    </form>
                </div>
            </div>
        </div>
    </div>
    <div class="modal fade" id="editModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title" id="editModalLabel">{{__('Edit user')}}</h4>
                </div>
                <div class="modal-body" id="form-edit">
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">{{__('Close')}}</button>
                </div>
            </div>
        </div>
    </div>
    <div class="modal fade" id="deleteModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title" id="deleteModalLabel">{{__('Delete user')}}</h4>
                </div>
                <div class="modal-body" id="">
                    <div class="alert alert-warning">
                        <span class="alert-link">{{__("Warning")}}!</span> {{__("We not recomended to delete user. Deleting user may caused reporting issue. The same username still can't be used even you have deleted the user.")}}</p>
                    </div>
                </div>
                <div class="modal-footer">
                    <form id="delete-user" method="post" action="">
                        @csrf
                        <input type="hidden" name="_method" value="DELETE">
                        <input class="btn btn-danger" type="submit" value="{{__('Delete')}}" >
                        <button type="button" class="btn btn-default" data-dismiss="modal">{{__('Cancel')}}</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection

@push('style')
@endpush
@push('scripts')
    <script>
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        $('#new-user').click(function (e){
            e.preventDefault(e);
            $.get('/user/create', function(data) {
                $('#form-create').html(data);
                $('#createModal').modal('show');
            });
        });

        $('.user-activation').click(function(){
            var id = $(this).data('id');
            $('input[name=user_activation]').val(id);
            $('#activationModal').modal('show');
        });

        $('.user-edit').click(function(){
            var id = $(this).data('id');
            $.get('/user/'+id+'/edit', function(data) {
                $('#form-edit').html(data);
                $('#editModal').modal('show');
            });
        });

        $('.user-delete').click(function(){
            var id = $(this).data('id');
            $('#delete-user').prop('action', '/user/'+id);
            $('#deleteModal').modal('show');
        });
    </script>
@endpush
