@extends('layouts.blank')
@section('content')
    <div class="alert-user-form">
    </div>
    <form id="edit-user" role="form">
        <input type="hidden" name="_method" value="PUT">
        <div class="form-group">
            <label>{{__('Fullname')}}</label>
            <input class="form-control" name={{__('fullname')}} value="{{ $user->name }}">
        </div>
        <div class="form-group">
            <label>{{__('Nickname')}}</label>
            <input class="form-control" name="{{__('nickname')}}" value="{{ $user->nickname }}">
        </div>
        <div class="form-group">
            <label>{{__('Username')}}</label>
            <input class="form-control" name="{{__('username')}}" value="{{ $user->username }}" disabled="disabled">
        </div>
        <div class="form-group">
            <label>{{__('Address')}}</label>
            <textarea class="form-control" rows="3" name="{{__('address')}}" >{{ $user->address }}</textarea>
        </div>
        <div class="form-group">
            <label>{{__('Mobile phone')}}</label>
            <input class="form-control" name="{{__('mobile_phone')}}" value="{{ $user->phone }}">
        </div>
        <div class="form-group">
            <label>{{__('Password')}}</label>
            <input type="password" class="form-control" name="{{__('password')}}" >
            <p class="help-block">{{__('Leave it blank if you do not want to change')}}</p>
        </div>
        <div class="form-group">
            <label>{{__('Re-password')}}</label>
            <input type="password" class="form-control" name="{{__('password_confirmation')}}" >
        </div>
        <div class="form-group">
            <label>{{__('Role')}}</label>
            <select class="form-control form-role" name="{{__('role')}}" >
                @foreach($roles as $role)
                @if($role->name != 'superadmin')
                <option {{$user->hasRole($role->name) ? 'selected' : ''}} value="{{$role->name}}">{{ucfirst($role->name)}}</option>
                @endif
                @endforeach
            </select>
        </div>
        <div class="form-group form-team">
            <label>{{__('Team')}}</label>
            <select class="form-control" name="{{__('team')}}" >
                @foreach($teams as $team)
                    <option {{$user->team_id == $team->id ? 'selected' : ''}} value="{{$team->id}}">{{ucfirst($team->name)}}</option>
                @endforeach
            </select>
        </div>
        
        <button type="submit" class="btn btn-primary">Submit</button>
        <button type="reset" class="btn btn-default">Reset</button>
    </form>
    <script>
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        $(document).ready(function(){
            $('.form-team').toggle($(this).val() == 'market');
        })
        
        $('.form-role').change(function(){
            $('.form-team').toggle($(this).val() == 'market');
        });

        $('#edit-user').submit(function(e){
            e.preventDefault(e);
            $('.alert-user-form').html("");

            $.post('/user/{{$user->id}}', $(this).serialize(), function(data){
                window.location.replace('/user');
            }).fail(function(data) {
                var response = JSON.parse(data.responseText);
                if (typeof(response.errors) != "undefined") {
                    var errorString = '<div class="alert alert-danger alert-dismissable">'+
                        '<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button><ul>';
                    $.each(response.errors, function(key, val){
                        errorString += '<li>' + val[0] + '</li>';
                    });
                    errorString += '</ul></div>';
                    $('.alert-user-form').html(errorString);
                    $("#editModal").animate({ scrollTop: 0 }, "slow");
                }
            });
        });
    </script>
@endsection