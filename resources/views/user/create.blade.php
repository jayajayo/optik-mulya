@extends('layouts.blank')
@section('content')
    <div class="alert-user-form">
    </div>
    <form id="create-user" role="form">
        <div class="form-group">
            <label>{{__('Fullname')}}</label>
            <input class="form-control" name={{__('fullname')}} >
        </div>
        <div class="form-group">
            <label>{{__('Nickname')}}</label>
            <input class="form-control" name="{{__('nickname')}}" >
        </div>
        <div class="form-group">
            <label>{{__('Username')}}</label>
            <input class="form-control" name="{{__('username')}}" >
        </div>
        <div class="form-group">
            <label>{{__('Address')}}</label>
            <textarea class="form-control" rows="3" name="{{__('address')}}" ></textarea>
        </div>
        <div class="form-group">
            <label>{{__('Mobile phone')}}</label>
            <input class="form-control" name="{{__('mobile_phone')}}" >
        </div>
        <div class="form-group">
            <label>{{__('Password')}}</label>
            <input type="password" class="form-control" name="{{__('password')}}" >
        </div>
        <div class="form-group">
            <label>{{__('Re-password')}}</label>
            <input type="password" class="form-control" name="{{__('password_confirmation')}}" >
        </div>
        <div class="form-group">
            <label>{{__('Role')}}</label>
            <select class="form-control form-role" name="{{__('role')}}" >
                @foreach($roles as $role)
                    @if($role->name != 'superadmin')
                    <option value="{{$role->name}}">{{ucfirst($role->name)}}</option>
                    @endif
                @endforeach
            </select>
        </div>
        <div class="form-group form-team">
            <label>{{__('Team')}}</label>
            <select class="form-control" name="{{__('team')}}" >
                @foreach($teams as $team)
                    <option value="{{$team->id}}">{{ucfirst($team->name)}}</option>
                @endforeach
            </select>
        </div>
        
        <button type="submit" class="btn btn-primary">Submit</button>
        <button type="reset" class="btn btn-default">Reset</button>
    </form>
    <script>
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        
        $(document).ready(function(){
            $('.form-team').toggle($(this).val() == 'market');
        })
        
        $('.form-role').change(function(){
            $('.form-team').toggle($(this).val() == 'market');
        });

        $('#create-user').submit(function(e){
            e.preventDefault(e);
            $('.alert-user-form').html("");

            $.post('/user',$(this).serialize(), function(data){
                window.location.replace('/user');
            }).fail(function(data) {
                var response = JSON.parse(data.responseText);
                if (typeof(response.errors) != "undefined") {
                    var errorString = '<div class="alert alert-danger alert-dismissable">'+
                        '<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button><ul>';
                    $.each(response.errors, function(key, val){
                        errorString += '<li>' + val[0] + '</li>';
                    });
                    errorString += '</ul></div>';
                    $('.alert-user-form').html(errorString);
                    $("#createModal").animate({ scrollTop: 0 }, "slow");
                }
            });
        });
    </script>
@endsection