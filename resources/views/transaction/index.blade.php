@extends('layouts.admin')

@section('content')
@include('components.navbar', ['active' => 'transaction'])

<div id="page-wrapper">
    @component('components.headerpage')
    {{__('Transaction management')}}
    @endcomponent
    <form class="form-inline" action="" >
    
        <div class="row">
            <div class="col-md-3" style="padding-bottom:10px">
                <div class="form-group input-group" style="padding-bottom:10px">
                    <span class="input-group-addon">{{__('Total omset')}}</span>
                    <input type="text" readonly="true" data-toggle="datepicker" name="start_date" class="form-control" value="{{ \Carbon\Carbon::createFromFormat('Y-m-d', $start)->format('d-m-Y')}}">
                </div>
                <div class="form-group input-group" style="padding-bottom:10px">
                    <span class="input-group-addon">{{__('To')}}</span>
                    <input type="text" readonly="true" data-toggle="datepicker" name="end_date" class="form-control" value="{{ \Carbon\Carbon::createFromFormat('Y-m-d', $end)->format('d-m-Y')}}">
                    <span class="input-group-btn">
                        <button class="btn btn-primary" type="submit"><i class="fa fa-search"></i></button>
                    </span>
                </div>
            </div>
            <div class="col-md-9 text-right" style="padding-bottom:10px">
                @foreach($teams as $team)
                <div class="form-group input-group" style="padding-bottom:10px">
                        <span class="input-group-addon">{{$team->name}}</span>
                        <p type="text" class="form-control" value="">{{ number_format($totalomset[$team->name]-$dataminus[$team->name],2,',','.')}}</p>
                </div>
                @endforeach
            </div>
        </div>
        <div class="row">
            <div class="col-md-9" style="padding-bottom:10px">
                <div class="dropdown">
                @can('transaction_create')
                <button class="btn btn-primary dropdown-toggle" type="button" data-toggle="dropdown">{{__('New transaction')}}</button>
                <ul class="dropdown-menu">
                    <li><a id="new-transaction" href="#">Kacamata</a></li>
                    @can('manage_user')
                    <li><a id="new-transaction-other" href="#">Produk Lainnya</a></li>
                    @endcan
                </ul>
                @endcan
                </div>
            </div>
            <div class="col-md-3" style="padding-bottom:10px">
                <div class="form-group input-group">
                    <input type="text" name="keyword" class="form-control" placeholder="{{__('Search')}}" value="{{request()->keyword}}">
                    <span class="input-group-btn">
                        <button class="btn btn-primary" type="submit"><i class="fa fa-search"></i></button>
                    </span>
                </div>
            </div>
        </div>
    </form>                
    <div class="row">
        <div class="col-md-12">
            @if(session()->has('status'))
                @component('components.alertsuccess')
                {{ session()->get('status') }}
                @endcomponent
            @endif
            <div class="panel panel-default">
                <div class="panel-heading dt-bootstrap">
                    {{__('List transaction')}}
                </div>
                <div class="panel-body">
                    <div class="row">
                        <div class="col-md-12 table-responsive">
                            <table id="users-table" class="table table-striped table-bordered table-hover dataTable">
                                <thead>
                                    <tr>
                                        <th><span class="nowrap-t">{{__('Invoice number')}} {!! \App\Libraries\SortHelper::header(request(), 'code') !!}</span></th>
                                        <th><span class="nowrap-t">{{__('Nama')}} {!! \App\Libraries\SortHelper::header(request(), 'name') !!}</span></th>
                                        <th><span class="nowrap-t">{{__('Address')}} {!! \App\Libraries\SortHelper::header(request(), 'address') !!}</span></th>
                                        <th><span class="nowrap-t">{{__('Order date')}} {!! \App\Libraries\SortHelper::header(request(), 'order_date') !!}</span></th>
                                        <th><span class="nowrap-t">{{__('Phone')}} {!! \App\Libraries\SortHelper::header(request(), 'phone') !!}</span></th>
                                        <th><span class="nowrap-t">{{__('Frame')}} {!! \App\Libraries\SortHelper::header(request(), 'frame') !!}</span></th>
                                        <th><span class="nowrap-t">{{__('Lens')}} {!! \App\Libraries\SortHelper::header(request(), 'lens') !!}</span></th>
                                        <th><span class="nowrap-t">{{__('Rx')}} {!! \App\Libraries\SortHelper::header(request(), 'rx_price') !!}</span></th>
                                        <th>{{__('SPH')}} </th>
                                        <th>{{__('Cyl')}}</th>
                                        <th>{{__('Axis')}}</th>
                                        <th>Add</th>
                                        <th><span class="nowrap-t">{{__('PD')}} {!! \App\Libraries\SortHelper::header(request(), 'pd') !!}</span></th>
                                        <th><span class="nowrap-t">{{__('Total price')}} {!! \App\Libraries\SortHelper::header(request(), 'total_payment') !!}</span></th>
                                        <th><span class="nowrap-t">{{__('Memo')}} {!! \App\Libraries\SortHelper::header(request(), 'memo') !!}</span></th>
                                        <th><span class="nowrap-t">{{__('Information')}} {!! \App\Libraries\SortHelper::header(request(), 'paid_off') !!}</span></th>
                                        <th><span class="nowrap-t">{{__('Status')}} {!! \App\Libraries\SortHelper::header(request(), 'status') !!}</span></th>
                                        <th><span class="nowrap-t">{{__('Action')}}</span></th>
                                    </tr>
                                </thead>
                                <tbody id="row-data-table">
                                    @if($transactions->count())
                                    @foreach($transactions as $trans)
                                    <tr>
                                        <td>{{$trans->code or '-'}}</td>
                                        <td>{{$trans->name}}</td>
                                        <td>{{$trans->address}}</td>
                                        <td>{{$trans->order_date->format('d-m-Y')}}</td>
                                        <td>{{$trans->phone}}</td>
                                        <td>
                                            <span class="nowrap-t">
                                                {{$trans->frame or ''}} 
                                                {{isset($trans->frame()->first()->merkframe->name) ? '('.$trans->frame()->first()->merkframe->name.')' : '-'}}
                                            </span>
                                        </td>
                                        <td>{{$trans->lens or '-'}} </td>
                                        <td class="text-right"><span class="nowrap-t">{{!empty($trans->rx_price) ? 'Rp '.number_format($trans->rx_price,2,',','.') : '-'}}</span></td>
                                        <td>
                                            <span class="nowrap-t">{{empty($trans->sph_r) ? '-' : 'R: '.$trans->sph_r}} </span>
                                            <span class="nowrap-t">{{empty($trans->sph_l) ? '-' : 'L: '.$trans->sph_l}} </span>
                                        </td>
                                        <td>
                                            <span class="nowrap-t">{{empty($trans->cyl_r) ? '-' : 'R: '.$trans->cyl_r}} </span>
                                            <span class="nowrap-t">{{empty($trans->cyl_l) ? '-' : 'L: '.$trans->cyl_l}}  </span>
                                        </td>
                                        <td>
                                            <span class="nowrap-t">{{empty($trans->axis_r) ? '-' : 'R: '.$trans->axis_r}} </span> 
                                            <span class="nowrap-t">{{empty($trans->axis_l) ? '-' : 'L: '.$trans->axis_l}} </span>
                                        </td>
                                        <td>
                                            <span class="nowrap-t">{{empty($trans->add_r) ? '-' : 'R: '.$trans->add_r}} </span>
                                            <span class="nowrap-t">{{empty($trans->add_l) ? '-' : 'L: '.$trans->add_l}} </span> 
                                        </td>
                                        <td>{{$trans->pd}}</td>
                                        <td class="text-right"><span class="nowrap-t">{{!empty($trans->total_payment) ? 'Rp '.number_format($trans->total_payment,2,',','.') : '-'}}</span></td>
                                        <td>{{$trans->memo or '-'}}</td>
                                        <td>{{ __($trans->paid_off) }}</td>
                                        <td>{{$trans->status}}</td>
                                        <td>
                                            @can('transaction_edit')
                                            <a class="btn btn-default" href="{{route('transaction.edit', ['id'=> $trans->id])}}">edit</a>
                                            @endcan
                                            <a class="btn btn-default" href="{{url('debt-details/'.$trans->id)}}">detail</a>
                                        </td>
                                    </tr>
                                    @endforeach
                                    @else
                                    <tr>
                                        <td colspan="18">{{__('Data not found')}}</td>
                                    </tr>
                                    @endif
                                </tbody>
                            </table>
                            {{ $transactions->links() }}
                        </div>
                    </div>
                    <div class="row">
                        @can('transaction_download')
                        <div class="col-md-12 text-right">
                            <button class="btn btn-default" data-toggle="modal" data-target="#downloadModal">Download</button>
                        </div>
                        @endcan
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="createModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close close-confirm" aria-hidden="true" data-modalid="createModal" data-message="{{__('Are you sure?')}}">&times;</button>
                <h4 class="modal-title" id="createModalLabel">{{__('New transaction')}}</h4>
            </div>
            <div class="modal-body" id="form-create">
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default close-confirm" data-modalid="createModal" data-message="{{__('Are you sure?')}}">{{__('Close')}}</button>
            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="manageMerkModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title" id="manageMerkModalLabel">{{__('Manage merk frame')}}</h4>
            </div>
            <div class="modal-body" id="form-manage-merk-frame">
            </div>
            <div class="modal-footer">
                <form id="delete-user" method="post" action="">
                    @csrf
                    <input type="hidden" name="_method" value="DELETE">
                    <button type="button" class="btn btn-default" data-dismiss="modal">{{__('Close')}}</button>
                </form>
            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="downloadModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title" id="downloadModallLabel">{{__('Download data customer')}}</h4>
            </div>
            <div class="modal-body" id="download-modal">
                <div class="alert-download-form">
                </div>
                <form class="form-horizontal" id="transaction-download" role="form">
                    <div class="form-group">
                        <label class="control-label col-sm-2">{{__('Date')}}</label>
                        <div class="col-sm-10">
                            <input type="text" readonly="true" data-toggle="datepicker" class="form-control" name="startdate" value="{{ \Carbon\Carbon::today()->format('d-m-Y')}}">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-sm-2">{{__('To')}}</label>
                        <div class="col-sm-10">
                            <input type="text" readonly="true" data-toggle="datepicker" class="form-control" name="enddate" value="{{ \Carbon\Carbon::today()->format('d-m-Y')}}">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-sm-2">{{__('Action')}}</label>
                        <div class="col-sm-10">
                            <button type="submit" class="btn btn-primary">Download</button>
                        </div>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">{{__('Close')}}</button>
            </div>
        </div>
    </div>
</div>
@endsection

@push('style')
<link href="{{ asset('vendor/datepicker/datepicker.min.css') }}" rel="stylesheet">
<script src="{{ asset('vendor/datepicker/datepicker.min.js') }}"></script>
@endpush

@push('scripts')
<script>
$.ajaxSetup({
    headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    }
});

$('#new-transaction').click(function (e){
    e.preventDefault(e);
    $.get('/transaction/create', function(data) {
        $('#form-create').html(data);
        $('#createModal').modal({ 
            backdrop: 'static',
            keyboard: false
        }).modal('show');
    });
});

$(document).ready(function() {
    var table = $('#users-table').DataTable( {
        scrollY:        false,
        scrollX:        true,
        scrollCollapse: true,
        paging:         false,
        ordering:       false,
        searching:      false,
        lengthChange:   false,
        info:           false,
        fixedColumns:   {
            leftColumns: 3,
        }
    } );
});

$('#new-transaction-other').click(function (e){
    e.preventDefault(e);
    $.get('/transaction/create/1', function(data) {
        $('#form-create').html(data);
        $('#createModal').modal({ 
            backdrop: 'static',
            keyboard: false
        }).modal('show');
    });
});

$('#go-manage-merk-frame, #manage-merk-frame').click(function (e){
    e.preventDefault(e);
    $.get('/merk-frame', function(data) {
        $('#form-manage-merk-frame').html(data);
        $('#manageMerkModal').modal('show');
    });
});

$('#manageMerkModal').on('hidden.bs.modal', function () {
    location.reload();
});

$('#transaction-download').submit(function (e){
    e.preventDefault(e);
    var formdata = $(this).serialize();
    $.post('/transaction-request-download', formdata , function(data){
        window.location.href = '/transaction-download?'+formdata;
    }).fail(function(data) {
            var response = data.responseJSON;
            if (typeof(response.errors) != "undefined") {
                var errorString = '<div class="alert alert-danger alert-dismissable">'+
                    '<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button><ul>';
                $.each(response.errors, function(key, val){
                    errorString += '<li>' + val[0] + '</li>';
                });
                errorString += '</ul></div>';
                $('.alert-download-form').html(errorString);
                $("#downloadModal").animate({ scrollTop: 0 }, "slow");
            }
        });
});
</script>
@include('scripts.closeconfirm')
@include('scripts.datepicker')
@endpush
