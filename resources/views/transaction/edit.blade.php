@extends('layouts.admin')

@section('content')
@include('components.navbar', ['active' => 'transaction'])
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.12.4/css/bootstrap-select.min.css">

<div id="page-wrapper">
    @component('components.headerpage')
    {{__('Transaction management')}}
    @endcomponent
    
    <div class="row">
        <div class="col-md-9" style="padding-bottom:10px">
        </div>
        <div class="col-md-3 text-right" style="padding-bottom:10px">
            <button class="btn btn-default" onclick="window.history.back();"><span class="fas fa-chevron-left"></span> kembali</button>        
        </div>
        <div class="col-md-12">
            @if(session()->has('status'))
                @component('components.alertsuccess')
                {{ session()->get('status') }}
                @endcomponent
            @endif
            <div class="panel panel-default">
                <div class="panel-heading dt-bootstrap">
                    {{__('Edit customer')}}
                </div>
                <div class="panel-body">
                    <div class="row">
                        <div class="col-md-12 table-responsive">
                            <form class="form-horizontal" method="post" action="{{ route('transaction.update', ['id' => $transaction->id]) }}" id="create-transaction" role="form">
                                @csrf
                                @method('PUT')
                                <div class="table-responsive">
                                    <table class="table table-bordered table-hover dataTable">
                                        <tbody>
                                            <tr>
                                                <td>#</td>
                                                <td>SPH</td>
                                                <td>CYL</td>
                                                <td>AXIS</td>
                                                <td>ADD</td>
                                            </tr>
                                            <tr>
                                                <td>R</td>
                                                <td><input class="form-control" name="sph_r" value="{{$transaction->sph_r}}"></td>
                                                <td><input class="form-control" name="cyl_r" value="{{$transaction->cyl_r}}"></td>
                                                <td><input class="form-control" name="axis_r" value="{{$transaction->axis_r}}"></td>
                                                <td><input class="form-control" name="add_r" value="{{$transaction->add_r}}"></td>
                                            </tr>
                                            <tr>
                                                <td>L</td>
                                                <td><input class="form-control" name="sph_l" value="{{$transaction->sph_l}}"></td>
                                                <td><input class="form-control" name="cyl_l" value="{{$transaction->cyl_l}}"></td>
                                                <td><input class="form-control" name="axis_l" value="{{$transaction->axis_l}}"></td>
                                                <td><input class="form-control" name="add_l" value="{{$transaction->add_l}}"></td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label class="control-label col-sm-3">{{__('No. nota')}}</label>
                                            <div class="col-sm-9">
                                                <input class="form-control" disabled="disabled" value={{$transaction->code}}>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="control-label col-sm-3">{{__('PD')}}</label>
                                            <div class="col-sm-9">
                                                <input class="form-control" name="{{__('pd')}}"  value="{{$transaction->pd}}">
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="control-label col-sm-3">{{__('Name')}}</label>
                                            <div class="col-sm-9">
                                                <input class="form-control" name="{{__('name')}}"  value="{{$transaction->name}}">
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="control-label col-sm-3">{{__('Address')}}</label>
                                            <div class="col-sm-9">
                                                <textarea class="form-control" rows="3" name="{{__('address')}}">{{$transaction->address}}</textarea>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="control-label col-sm-3">{{__('Phone')}}</label>
                                            <div class="col-sm-9">
                                                <input class="form-control" name="{{__('phone')}}"  value="{{$transaction->phone}}">
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="control-label col-sm-3">Status</label>
                                            <div class="col-sm-9">
                                            <select data-live-search="true" class="form-control selectpicker" name="status_customer" value="{{$transaction->status}}">
                                                        <option value="Lancar">Lancar</option>
                                                        <option value="Macet">Macet</option>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label class="control-label col-sm-3">{{__('Frame')}}</label>
                                            <div class="col-sm-9">
                                                <select data-live-search="true" title="-- {{__('select merk frame')}} --" class="form-control selectpicker" id="select-merkframe" name="{{__('merkframe')}}">
                                                    @php($selectmerkframe = isset($transaction->frame->merkframe->id) ? $transaction->frame->merkframe->id : '0');
                                                    @foreach($merkframes as $merkframe)
                                                        <option {{ $merkframe->id == $selectmerkframe ? 'selected' : '' }} value="{{$merkframe->id}}">{{$merkframe->name}}</option>
                                                    @endforeach
                                                </select>
                                                <br><br>
                                                <select class="form-control" id="select-team-frame" name="{{__('frame_team')}}" >
                                                    @foreach($teams as $iteam)
                                                    <option {{ $iteam->id == $frame_team? 'selected' : '' }} value="{{$iteam->id}}">{{$iteam->name}}</option>
                                                    @endforeach
                                                </select>
                                                <br>
                                                <div class="ins-select-frame" data-default="{{$transaction->frame_id}}">
                                                    <select class="form-control selectpickers" id="select-frame" name="{{__('frame_type')}}" ></select>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="control-label col-sm-4">Rp <span class="pull-right target-mask" data-t="frame">{{!empty($transaction->frame_price) ? ''.number_format($transaction->frame_price,0,',','.') : '-'}}</span></label>
                                            <div class="col-sm-8">
                                                <input type="number" min="0" id="i-frame-price" class="form-control money-mask i-change" data-t="frame" name="{{__('frame_price')}}" value="{{ (int)$transaction->frame_price}}">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label class="control-label col-sm-3">{{__('Lens')}}</label>
                                            <div class="col-sm-9">
                                                <input type="text" class="form-control" name="{{__('lens')}}" value="{{$transaction->lens}}">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="control-label col-sm-4">Rp <span class="pull-right target-mask" data-t="lens">{{!empty($transaction->lens_price) ? ''.number_format($transaction->lens_price,0,',','.') : '-'}}</span></label>
                                            <div class="col-sm-8">
                                                <input type="number" min="0" id="i-lens" class="form-control money-mask i-change" data-t="lens" name="{{__('lens_price')}}" value="{{ (int)$transaction->lens_price}}">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label class="control-label col-sm-3">{{__('Rx')}}</label>
                                            <div class="col-sm-9">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="control-label col-sm-4">Rp <span class="pull-right target-mask" data-t="rx">{{!empty($transaction->rx_price) ? ''.number_format($transaction->rx_price,0,',','.') : '-'}}</span></label>
                                            <div class="col-sm-8">
                                                <input type="number" min="0" id="i-rx" class="form-control money-mask i-change" data-t="rx" name="{{__('rx_price')}}" value="{{ (int)$transaction->rx_price}}">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label class="control-label col-sm-3">{{__('Total')}}</label>
                                            <div class="col-sm-9">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="control-label col-sm-4">Rp <span class="pull-right target-mask" data-t="total">{{!empty($transaction->total_payment) ? ''.number_format($transaction->total_payment,0,',','.') : '-'}}</span></label>
                                            <div class="col-sm-8">
                                                <input type="number" min="0" readonly="true" id="total-price" class="form-control money-mask" data-t="total" name="{{__('total')}}" value="{{ (int)$transaction->total_payment}}">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label class="control-label col-sm-4">{{__('Down payment')}}</label>
                                            <div class="col-sm-8">
                                                <select class="form-control" name="{{__('information')}}">
                                                    @php($dp = $transaction->payments()->where('name', 'dp')->first())
                                                    @foreach($types as $type)
                                                    <option {{ $dp['information'] == $type->id ? 'selected' : '' }} value="{{$type->id}}">{{$type->code}}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="control-label col-sm-4">Rp <span class="pull-right target-mask" data-t="dp">{{!empty($transaction->down_payment) ? ''.number_format($transaction->down_payment,0,',','.') : '-'}}</span></label>
                                            <div class="col-sm-8">
                                                <input type="number" min="0" id="i-dp" class="form-control money-mask i-change" data-t="dp" name="{{__('down_payment')}}" value="{{(int)$transaction->down_payment}}">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label class="control-label col-sm-4">{{__('Remaining')}}</label>
                                            <div class="col-sm-8">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="control-label col-sm-4">Rp <span class="pull-right target-mask" data-t="remain">0</span></label>
                                            <div class="col-sm-8">
                                                <input type="number" min="0" readonly="true" id="i-remain" class="form-control money-mask" data-t="remain" name="{{__('remaining')}}" value="0">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label class="control-label col-sm-4">{{__('Order date')}}</label>
                                            <div class="col-sm-8">
                                                <input data-toggle="datepicker" readonly="true" type="text" class="form-control" name={{__('order_date')}} value="{{$transaction->order_date->format('d-m-Y')}}">
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="control-label col-sm-4">{{ __('Tempo')}}</label>
                                            <div class="col-sm-8">
                                                <select class="form-control" name="{{__('tempo')}}">
                                                    <option disabled selected value>-- {{__('pilih')}} --</option>
                                                    @foreach(App\Enums\TransactionEnum::tempo as $tempo)
                                                        <option {{$transaction->tempo == $tempo ? 'selected' : ''}} value="{{$tempo}}">{{$tempo}}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label class="control-label col-sm-4">{{ __('Finish date')}}</label>
                                            <div class="col-sm-8">
                                                <input data-toggle="datepicker" readonly="true" type="text" class="form-control" name={{__('finish_date')}} value="{{$transaction->finish_date->format('d-m-Y')}}">
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="control-label col-sm-4">{{ __('Charge date')}}</label>
                                            <div class="col-sm-8">
                                                <input data-toggle="datepicker" readonly="true" type="text" class="form-control" name={{__('charge_date')}} value="{{$transaction->charge_date->format('d-m-Y')}}">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label class="control-label col-sm-4">{{__('Memo')}}</label>
                                            <div class="col-sm-8">
                                                <textarea type="text" class="form-control" rows="3" name="{{__('memo')}}" >{{$transaction->memo}}</textarea>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label class="control-label col-sm-4">{{ __('Finish payment')}}</label>
                                        </div>
                                        <div class="form-group">
                                            <label class="control-label col-sm-4">Rp <span class="pull-right target-mask" data-t="finish">{{!empty($transaction->finish_payment) ? ''.number_format($transaction->finish_payment,0,',','.') : '-'}}</span></label>
                                            <div class="col-sm-8">
                                                <input type="number" class="form-control money-mask" data-t="finish" name={{__('finish_payment')}} value="{{ (int)$transaction->finish_payment }}">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <br>
                                    <div class="col-md-12 text-center">
                                        <button type="submit" class="btn btn-primary">Update</button>
                                        <a href="{{route('transaction.index')}}" class="btn btn-default">Kembali</a>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                    <div class="row">
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@push('style')
<link href="{{ asset('vendor/datepicker/datepicker.min.css') }}" rel="stylesheet">
<script src="{{ asset('vendor/datepicker/datepicker.min.js') }}"></script>
@endpush

@push('scripts')
@include('scripts.moneymask')
<script>
    $('.i-change').on('input change', function(e){ updateprice() });

    function updateprice() {
        var sum = parseFloat($('#i-lens').val()) + parseFloat($('#i-rx').val()) + parseFloat($('#i-frame-price').val());
        var total = $('#total-price');
        total.val(sum); 
        updatemask(total.data('t'), sum);
        var subs = sum - parseFloat($('#i-dp').val());
        var remain = $('#i-remain');
        remain.val(subs);
        updatemask(remain.data('t'), subs);
    }
    $('#select-merkframe, #select-team-frame').change(function(){
        $('#select-frame, .ins-select-frame .bootstrap-select.form-control').off().remove();
        $('.ins-select-frame').append('<select title="{{__('select')}}" data-live-search="true" class="form-control selectpickers" id="select-frame" name="{{__('frame_type')}}" ></select>');
        $.get('/frame-by-merk/'+$('.selectpicker').selectpicker('val')+'/'+$('#select-team-frame').val(), function(data){
            $(data).each(function(k, v){
                $('#select-frame').append('<option value='+v.id+'>'+v.name+' (stock = '+v.stock+')</option>');
            });
            $('.selectpickers').selectpicker();
        });
    });
    $(document).ready(function(){
        $('.selectpicker').selectpicker();
        $('#select-frame, .ins-select-frame .bootstrap-select.form-control').remove();
        $('.ins-select-frame').append('<select title="{{__('select')}}" data-live-search="true" class="form-control selectpickers" id="select-frame" name="{{__('frame_type')}}" ></select>');

        $.get('/frame-by-merk/'+$('.selectpicker').selectpicker('val')+'/'+$('#select-team-frame').val(), function(data){
            $(data).each(function(k, v){
                let selected = $('.ins-select-frame').data('default') == v.id ? 'selected' : '';                
                $('#select-frame').append('<option value='+v.id+' '+selected+'>'+v.name+' (stock = '+v.stock+')</option>');
            });
        });

        updateprice();
    });
</script>
@include('scripts.closeconfirm')
@include('scripts.datepicker')
@endpush
