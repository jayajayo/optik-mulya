@extends('layouts.blank')
@section('content')
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.12.4/css/bootstrap-select.min.css">
<link href="{{ asset('vendor/datepicker/datepicker.min.css') }}" rel="stylesheet">
<script src="{{ asset('vendor/datepicker/datepicker.min.js') }}"></script>
<div class="alert-transaction-form">
</div>
<form class="form-horizontal" id="create-transaction" role="form">
    <div class="table-responsive">
        <table class="table table-bordered table-hover dataTable">
            <tbody>
                <tr>
                    <td>#</td>
                    <td>SPH</td>
                    <td>CYL</td>
                    <td>AXIS</td>
                    <td>ADD</td>
                </tr>
                <tr>
                    <td>R</td>
                    <td> <input class="form-control" name="sph_r" ></td>
                    <td> <input class="form-control" name="cyl_r" ></td>
                    <td> <input class="form-control" name="axis_r" ></td>
                    <td> <input class="form-control" name="add_r" ></td>
                </tr>
                <tr>
                    <td>L</td>
                    <td> <input class="form-control" name="sph_l" ></td>
                    <td> <input class="form-control" name="cyl_l" ></td>
                    <td> <input class="form-control" name="axis_l" ></td>
                    <td> <input class="form-control" name="add_l" ></td>
                </tr>
            </tbody>
        </table>
    </div>
    <div class="row">
        <div class="col-md-6">
            <div class="form-group">
                <label class="control-label col-sm-3">{{__('No. nota')}}</label>
                <div class="col-sm-9">
                    <input class="form-control" disabled="disabled" value={{$newcode}}>
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-sm-3">{{__('PD')}}</label>
                <div class="col-sm-9">
                    <input class="form-control" name="{{__('pd')}}" >
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-sm-3">{{__('Name')}}</label>
                <div class="col-sm-9">
                    <input class="form-control" name="{{__('name')}}" >
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-sm-3">{{__('Address')}}</label>
                <div class="col-sm-9">
                    <textarea class="form-control" rows="3" name="{{__('address')}}" ></textarea>
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-sm-3">{{__('Phone')}}</label>
                <div class="col-sm-9">
                    <input class="form-control" name="{{__('phone')}}" required="true">
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-sm-3">Status</label>
                <div class="col-sm-9">
                    <select data-live-search="true" title="-- pilih status customer --" class="form-control selectpicker" name="status_customer">
                            <option selected="selected" value="Lancar">Lancar</option>
                            <option value="Macet">Macet</option>
                    </select>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-6">
            <div class="form-group">
                <label class="control-label col-sm-3">{{__('Frame')}}</label>
                <div class="col-sm-9">
                    <select data-live-search="true" title="-- {{__('select merk frame')}} --" class="form-control selectpicker" id="select-merkframe" name="{{__('merkframe')}}">
                        @foreach($merkframes as $merkframe)
                            <option value="{{$merkframe->id}}">{{$merkframe->name}}</option>
                        @endforeach
                    </select>
                    <br><br>
                    <div class="ins-select-frame">
                        <select title="{{__('select')}}" data-live-search="true" class="form-control selectpickers" id="select-frame" name="{{__('frame_type')}}" ></select>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-6">
        <div class="form-group">
            <label class="control-label col-sm-4">Rp <span class="pull-right target-mask" data-t="frame">0</span></label>
                <div class="col-sm-8">
                    <input type="number" min="0" id="i-frame-price" class="form-control money-mask i-change" data-t="frame" name="{{__('frame_price')}}" value="0">
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-6">
            <div class="form-group">
                <label class="control-label col-sm-3">{{__('Lens')}}</label>
                <div class="col-sm-9">
                    <input type="text" class="form-control" name="{{__('lens')}}" value="">
                </div>
            </div>
        </div>
        <div class="col-md-6">
        <div class="form-group">
            <label class="control-label col-sm-4">Rp <span class="pull-right target-mask" data-t="lens">0</span></label>
                <div class="col-sm-8">
                    <input type="number" min="0" id="i-lens" class="form-control money-mask i-change" data-t="lens" name="{{__('lens_price')}}" value="0">
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-6">
            <div class="form-group">
                <label class="control-label col-sm-3">{{__('Rx')}}</label>
                <div class="col-sm-9">
                </div>
            </div>
        </div>
        <div class="col-md-6">
        <div class="form-group">
            <label class="control-label col-sm-4">Rp <span class="pull-right target-mask" data-t="rx">0</span></label>
                <div class="col-sm-8">
                    <input type="number" min="0" id="i-rx" class="form-control money-mask i-change" data-t="rx" name="{{__('rx_price')}}" value="0">
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-6">
            <div class="form-group">
                <label class="control-label col-sm-3">{{__('Total')}}</label>
                <div class="col-sm-9">
                </div>
            </div>
        </div>
        <div class="col-md-6">
        <div class="form-group">
            <label class="control-label col-sm-4">Rp <span class="pull-right target-mask" data-t="total">0</span></label>
                <div class="col-sm-8">
                    <input type="number" min="0" readonly="true" id="total-price" class="form-control money-mask" data-t="total" name="{{__('total')}}" value="0">
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-6">
            <div class="form-group">
                <label class="control-label col-sm-4">{{__('Down payment')}}</label>
                <div class="col-sm-8">
                    <select class="form-control" name="{{__('information')}}">
                        @foreach($types as $type)
                        <option value="{{$type->code}}">{{$type->code}}</option>
                        @endforeach
                    </select>
                </div>
            </div>
        </div>
        <div class="col-md-6">
        <div class="form-group">
            <label class="control-label col-sm-4">Rp <span class="pull-right target-mask" data-t="dp">0</span></label>
                <div class="col-sm-8">
                    <input type="number" min="0" id="i-dp" class="form-control money-mask i-change" data-t="dp" name="{{__('down_payment')}}" value="0">
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-6">
            <div class="form-group">
                <label class="control-label col-sm-4">{{__('Remaining')}}</label>
                <div class="col-sm-8">
                </div>
            </div>
        </div>
        <div class="col-md-6">
        <div class="form-group">
            <label class="control-label col-sm-4">Rp <span class="pull-right target-mask" data-t="remain">0</span></label>
                <div class="col-sm-8">
                    <input type="number" min="0" readonly="true" id="i-remain" class="form-control money-mask" data-t="remain" name="{{__('remaining')}}" value="0">
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-6">
            <div class="form-group">
                <label class="control-label col-sm-4">{{__('Order date')}}</label>
                <div class="col-sm-8">
                    <input data-toggle="datepicker" readonly="true" type="text" class="form-control" name={{__('order_date')}} value="{{\Carbon\Carbon::today()->format('d-m-Y')}}">
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-sm-4">{{ __('Tempo')}}</label>
                <div class="col-sm-8">
                    <select class="form-control" name="{{__('tempo')}}">
                        <option disabled selected value>-- {{__('pilih')}} --</option>
                        @foreach(App\Enums\TransactionEnum::tempo as $tempo)
                            <option value="{{$tempo}}">{{$tempo}}</option>
                        @endforeach
                    </select>
                </div>
            </div>
        </div>
        <div class="col-md-6">
            <div class="form-group">
                <label class="control-label col-sm-4">{{ __('Finish date')}}</label>
                <div class="col-sm-8">
                    <input data-toggle="datepicker" readonly="true" type="text" class="form-control" name={{__('finish_date')}} value="{{\Carbon\Carbon::today()->format('d-m-Y')}}">
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-sm-4">{{ __('Charge date')}}</label>
                <div class="col-sm-8">
                    <input data-toggle="datepicker" readonly="true" type="text" class="form-control" name={{__('charge_date')}} value="{{\Carbon\Carbon::today()->format('d-m-Y')}}">
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-6">
            <div class="form-group">
                <label class="control-label col-sm-4">{{__('Memo')}}</label>
                <div class="col-sm-8">
                    <textarea type="text" class="form-control" rows="3" name="{{__('memo')}}" ></textarea>
                </div>
            </div>
        </div>
        <div class="col-md-6">
            <div class="form-group">
                <label class="control-label col-sm-4">{{ __('Finish payment')}}</label>
            </div>
            <div class="form-group">
                <label class="control-label col-sm-4">Rp <span class="pull-right target-mask" data-t="finish">0</span></label>
                <div class="col-sm-8">
                    <input type="number" class="form-control money-mask" data-t="finish" name={{__('finish_payment')}} value="0">
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <br>
        <div class="col-md-12 text-center">
        <button type="submit" class="btn btn-primary do-submit" onclick="return confirm('{{__('Are you sure? data that already input can not be edit or delete anymore')}}')">Submit</button>
            <button type="reset" class="btn btn-default">Reset</button>
        </div>
    </div>
</form>
@include('scripts.moneymask')
<script>
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
    
    $.fn.datepicker;
    $('[data-toggle="datepicker"]').datepicker({format:'dd-mm-yyyy',zIndex:2000});

    $('.i-change').on('input change', function(e){ 
        updateprice(); 
    }).on('change', function(){
        if ($(this).val() == '') {
            $(this).val(0);
        }
    });

    function updateprice() {
        var sum = parseFloat($('#i-lens').val()) + parseFloat($('#i-rx').val()) + parseFloat($('#i-frame-price').val());
        var total = $('#total-price');
        total.val(sum); 
        updatemask(total.data('t'), sum);
        var subs = sum - parseFloat($('#i-dp').val());
        var remain = $('#i-remain');
        remain.val(subs);
        updatemask(remain.data('t'), subs);
    }

    $('#create-transaction').submit(function(e){
        e.preventDefault(e);
        $('.do-submit').prop('disabled',true);
        $('.alert-transaction-form').html("").append('<option>-- {{__('select merk frame')}} --</option>');

        $.post('/transaction',$(this).serialize(), function(data){
            $('.do-submit').prop('disabled', false);
            window.location.replace('/transaction');
        }).fail(function(data) {
            $('.do-submit').prop('disabled', false);
            var response = JSON.parse(data.responseText);
            if (typeof(response.errors) != "undefined") {
                var errorString = '<div class="alert alert-danger alert-dismissable">'+
                    '<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button><ul>';
                $.each(response.errors, function(key, val){
                    errorString += '<li>' + val[0] + '</li>';
                });
                errorString += '</ul></div>';
                $('.alert-transaction-form').html(errorString);
                $("#createModal").animate({ scrollTop: 0 }, "slow");
            }
        });
    });

    $('#select-merkframe').change(function(){
        $('#select-frame').html('');
        $.get('/frame-by-merk/'+$(this).val(), function(data){

            $('#select-frame, .ins-select-frame .bootstrap-select.form-control').remove();
            $('.ins-select-frame').append('<select title="{{__('select')}}" data-live-search="true" class="form-control selectpickers" id="select-frame" name="{{__('frame_type')}}" ></select>');

            $(data).each(function(k, v){
                $('#select-frame').append('<option value='+v.id+'>'+v.name+' (stock = '+v.stock+')</option>');
            });
            $('.selectpickers').selectpicker();
        });
    });

    $('.selectpicker').selectpicker();
</script>
@endsection