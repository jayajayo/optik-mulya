@extends('layouts.blank')
@section('content')
<div class="alert-outcome-form">
</div>
<form class="form-horizontal" action="{{ route("financial.inputoutcome") }}" id="create-outcome" role="form" method="POST">
    <input type="hidden" class="form-control" name="_method" value="POST" >
    @csrf
    {{-- <div class="form-group">
        <label class="control-label col-sm-2">{{__('Tanggal')}}</label>
        <div class="col-sm-10">
        <input class="form-control" disabled="disabled" value="1M" name="tanggal">
        </div>
    </div> --}}
    <div class="form-group">
        <label class="control-label col-sm-2">{{__('Date')}}</label>
        <div class="col-sm-10">
            <input data-toggle="datepicker" readonly="true" type="text" class="form-control" name="payment_date" value="{{\Carbon\Carbon::today()->format('d-m-Y')}}">
        </div>
    </div>
    <div class="form-group">
        <label class="control-label col-sm-2">{{__('Code')}}</label>
        <div class="col-sm-10">
            <select class="form-control" name="{{__('code')}}" title="-- pilih status customer --">
                <option value="Lensa">Lensa</option>
                <option value="Frame">Frame</option>
                <option value="Faset">Faset</option>
                <option value="Case">Case</option>
                <option value="Tetes dan Lap">Tetes dan Lap</option>
                <option value="Gaji Karyawan">Gaji Karyawan</option>
                <option value="Gaji Pemilik">Gaji Pemilik</option>
                <option value="Insentif">Insentif</option>
                <option value="Uang Makan">Uang Makan</option>
                <option value="Bensin Mobil">Bensin Mobil</option>
                <option value="Bensin Motor">Bensin Motor</option>
                <option value="Angsuran">Angsuran</option>
                <option value="Service">Service</option>
                <option value="RX">RX</option>
                <option value="Operasional">Operasional</option>
                <option value="Operasional Mobil dan Motor">Operasional Mobil dan Motor</option>
                <option value="dll/Operasional Insidentil">dll/Operasional Insidentil</option>
            </select>
        </div>
    </div>
    <div class="form-group">
        <label class="control-label col-sm-2">{{__('Information')}}</label>
        <div class="col-sm-10">
            <select class="form-control" name="{{__('information')}}" title="-- pilih status customer --">
                <option value="Cash">Cash</option>
                <option value="Transfer">Transfer</option>
            </select>
        </div>
    </div>
    <div class="form-group">
        <label class="control-label col-sm-2">{{__('Nomine')}}</label>
        <div class="col-sm-10">
            <input type="number" name="{{__('nomine')}}" class="form-control" min="0" step="100" value="0">
        </div>
    </div>
    <div class="form-group">
        <label class="control-label col-sm-2">{{__('Memo')}}</label>
        <div class="col-sm-10">
                <textarea class="form-control" rows="3" name="{{__('memo')}}" ></textarea>
        </div>
    </div>
    <div class="form-group">
        <label class="control-label col-sm-2">{{__('Action')}}</label>
        <div class="col-sm-10">
            <button type="submit" class="btn btn-primary">Submit</button>
            <button type="reset" class="btn btn-default">Reset</button>
        </div>
    </div>
</form>
<script>
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });

    $.fn.datepicker;
    $('[data-toggle="datepicker"]').datepicker({format:'dd-mm-yyyy',zIndex:2000});
    
    $('#create-outcome').submit(function(e){
        e.preventDefault(e);
        $('.alert-outcome-form').html("");
        $.post('/financial/out-report/input', $(this).serialize(), function(data){
            window.location.replace('/financial/out-report');
        }).fail(function(data) {
            var response = JSON.parse(data.responseText);
            if (typeof(response.errors) != "undefined") {
                var errorString = '<div class="alert alert-danger alert-dismissable">'+
                    '<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button><ul>';
                $.each(response.errors, function(key, val){
                    errorString += '<li>' + val[0] + '</li>';
                });
                errorString += '</ul></div>';
                $('.alert-outcome-form').html(errorString);
                $("#createModal").animate({ scrollTop: 0 }, "slow");
            }
        });
    });
</script>
@endsection