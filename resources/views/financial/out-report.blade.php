@extends('layouts.admin')

@section('content')
@include('components.navbar', ['active' => 'financial/out-report'])

<div id="page-wrapper">
    @component('components.headerpage')
        {{__('Laporan Pengeluaran')}}
    @endcomponent
    
    <div class="row">
        <div class="col-md-12" style="padding-bottom:10px">
            <form class="form-inline" action="" id="outcome-report-form">
                <div class="form-group input-group" style="padding-bottom:10px">
                    <span class="input-group-addon">{{__('Total omset')}}</span>
                    <input type="text" readonly="true" data-toggle="datepicker" name="start_date" class="form-control" value="{{ \Carbon\Carbon::createFromFormat('Y-m-d', $start)->format('d-m-Y')}}">
                </div>
                <div class="form-group input-group" style="padding-bottom:10px">
                    <span class="input-group-addon">{{__('To')}}</span>
                    <input type="text" readonly="true" data-toggle="datepicker" name="end_date" class="form-control" value="{{ \Carbon\Carbon::createFromFormat('Y-m-d', $end)->format('d-m-Y')}}">
                     <span class="input-group-btn">
                        <button class="btn btn-primary" type="submit"><i class="fa fa-search"></i></button>
                    </span>
                 </div>
             </form>                
         </div>
        <div class="col-md-9" style="padding-bottom:10px">
            <button class="btn btn-primary add-new-outcome">Tambah Data Pengeluaran</button>
        </div>
        <div class="col-md-12">
            @if(session()->has('status'))
                @component('components.alertsuccess')
                    {{ session()->get('status') }}
                @endcomponent
            @endif
            <div class="alert-download-form">
            </div>
            <div class="panel panel-default">
                <div class="panel-heading dt-bootstrap">
                    {{__('Laporan Pengeluaran')}}
                </div>
                <div class="panel-body">
                    <div class="row">
                        <div class="col-md-12 table-responsive">
                            <table id="users-table" class="table table-striped table-bordered table-hover dataTable">
                                <thead>
                                    <tr>
                                        {{-- <th>{{__('Date')}}</th> --}}
                                        <th width="10%" style="text-align: center">Tanggal</th>
                                        <th width="18%" style="text-align: center">{{__('Code')}}</th>
                                        <th width="10%">{{__('Keterangan')}}</th>
                                        <th width="18%" style="text-align: center">{{__('Nomine')}}</th>
                                        <th style="text-align: center">{{__('Memo')}}</th>
                                        <th width="15%" style="text-align: center">{{__('Action')}}</th>
                                    </tr>
                                </thead>
                                <tbody id="row-data-table">
                                    @if($data->count())
                                    @php($total=0)
                                    @foreach($data as $key)
                                    <tr>
                                        {{-- <td>{{$key->code}}</td> --}}
                                        <td>{{date('d-m-Y',strtotime($key['payment_date']))}}</td>
                                        <td>{{$key->code}}</td>
                                        <td>{{$key->information}}</td>
                                        @php($total+=$key->nomine)
                                        <td class="text-right">Rp {{number_format($key->nomine,2,',','.')}}</td>
                                        <td>{{$key->memo}}</td>
                                        <td> 
                                            <form method="post" action="{{route('financial.deleteoutcome',$key['id'])}}">
                                                <button class="btn btn-default outcome-edit" data-id="{{$key->id}}">{{__('Edit')}}</button> 
                                                @csrf
                                                @method('DELETE')
                                                <button type="submit" class="btn btn-danger" onclick="return confirm('{{ __('Are you sure?') }}')">{{__('Delete')}}</button> 
                                            </form>
{{--                                             
                                            <form method="post" action="{{url('/frame/'.$payment->id)}}">
                                                @csrf
                                                @method('DELETE')
                                                <button type="submit" class="btn btn-danger frame-delete" data-id="{{$payment->id}}" onclick="return confirm('{{ __('Are you sure?') }}')">{{__('Delete')}}</button> 
                                            </form> --}}
                                            
                                        </td>
                                    </tr>
                                    @endforeach
                                    <tr>
                                        <td colspan="2"></td>
                                        <td><b>{{__('Total')}}</b></td>
                                        <td class="text-right">Rp {{number_format($total,2,'.','.')}}</td>
                                        <td colspan="2"></td>
                                    </tr>
                                    @else
                                    <tr class="data-not-found">
                                        <td colspan="6">{{__('Data not found')}}</td>
                                    </tr>
                                    @endif 
                                </tbody>
                            </table>
                            {{-- <div class="text-left">
                                <b>{{__('Total')}}</b>
                                <b>Rp {{number_format($total,2,',','.')}}</b>
                            </div> --}}
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12 text-right">
                            <button class="btn btn-default" id="download-report">Download</button>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>
</div>
<div class="modal fade" id="createModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title" id="createModalLabel">Tambah Data Pengeluaran</h4>
            </div>
            <div class="modal-body" id="form-create">
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">{{__('Close')}}</button>
            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="editModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title" id="editModalLabel">{{__('Edit Pengeluaran')}}</h4>
            </div>
            <div class="modal-body" id="form-edit">
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">{{__('Close')}}</button>
            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="createModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title" id="createModalLabel">{{__('Add Outcome')}}</h4>
            </div>
            <div class="modal-body" id="form-create">
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">{{__('Close')}}</button>
            </div>
        </div>
    </div>
</div>
@endsection

@push('style')
<link href="{{ asset('vendor/datepicker/datepicker.min.css') }}" rel="stylesheet">
<script src="{{ asset('vendor/datepicker/datepicker.min.js') }}"></script>
@endpush

@push('scripts')
<script>
$.ajaxSetup({
    headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    }
});

$('.add-new-outcome').click(function(e){
    e.preventDefault(e);
    $.get('/financial/out-report/create',function(data){
        $('#form-create').html(data);
        $('#createModal').modal('show');
    });
});

$('#download-report').click(function(){
    var notfound = $('.data-not-found');
    if(notfound.length) {
        var errorString = '<div class="alert alert-danger alert-dismissable">'+
            '<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button><ul><li>{{__("Data not found")}}</li></ul></div>';
        $('.alert-download-form').html(errorString);
        return;
    }
    var formserialize = $('#outcome-report-form').serialize();
    window.location.href = '/financial/outcome-report-download?'+formserialize;
});

$('.outcome-edit').click(function (e){
    e.preventDefault(e);
    var id = $(this).data('id');
    $.get('/financial/out-report/edit/'+id+'', function(data) {
        $('#form-edit').html(data);
        $('#editModal').modal('show');
    });
    });
</script>
@include('scripts.datepicker')
@endpush
