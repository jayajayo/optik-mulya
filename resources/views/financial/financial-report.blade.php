@extends('layouts.admin')

@section('content')
@include('components.navbar', ['active' => 'financial/financial-report'])

<div id="page-wrapper">
    @component('components.headerpage')
        {{__('Laporan Keuangan')}}
    @endcomponent
    <p style = "color:grey">SALDO SAAT INI</p>
    <div class="row">
        <div class="col-md-4" >
            <div class="form-group input-group">
                <span class="input-group-addon">Tunai</span>
                <p type="text" class="form-control" value="">Rp {{number_format($allCash,2,',','.')}}</p>
            </div>
        </div>
        <div class="col-md-4">
            <div class="form-group input-group">
                <span class="input-group-addon">Transfer</span>
                <p type="text" class="form-control" value="">Rp {{number_format($allTransfer,2,',','.')}}</p>
            </div>
        </div>
        <div class="col-md-4">
            <div class="form-group input-group">
                <span class="input-group-addon">Total</span>
                <p type="text" class="form-control" value="">Rp {{number_format($allCash + $allTransfer,2,',','.')}}</p>
            </div>
        </div>
    </div>
    <hr>


    <div class="row">
        <div class="col-md-12">
            @if(session()->has('status'))
                @component('components.alertsuccess')
                    {{ session()->get('status') }}
                @endcomponent
            @endif
            <div class="alert-download-form">
            </div>
            <div class="panel panel-default">
                <div class="panel-heading dt-bootstrap">
                    <div class="row">
                        <div class="col-md-4" style="padding-top:8px">
                            {{__('Financial report')}}
                        </div>
                        <div class="col-md-8" style="text-align:right">
                            <form class="form-inline" action="" id="financial-report-form" >
                                <div class="form-group input-group">
                                    <span class="input-group-addon">{{__('Tanggal')}}</span>
                                    <input type="text" readonly="true" data-toggle="datepicker" name="start_date" class="form-control" value="{{ \Carbon\Carbon::createFromFormat('Y-m-d', $start)->format('d-m-Y')}}">
                                </div>
                                <div class="form-group input-group">
                                    <span class="input-group-addon">{{__('To')}}</span>
                                    <input type="text" readonly="true" data-toggle="datepicker" name="end_date" class="form-control" value="{{ \Carbon\Carbon::createFromFormat('Y-m-d', $end)->format('d-m-Y')}}">
                                    <span class="input-group-btn">
                                        <button class="btn btn-primary" type="submit"><i class="fa fa-search"></i></button>
                                    </span>
                                </div>
                            </form>                
                        </div>
                    </div>
                </div>
                <div class="panel-body">
                    <div class="row">
                        <div class="col-md-4" >
                            <div class="form-group input-group" >
                                <span class="input-group-addon">Saldo Tunai</span>
                                <p type="text" class="form-control" value="">Rp {{number_format($cash,2,',','.')}}</p>
                            </div>
                        </div>
                        <div class="col-md-4" >
                            <div class="form-group input-group">
                                <span class="input-group-addon">Saldo Transfer</span>
                                <p type="text" class="form-control" value="">Rp {{number_format($transfer,2,',','.')}}</p>
                            </div>
                        </div>
                        <div class="col-md-4" >
                            <div class="form-group input-group">
                                <span class="input-group-addon">Saldo Total</span>
                                <p type="text" class="form-control" value="">Rp {{number_format($cash + $transfer,2,',','.')}}</p>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12 table-responsive">
                            <table id="users-table" class="table table-striped table-bordered table-hover dataTable">
                                <thead>
                                    <tr>
                                        {{-- <th>{{__('Date')}}</th> --}}
                                        <th width="10%" style="text-align: center">Tanggal</th>
                                        <th width="18%" style="text-align: center">Keterangan</th>
                                        <th width="10%" style="text-align: center">{{__('Code')}}</th>
                                        <th width="18%" style="text-align: center">{{__('Masuk')}}</th>
                                        <th width="18%" style="text-align: center">{{__('Keluar')}}</th>
                                        <th>{{__('Memo')}}</th>
                                        {{-- <th>{{__('Action')}}</th> --}}
                                    </tr>
                                </thead>
                                <tbody id="row-data-table">
                                    @if($data->count())
                                    @php($total=0)
                                    @php($out=0)
                                    @foreach($data as $key)
                                    <tr>
                                        {{-- <td>{{$key->code}}</td> --}}
                                        <td>{{date('d-m-Y',strtotime($key['payment_date']))}}</td>
                                        <td>{{!empty($key['code']) ? $key['code'] : 'Uang Masuk Harian'}}</td>
                                        <td>{{@$key['information']}}</td>
                                        @if(@$key['jenis']=='income')
                                        @php($total+=@$key['nomine'])
                                        <td class="text-right">Rp {{number_format(@$key['nomine'],2,',','.')}}</td>
                                        <td></td>
                                        @else
                                        <td></td>
                                        @php($out+=$key['nomine'])
                                        <td class="text-right">Rp {{number_format(@$key['nomine'],2,',','.')}}</td>
                                        @endif
                                        <td>{{@$key['memo']}}</td>
                                    </tr>
                                    @endforeach
                                    <tr>
                                        <td colspan="2"></td>
                                        <td><b>{{__('Total')}}</b></td>
                                        <td class="text-right">Rp {{number_format($total,2,'.','.')}}</td>
                                        <td class="text-right">Rp {{number_format($out,2,'.','.')}}</td>
                                        <td colspan="2"></td>
                                    </tr>
                                    @else
                                    <tr class="data-not-found">
                                        <td colspan="6">{{__('Data not found')}}</td>
                                    </tr>
                                    @endif 
                                </tbody>
                            </table>
                        </div>
                    </div>
                    {{-- <div class="row">
                        <div class="col-md-12 text-right">
                            <a href="/financial/financial-report/cetak_pdf?start_date={{$start}}&end_date={{$end}}" target="_blank" class="btn btn-default">Print</a>
                        </div>
                    </div> --}}
                    <div class="row">
                        <div class="col-md-12 text-right">
                            <button class="btn btn-default" id="download-report">Download</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="createModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title" id="createModalLabel">Tambah Data Pemasukan</h4>
            </div>
            <div class="modal-body" id="form-create">
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">{{__('Close')}}</button>
            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="editModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title" id="editModalLabel">{{__('Edit payment')}}</h4>
            </div>
            <div class="modal-body" id="form-edit">
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">{{__('Close')}}</button>
            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="createModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title" id="createModalLabel">{{__('Add Income')}}</h4>
            </div>
            <div class="modal-body" id="form-create">
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">{{__('Close')}}</button>
            </div>
        </div>
    </div>
</div>
@endsection

@push('style')
<link href="{{ asset('vendor/datepicker/datepicker.min.css') }}" rel="stylesheet">
<script src="{{ asset('vendor/datepicker/datepicker.min.js') }}"></script>
@endpush

@push('scripts')
<script>
$.ajaxSetup({
    headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    }
});

$('.add-new-payment').click(function(e){
    e.preventDefault(e);
    $.post('/deposit/payment-form',function(data){
        $('#form-create').html(data);
        $('#createModal').modal('show');
    });
})

$('#download-report').click(function(){
    var notfound = $('.data-not-found');
    if(notfound.length) {
        var errorString = '<div class="alert alert-danger alert-dismissable">'+
            '<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button><ul><li>{{__("Data not found")}}</li></ul></div>';
        $('.alert-download-form').html(errorString);
        return;
    }
    var formserialize = $('#financial-report-form').serialize();
    window.location.href = '/financial/financial-report-download?'+formserialize;
});

$('.payment-edit').click(function (e){
    e.preventDefault(e);
    var id = $(this).data('id');
    $.get('/payment/payment-report/edit/'+id+'', function(data) {
        $('#form-edit').html(data);
        $('#editModal').modal('show');
    });
    });

    $(document).ready(function() {
    var table = $('#users-table').DataTable( {
        
        scrollCollapse: true,
        paging:         true,
        ordering:       false,
        searching:      false,
        pageLength:     50,
        info:           false,
    } );
});
</script>
@include('scripts.datepicker')
@endpush
