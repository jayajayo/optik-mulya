@extends('layouts.blank')
@section('content')
{{-- @include('components.navbar', ['active' => 'payment/payment-report']) --}}
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.12.4/css/bootstrap-select.min.css">


 
    
            @if(session()->has('status'))
                @component('components.alertsuccess')
                {{ session()->get('status') }}
                @endcomponent
            @endif
            <div class="alert-outcome-form">
            </div>
        <form class="form-horizontal" action="{{ route("financial.update-out-report",['id' => $data->id]) }}" id="create-outcome" role="form" method="POST">
                <input type="hidden" class="form-control" name="_method" value="POST" >
                @csrf
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label class="control-label col-sm-3">{{__('Date')}}</label>
                                            <div class="col-sm-9">
                                                <input data-toggle="datepicker" readonly="true" type="text" class="form-control" name="payment_date" value="{{\Carbon\Carbon::today()->format('d-m-Y')}}">
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="control-label col-sm-3">{{__('Code')}}</label>
                                            <div class="col-sm-9">
                                                <select id="kode-select" class="form-control" name="{{__('code')}}" value="{{$data->code}}">
                                                    <option value="Lensa">Lensa</option>
                                                    <option value="Frame">Frame</option>
                                                    <option value="Faset">Faset</option>
                                                    <option value="Case">Case</option>
                                                    <option value="Tetes dan Lap">Tetes dan Lap</option>
                                                    <option value="Gaji Karyawan">Gaji Karyawan</option>
                                                    <option value="Gaji Pemilik">Gaji Pemilik</option>
                                                    <option value="Insentif">Insentif</option>
                                                    <option value="Uang Makan">Uang Makan</option>
                                                    <option value="Bensin Mobil">Bensin Mobil</option>
                                                    <option value="Bensin Motor">Bensin Motor</option>
                                                    <option value="Angsuran">Angsuran</option>
                                                    <option value="Service">Service</option>
                                                    <option value="RX">RX</option>
                                                    <option value="Operasional">Operasional</option>
                                                    <option value="Operasional Mobil dan Motor">Operasional Mobil dan Motor</option>
                                                    <option value="dll/ Operasional Insidentil">dll/Operasional Insidentil</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="control-label col-sm-3">{{__('Information')}}</label>
                                            <div class="col-sm-9">
                                                <select class="form-control" name="{{__('information')}}" value="{{$data->information}}">
                                                    <option value="Cash">Cash</option>
                                                    <option value="Transfer">Transfer</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="control-label col-sm-3">{{__('Nominal')}}</label>
                                            <div class="col-sm-9">
                                                <input class="form-control" name="{{__('nominal')}}"  value="{{$data->nomine}}">
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="control-label col-sm-3">{{__('Memo')}}</label>
                                            <div class="col-sm-8">
                                                <textarea type="text" class="form-control" rows="3" name="{{__('memo')}}" >{{$data->memo}}</textarea>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <br>
                                    <div class="col-md-12 text-center">
                                        <button type="submit" class="btn btn-primary">Update</button>
                                        <a href="{{route('financial.out-report')}}" class="btn btn-default">Kembali</a>
                                    </div>
                                </div>
                            </form>



{{-- @push('scripts') --}}
{{-- @include('scripts.moneymask') --}}
<script>
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        $('#kode-select').val("{{$data->code}}");
        $.fn.datepicker;
        $('[data-toggle="datepicker"]').datepicker({format:'dd-mm-yyyy',zIndex:2000});
        
        $('#create-outcome').submit(function(e){
            e.preventDefault(e);
            $('.alert-outcome-form').html("");
            $.post('/financial/out-report/update/{{$data->id}}',$(this).serialize(), function(data){
                window.location.replace('/financial/out-report');
            }).fail(function(data) {
                var response = JSON.parse(data.responseText);
                if (typeof(response.errors) != "undefined") {
                    var errorString = '<div class="alert alert-danger alert-dismissable">'+
                        '<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button><ul>';
                    $.each(response.errors, function(key, val){
                        errorString += '<li>' + val[0] + '</li>';
                    });
                    errorString += '</ul></div>';
                    $('.alert-outcome-form').html(errorString);
                    $("#createModal").animate({ scrollTop: 0 }, "slow");
                }
            });
        });
</script>

{{-- @endpush --}}
@endsection
