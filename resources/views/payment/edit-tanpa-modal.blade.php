@extends('layouts.admin')

@section('content')
@include('components.navbar', ['active' => 'payment/payment-report'])
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.12.4/css/bootstrap-select.min.css">

<div id="page-wrapper">
    @component('components.headerpage')
    {{__('Transaction management')}}
    @endcomponent
    
    <div class="row">
        <div class="col-md-9" style="padding-bottom:10px">
        </div>
        {{-- <div class="col-md-3 text-right" style="padding-bottom:10px">
            <button class="btn btn-default" onclick="window.history.back();"><span class="fas fa-chevron-left"></span> kembali</button>        
        </div> --}}
        <div class="col-md-12">
            @if(session()->has('status'))
                @component('components.alertsuccess')
                {{ session()->get('status') }}
                @endcomponent
            @endif
            <div class="panel panel-default">
                <div class="panel-heading dt-bootstrap">
                    {{__('Edit Payments')}}
                </div>
                <div class="panel-body">
                    <div class="row">
                        <div class="col-md-12 table-responsive">
                            <form class="form-horizontal" method="post" action="" id="create-transaction" role="form">
                                @csrf
                                @method('PUT')
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label class="control-label col-sm-3">{{__('No. nota')}}</label>
                                            <div class="col-sm-9">
                                                <input class="form-control" disabled="disabled" value={{$data->transaction->code}}>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="control-label col-sm-3">{{__('Code')}}</label>
                                            <div class="col-sm-9">
                                                <input class="form-control" name="{{__('code')}}"  value="{{$data->name}}">
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="control-label col-sm-3">{{__('Petugas')}}</label>
                                            <div class="col-sm-9">
                                                <input class="form-control" name="{{__('petugas')}}"  value="{{$data->user->name}}">
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="control-label col-sm-3">{{__('Nominal')}}</label>
                                            <div class="col-sm-9">
                                                <input class="form-control" name="{{__('nominal')}}"  value="{{$data->nomine}}">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <br>
                                    <div class="col-md-12 text-center">
                                        <button type="submit" class="btn btn-primary">Update</button>
                                        <a href="{{route('payment.index')}}" class="btn btn-default">Kembali</a>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                    <div class="row">
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@push('style')
<link href="{{ asset('vendor/datepicker/datepicker.min.css') }}" rel="stylesheet">
<script src="{{ asset('vendor/datepicker/datepicker.min.js') }}"></script>
@endpush

@push('scripts')
@include('scripts.moneymask')
<script>
    $('.i-change').on('input change', function(e){ updateprice() });

    function updateprice() {
        var sum = parseFloat($('#i-lens').val()) + parseFloat($('#i-rx').val()) + parseFloat($('#i-frame-price').val());
        var total = $('#total-price');
        total.val(sum); 
        updatemask(total.data('t'), sum);
        var subs = sum - parseFloat($('#i-dp').val());
        var remain = $('#i-remain');
        remain.val(subs);
        updatemask(remain.data('t'), subs);
    }
    $('#select-merkframe, #select-team-frame').change(function(){
        $('#select-frame, .ins-select-frame .bootstrap-select.form-control').off().remove();
        $('.ins-select-frame').append('<select title="{{__('select')}}" data-live-search="true" class="form-control selectpickers" id="select-frame" name="{{__('frame_type')}}" ></select>');
        $.get('/frame-by-merk/'+$('.selectpicker').selectpicker('val')+'/'+$('#select-team-frame').val(), function(data){
            $(data).each(function(k, v){
                $('#select-frame').append('<option value='+v.id+'>'+v.name+' (stock = '+v.stock+')</option>');
            });
            $('.selectpickers').selectpicker();
        });
    });
    $(document).ready(function(){
        $('.selectpicker').selectpicker();
        $('#select-frame, .ins-select-frame .bootstrap-select.form-control').remove();
        $('.ins-select-frame').append('<select title="{{__('select')}}" data-live-search="true" class="form-control selectpickers" id="select-frame" name="{{__('frame_type')}}" ></select>');

        $.get('/frame-by-merk/'+$('.selectpicker').selectpicker('val')+'/'+$('#select-team-frame').val(), function(data){
            $(data).each(function(k, v){
                let selected = $('.ins-select-frame').data('default') == v.id ? 'selected' : '';                
                $('#select-frame').append('<option value='+v.id+' '+selected+'>'+v.name+' (stock = '+v.stock+')</option>');
            });
        });

        updateprice();
    });
</script>
@include('scripts.closeconfirm')
@include('scripts.datepicker')
@endpush
