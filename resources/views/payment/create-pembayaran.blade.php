@extends('layouts.blank')
@section('content')
<div class="alert-pemb-form">
</div>
<form class="form-horizontal" action="{{ route("payment.inputpembayaran") }}" id="create-pembayaran" role="form" method="POST">
    <input type="hidden" class="form-control" name="_method" value="POST" >
    @csrf
    {{-- <div class="form-group">
        <label class="control-label col-sm-2">{{__('Tanggal')}}</label>
        <div class="col-sm-10">
        <input class="form-control" disabled="disabled" value="1M" name="tanggal">
        </div>
    </div> --}}
    <div class="form-group">
        <label class="control-label col-sm-2">Nama</label>
        <div class="col-sm-10">
            <input class="form-control" name="code">
        </div>   
    </div>
    <div class="form-group">
        <label class="control-label col-sm-2"></label>
        <div class="col-sm-10">
            <button type="submit" class="btn btn-primary">Submit</button>
            <button type="reset" class="btn btn-default">Reset</button>
        </div>
    </div>
</form>
@include('scripts.moneymask')
<script>
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
    
    $('#create-pembayaran').submit(function(e){
        e.preventDefault(e);
        $('.alert-pemb-form').html("");
        $.post({{ route('payment.inputpembayaran') }}, $(this).serialize(), function(data){
            window.location.replace('/payment/payment-info');
        }).fail(function(data) {
            var response = JSON.parse(data.responseText);
            if (typeof(response.errors) != "undefined") {
                var errorString = '<div class="alert alert-danger alert-dismissable">'+
                    '<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button><ul>';
                $.each(response.errors, function(key, val){
                    errorString += '<li>' + val[0] + '</li>';
                });
                errorString += '</ul></div>';
                $('.alert-pemb-form').html(errorString);
                $("#createModal").animate({ scrollTop: 0 }, "slow");
            }
        });
    });
</script>
@endsection