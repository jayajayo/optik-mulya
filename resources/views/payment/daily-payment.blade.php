@extends('layouts.admin')

@section('content')
@include('components.navbar', ['active' => 'payment/daily-payment'])

<div id="page-wrapper">
    @component('components.headerpage')
        {{__('Daily payment')}}
    @endcomponent
    
    <div class="row">
        <div class="col-md-12" style="padding-bottom:10px">
           <form id="daily-payment-form" class="form-inline" action="/payment/daily-payment" >
                <div class="form-group input-group">
                    <span class="input-group-addon">{{__('Date')}}</span>
                    <input type="text" readonly="true" data-toggle="datepicker" name="payment_date" class="form-control" value="{{ !empty(request()->payment_date) ? request()->payment_date : \Carbon\Carbon::today()->format('d-m-Y')}}">
                </div>
                <div class="form-group input-group">
                    <span class="input-group-addon">{{__('User')}}</span>
                    <select type="text" name="user" class="form-control">
                        <option value="0">-- {{__('All')}} --</option>
                        @foreach($users as $userid)
                        <option {{ request()->user == $userid->id ? 'selected' : '' }} value="{{$userid->id}}">{{__($userid->name)}}</option>
                        @endforeach
                    </select>
                </div>
                <div class="form-group input-group">
                    <span class="input-group-addon">{{__('Payment')}}</span>
                    <select type="text" name="payment_method" class="form-control">
                        <option value="0">-- {{__('All')}} --</option>
                        @foreach($types as $method)
                        <option {{ (Request::get('payment_method')== $method->code) ? 'selected' : '' }} value="{{$method->code}}">{{__($method->code)}}</option>
                        @endforeach
                    </select>
                </div>
                <div class="form-group input-group">
                    <button class="btn btn-primary" type="submit"><i class="fa fa-search"></i></button>
                </div>
            </form>                
        </div>
        <div class="col-md-12">
            @if(session()->has('status'))
                @component('components.alertsuccess')
                    {{ session()->get('status') }}
                @endcomponent
            @endif
            <div class="alert-download-form">
            </div>
            <div class="panel panel-default">
                <div class="panel-heading dt-bootstrap">
                    {{__('Daily payment')}}
                </div>
                <div class="panel-body">
                    <div class="row">
                        <div class="col-md-12 table-responsive">
                            <table id="users-table" class="table table-striped table-bordered table-hover dataTable">
                                <thead>
                                    <tr>
                                        <th>{{__('Invoice number')}}</th>
                                        <th>{{__('Name')}}</th>
                                        <th>{{__('Address')}}</th>
                                        <th>{{__('Code')}}</th>
                                        <th>{{__('Nomine')}}</th>
                                    </tr>
                                </thead>
                                <tbody id="row-data-table">
                                    @if($payments->count())
                                    @php($total=0)
                                    @foreach($payments as $payment)
                                    <tr>
                                        <td>{{$payment->transaction->code}}</td>
                                        <td>{{$payment->transaction->name}}</td>
                                        <td>{{$payment->transaction->address}}</td>
                                        <td>{{$payment->name}}</td>
                                        @php($total+=$payment->nomine)
                                        <td>{{number_format($payment->nomine,2,',','.')}}</td>
                                    </tr>
                                    @endforeach
                                    <tr>
                                        <td colspan="2" class="text-right"><b>{{__('Total')}}</b></td>
                                        <td>{{number_format($total,2,',','.')}}</td>
                                    </tr>
                                    @else
                                    <tr class="data-not-found">
                                        <td colspan="5">{{__('Data not found')}}</td>
                                    </tr>
                                    @endif
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12 text-right">
                        @php($currentQueries = request()->query())
                        @php($newQueries = ['accept' => 'accept'])
                        @php($allQueries = array_merge($currentQueries, $newQueries))
                        <button class="btn btn-primary accept-payment" data-url="{{request()->fullUrlWithQuery($allQueries)}}">{{__('Accept')}}</button>
                        @can('transaction_download')
                        <button class="btn btn-default" id="download-report">Download</button>
                        @endcan
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="createModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title" id="createModalLabel">{{__('New Frame')}}</h4>
            </div>
            <div class="modal-body" id="form-create">
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">{{__('Close')}}</button>
            </div>
        </div>
    </div>
</div>
@endsection

@push('style')
<link href="{{ asset('vendor/datepicker/datepicker.min.css') }}" rel="stylesheet">
<script src="{{ asset('vendor/datepicker/datepicker.min.js') }}"></script>
@endpush

@push('scripts')
<script>
$.ajaxSetup({
    headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    }
});

$('.accept-payment').click(function(e){
    e.preventDefault(e);
    if (!confirm("{{__('Are you sure?')}}")) return;
    var url = $(this).data('url');
    window.location.replace(url);
});

$('#download-report').click(function(){
    var notfound = $('.data-not-found');
    if(notfound.length) {
        var errorString = '<div class="alert alert-danger alert-dismissable">'+
            '<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button><ul><li>{{__("Data not found")}}</li></ul></div>';
        $('.alert-download-form').html(errorString);
        return;
    }
    var formserialize = $('#daily-payment-form').serialize();
    window.location.href = '/payment/daily-payment-download?'+formserialize;
});
</script>
@include('scripts.datepicker')
@endpush
