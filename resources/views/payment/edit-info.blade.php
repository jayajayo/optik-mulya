@extends('layouts.blank')

@section('content')
{{-- @include('components.navbar', ['active' => 'payment/payment-report']) --}}
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.12.4/css/bootstrap-select.min.css">

 
    
            @if(session()->has('status'))
                @component('components.alertsuccess')
                {{ session()->get('status') }}
                @endcomponent
            @endif
            <div class="alert-payment-form">
            </div>
        <form class="form-horizontal" action="{{ route("payment.updateinfo",['id' => $data->id]) }}" id="create-payment" role="form" method="POST">
                <input type="hidden" class="form-control" name="_method" value="POST" >
                @csrf
                                <div class="row">
                                    <div class="col-md-7">
                                        <div class="form-group">
                                            <label class="control-label col-sm-3">{{__('Information')}}:</label>
                                            <div class="col-sm-9">
                                                <input class="form-control" name="{{__('information')}}"  value="{{$data->code}}">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <br>
                                    <div class="col-md-12 text-center">
                                        <button type="submit" class="btn btn-primary">Update</button>
                                        <a href="{{route('payment.paymentinfo')}}" class="btn btn-default">Kembali</a>
                                    </div>
                                </div>
                            </form>
@endsection

@push('style')
<link href="{{ asset('vendor/datepicker/datepicker.min.css') }}" rel="stylesheet">
<script src="{{ asset('vendor/datepicker/datepicker.min.js') }}"></script>
@endpush

@push('scripts')
@include('scripts.moneymask')
<script>
    $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        $('#create-payment').submit(function(e){
            e.preventDefault(e);
            $('.alert-payment-form').html("");

            $.post('/payment-report/{{$data->id}}',$(this).serialize(), function(data){
                window.location.replace('/payment/payment-report');
            }).fail(function(data) {
                var response = JSON.parse(data.responseText);
                if (typeof(response.errors) != "undefined") {
                    var errorString = '<div class="alert alert-danger alert-dismissable">'+
                        '<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button><ul>';
                    $.each(response.errors, function(key, val){
                        errorString += '<li>' + val[0] + '</li>';
                    });
                    errorString += '</ul></div>';
                    $('.alert-payment-form').html(errorString);
                    $("#createModal").animate({ scrollTop: 0 }, "slow");
                }
            });
        });

    $('.i-change').on('input change', function(e){ updateprice() });

    $('#select-merkframe, #select-team-frame').change(function(){
        $('#select-frame, .ins-select-frame .bootstrap-select.form-control').off().remove();
        $('.ins-select-frame').append('<select title="{{__('select')}}" data-live-search="true" class="form-control selectpickers" id="select-frame" name="{{__('frame_type')}}" ></select>');
        $.get('/frame-by-merk/'+$('.selectpicker').selectpicker('val')+'/'+$('#select-team-frame').val(), function(data){
            $(data).each(function(k, v){
                $('#select-frame').append('<option value='+v.id+'>'+v.name+' (stock = '+v.stock+')</option>');
            });
            $('.selectpickers').selectpicker();
        });
    });
    $(document).ready(function(){
        $('.selectpicker').selectpicker();
        $('#select-frame, .ins-select-frame .bootstrap-select.form-control').remove();
        $('.ins-select-frame').append('<select title="{{__('select')}}" data-live-search="true" class="form-control selectpickers" id="select-frame" name="{{__('frame_type')}}" ></select>');

        $.get('/frame-by-merk/'+$('.selectpicker').selectpicker('val')+'/'+$('#select-team-frame').val(), function(data){
            $(data).each(function(k, v){
                let selected = $('.ins-select-frame').data('default') == v.id ? 'selected' : '';                
                $('#select-frame').append('<option value='+v.id+' '+selected+'>'+v.name+' (stock = '+v.stock+')</option>');
            });
        });

        updateprice();
    });

    $('#select-merkframe').change(function(){
        $('#select-frame').html('');
        $.get('/frame-by-merk/'+$(this).val(), function(data){

            $('#select-frame, .ins-select-frame .bootstrap-select.form-control').remove();
            $('.ins-select-frame').append('<select title="{{__('select')}}" data-live-search="true" class="form-control selectpickers" id="select-frame" name="{{__('frame_type')}}" ></select>');

            $(data).each(function(k, v){
                $('#select-frame').append('<option value='+v.id+'>'+v.name+' (stock = '+v.stock+')</option>');
            });
            $('.selectpickers').selectpicker();
        });
    });

    $('.selectpicker').selectpicker();

</script>
@include('scripts.closeconfirm')
@include('scripts.datepicker')
@endpush
