@extends('layouts.admin')

@section('content')
@include('components.navbar', ['active' => 'payment/payment-info'])

<div id="page-wrapper">
    @component('components.headerpage')
        {{__('Keterangan Pembayaran')}}
    @endcomponent
    
    <div class="row">
        <div class="col-md-9" style="padding-bottom:10px">
            <button class="btn btn-primary add-new-pemb">{{__('Tambah')}}</button>
        </div>
        <div class="col-md-12">
            @if(session()->has('status'))
                @component('components.alertsuccess')
                    {{ session()->get('status') }}
                @endcomponent
            @endif
            <div class="alert-info-form">
            </div>
            <div class="row">
                <div class="col-md-6">
                    <div class="panel panel-default">
                        <div class="panel-body">
                            <div class="row">
                                <div class="col-md-12 table-responsive">
                                    <table id="users-table" class="table table-striped table-bordered table-hover dataTable">
                                        <thead>
                                            <tr>
                                                <th>Keterangan Pembayaran</th>
                                                <th width="35%" style="text-align: center;">{{__('Action')}}</th>
                                            </tr>
                                        </thead>
                                        <tbody id="row-data-table">
                                                @foreach($types as $type)
                                                    <tr class="info-list-edit" data-id="{{$type->id}}">
                                                        <td>{{$type->code}}</td>
                                                        <td style="text-align: center;">
                                                            @if($type->code!='cash')
                                                            <form method="post" action="{{route('payment.deleteinfo',$type->id)}}">
                                                                <button class="btn btn-default payment-edit" data-id="{{$type->id}}">{{__('Edit')}}</button> 
                                                                @csrf
                                                                @method('DELETE')
                                                                <button type="submit" class="btn btn-danger" onclick="return confirm('{{ __('Are you sure?') }}')">{{__('Delete')}}</button> 
                                                            </form>
                                                            @endif
                                                        </td>
                                                    </tr>
                                                @endforeach
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="createModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title" id="createModalLabel">{{__('New Frame')}}</h4>
            </div>
            <div class="modal-body" id="form-create">
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">{{__('Close')}}</button>
            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="editModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title" id="editModalLabel">{{__('Edit Jenis Pembayaran')}}</h4>
            </div>
            <div class="modal-body" id="form-edit">
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">{{__('Close')}}</button>
            </div>
        </div>
    </div>
</div>
@endsection

@push('style')
<link href="{{ asset('vendor/datepicker/datepicker.min.css') }}" rel="stylesheet">
<script src="{{ asset('vendor/datepicker/datepicker.min.js') }}"></script>
@endpush

@push('scripts')
<script>
$.ajaxSetup({
    headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    }
});

$('.add-new-pemb').click(function(e){
    e.preventDefault(e);
    $.get('/payment/payment-info/create',function(data){
        $('#form-create').html(data);
        $('#createModal').modal('show');
    });
});

// function initEditBill() {
//     $('.edit-payment').off();
//     $('.edit-payment').click(function(e){
//         e.preventDefault(e);
//         var open = $('.editor-open');
//         if (open.length) {
//             $('.editor-open').addClass('has-error').find("input").focus();
//             return;
//         } 
//         var id = $(this).data('id');
//         var type = $(this).data('types');
//         $.get('/payment/form-edit-info/'+types, function(data){
//             $('.info-list-edit[data-id='+id+']').html(data);
//         });
//     });
// }

$('.payment-edit').click(function (e){
    e.preventDefault(e);
    var id = $(this).data('id');
    $.get('/payment/payment-info/edit/'+id+'', function(data) {
        $('#form-edit').html(data);
        $('#editModal').modal('show');
    });
    });
</script>
@include('scripts.datepicker')
@endpush
