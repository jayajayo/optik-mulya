@extends('layouts.blank')

@section('content')
{{-- @include('components.navbar', ['active' => 'payment/payment-report']) --}}
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.12.4/css/bootstrap-select.min.css">
</div>

 
    
            @if(session()->has('status'))
                @component('components.alertsuccess')
                {{ session()->get('status') }}
                @endcomponent
            @endif
            <div class="alert-payment-form">
            </div>
        <form class="form-horizontal" action="{{ route("payment.updatepaymentReport",['id' => $data->id]) }}" id="create-payment" role="form" method="POST">
                <input type="hidden" class="form-control" name="_method" value="POST" >
                @csrf
                                <div class="row">
                                    <div class="col-md-8">
                                        <div class="form-group">
                                            <label class="control-label col-sm-3">{{__('No. nota')}}</label>
                                            <div class="col-sm-9">
                                                <select class="form-control selectpicker select-transaction" name="{{__('no_nota')}}" data-live-search="true">
                                                    @foreach($transaction as $trans)
                                                        @if ($data->transaction_id == $trans->id)
                                                        <option data-tokens="{{$trans->code}}" value="{{$trans->id}}" selected>{{$trans->code}}</option>
                                                        @else
                                                        <option data-tokens="{{$trans->code}}" value="{{$trans->id}}">{{$trans->code}}</option>
                                                        @endif
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="control-label col-sm-3" required="required">{{__('Code')}}</label>
                                            <div class="col-sm-9">
                                                <div class="form-group">
                                                    <div class="col-sm-9">
                                                        
                                                        <!--<input disabled class="form-control" name="{{__('code')}}" value="{{$data->name}}">-->
                                                            
                                                            <select class="form-control select-code" name="{{__('code')}}" required>
                                                            @foreach($payment as $key)
                                                                
                                                                @if (@$data->id == @$key->id)
                                                                    <option data-tokens="{{$key->name}}" selected value="{{$key->name}}">{{$key->name}}</option>
                                                                @else
                                                                    <option data-tokens="{{$key->name}}" value="{{$key->name }}">{{$key->name}}</option> 
                                                                @endif
                                                            
                                                            @endforeach         
                                                            </select>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        
                                        <div class="form-group">
                                            <label class="control-label col-sm-3">{{__('Petugas')}}</label>
                                            <div class="col-sm-9">
                                                <select class="form-control" name="{{__('petugas')}}" >
                                                    @foreach($user as $us)
                                                        @if ($data->recipient_id == $us->id)
                                                            <option value="{{$us->id}}" selected>{{$us->name}}</option>
                                                        @else
                                                            <option value="{{$us->id}}">{{$us->name}}</option>
                                                        @endif
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="control-label col-sm-3">{{__('Nominal')}}</label>
                                            <div class="col-sm-9">
                                                <input class="form-control" name="{{__('nominal')}}"  value="{{$data->nomine}}">
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="control-label col-sm-3">{{__('Name')}}</label>
                                            <div class="col-sm-9" >
                                            <input name="{{__('name')}}" id="name" disabled="disabled" class="form-control" value="{{$data->transaction->name}}">
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="control-label col-sm-3">{{__('Address')}}</label>
                                            <div class="col-sm-9">
                                                <textarea rows="5" name="{{__('address')}}" id="address" disabled="disabled" class="form-control">{{$data->transaction->address}}</textarea>
                                            </div>
                                        </div>
                                    </div>
                                   
                                        
                                    
                                </div>
                                <div class="row">
                                    <br>
                                    <div class="col-md-12 text-center">
                                        <button type="submit" class="btn btn-primary">Update</button>
                                        <a href="{{route('payment.index')}}" class="btn btn-default">Kembali</a>
                                    </div>
                                </div>
                            </form>
@endsection

@push('style')
<link href="{{ asset('vendor/datepicker/datepicker.min.css') }}" rel="stylesheet">
<script src="{{ asset('vendor/datepicker/datepicker.min.js') }}"></script>
@endpush


<script>
    $(document).ready(function() {
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

     
        $('.selectpicker').selectpicker();

        $('.select-transaction').change(function(e){
            
            $('#select-address').html('<input name="{{__('address')}}" class="form-control">');
            $.post('/payment/get-transaction/'+$(this).val(), function(data){
                $('#address').val(data.address);
            });
            $('#select-name').html('<input name="{{__('name')}}" class="form-control">');
            $.post('/payment/get-transaction/'+$(this).val(), function(data){
                $('#name').val(data.name);
            });
            
        });
        $('.select-transaction').change(function(e){
            $('.select-code').html('<option  disabled selected value>{{isset($data->name) ? __($data->name) : "__('code')"}}</option>');
            $.post('/payment/get-payment/'+$(this).val(), function(data){
                $.each(data, function(k,v) {
                    $('.select-code').append('<option value="'+v.name+'">'+v.name+'</option>')
                })
            });
        });
        $('#create-payment').submit(function(e){
            e.preventDefault(e);
            $('.alert-payment-form').html("");

            // $.post('/payment/payment-report/update/{{$data->id}}',$(this).serialize(), function(data){
            //     console.log(data);
            // });
            $.post('/payment/payment-report/update/{{$data->id}}',$(this).serialize(), function(data){
               window.location.replace('/payment/payment-report');
            }).fail(function(data) {
                var response = JSON.parse(data.responseText);
                if (typeof(response.errors) != "undefined") {
                    var errorString = '<div class="alert alert-danger alert-dismissable">'+
                        '<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button><ul>';
                    $.each(response.errors, function(key, val){
                        errorString += '<li>' + val[0] + '</li>';
                    });
                    errorString += '</ul></div>';
                    $('.alert-payment-form').html(errorString);
                    $("#createModal").animate({ scrollTop: 0 }, "slow");
                }
            });
        });

        
    });
    

</script>
@include('scripts.closeconfirm')
@include('scripts.datepicker')
