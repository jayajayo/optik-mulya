@extends('layouts.admin')

@section('content')
@include('components.navbar', ['active' => 'payment/payment-report'])

<div id="page-wrapper">
    @component('components.headerpage')
        {{__('Payment report')}}
    @endcomponent
    
    <div class="row">
        <div class="col-md-12" style="padding-bottom:10px">
           <form class="form-inline" action="/payment/payment-report"id="daily-payment-form" >
                <div class="form-group input-group" style="padding-bottom:10px">
                    <span class="input-group-addon">{{__('Date')}}</span>
                    <input type="text" readonly="true" data-toggle="datepicker" name="payment_date_start" class="form-control" value="{{ !empty(request()->payment_date_start) ? request()->payment_date_start : \Carbon\Carbon::today()->format('d-m-Y')}}">
                </div>
                <div class="form-group input-group" style="padding-bottom:10px">
                    <span class="input-group-addon">{{__('To')}}</span>
                    <input type="text" readonly="true" data-toggle="datepicker" name="payment_date_end" class="form-control" value="{{ !empty(request()->payment_date_end) ? request()->payment_date_end : \Carbon\Carbon::today()->format('d-m-Y')}}">
                </div>
                <div class="form-group input-group" style="padding-bottom:10px">
                    <span class="input-group-addon">{{__('User')}}</span>
                    <select type="text" name="user" class="form-control">
                        <option value="0">-- {{__('All')}} --</option>
                        @foreach($users as $userid)
                        <option {{ request()->user == $userid->id ? 'selected' : '' }} value="{{$userid->id}}">{{__($userid->name)}}</option>
                        @endforeach
                    </select>
                </div>
                <div class="form-group input-group" style="padding-bottom:10px">
                    <span class="input-group-addon">{{__('Kode pembayaran')}}</span>
                    <select type="text" name="payment_id" class="form-control">
                        <option value="0">-- {{__('All')}} --</option>
                        @foreach(\App\Enums\TransactionEnum::payments as $payment_id)
                        <option {{ request()->payment_id == $payment_id ? 'selected' : '' }} value="{{$payment_id}}">{{__($payment_id)}}</option>
                        @endforeach
                    </select>
                </div>
                <div class="form-group input-group" style="padding-bottom:10px">
                    <span class="input-group-addon">{{__('Payment')}}</span>
                    <select type="text" name="payment_method" class="form-control">
                        <option value="0">-- {{__('All')}} --</option>
                        @foreach($types as $type)
                        <option {{ request()->payment_method == $type->code ? 'selected' : '' }} value="{{$type->code}}">{{$type->code}}</option>
                        @endforeach
                    </select>
                </div>
                <div class="form-group input-group" style="padding-bottom:10px">
                    <button class="btn btn-primary" type="submit"><i class="fa fa-search"></i></button>
                </div>
            </form>                
        </div>
        <div class="col-md-12">
            @if(session()->has('status'))
                @component('components.alertsuccess')
                    {{ session()->get('status') }}
                @endcomponent
            @endif
            <div class="alert-download-form">
            </div>
            <div class="panel panel-default">
                <div class="panel-heading dt-bootstrap">
                    {{__('Payment report')}}
                </div>
                <div class="panel-body">
                    <div class="row">
                        <div class="col-md-12 table-responsive">
                            <table id="users-table" class="table table-striped table-bordered table-hover dataTable">
                                <thead>
                                    <tr>
                                        <th width="70px">Tanggal</th>
                                        <th>{{__('Invoice number')}}</th>
                                        <th>Nama</th>
                                        <th>Alamat</th>
                                        <th>{{__('Code')}}</th>
                                        <th>{{__('Operator')}}</th>
                                        <th>Jenis Pembayaran</th>
                                        <th>{{__('Nomine')}}</th>
                                        <th width="120px">{{__('Action')}}</th>
                                    </tr>
                                </thead>
                                <tbody id="row-data-table">
                                    @if($payments->count())
                                    @php($total=0)
                                    @foreach($payments as $payment)
                                    <tr>
                                        <td>{{date('d-m-Y',strtotime($payment->payment_date))}}</td>
                                        <td>{{$payment->transaction->code}}</td>
                                        <td>{{$payment->transaction->name}}</td>
                                        <td>{{$payment->transaction->address}}</td>
                                        <td>{{$payment->name}}</td>
                                        <td>{{$payment->recipient->name}}</td>
                                        @php($total+=$payment->nomine)
                                        <td>{{$payment->information}}</td>
                                        <td>{{number_format($payment->nomine,2,',','.')}}</td>
                                        <td>
                                        
                                            <form method="post" action="{{route('payment.deletepayment',$payment->id)}}">
                                                <button class="btn btn-default payment-edit" data-id="{{$payment->id}}">{{__('Edit')}}</button> 
                                                @csrf
                                                @method('DELETE')
                                                <button type="submit" class="btn btn-danger frame-delete" data-id="{{$payment->id}}" onclick="return confirm('{{ __('Are you sure?') }}')">{{__('Delete')}}</button> 
                                            </form>
                                            
                                        </td>
                                    </tr>
                                    @endforeach
                                    <tr>
                                        <td colspan="7" class="text-right"><b>{{__('Total')}}</b></td>
                                        <td>{{number_format($total,2,',','.')}}</td>
                                        <td colspan="1"></td>
                                    </tr>
                                    @else
                                    <tr class="data-not-found">
                                        <td colspan="8">{{__('Data not found')}}</td>
                                    </tr>
                                    @endif
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12 text-right">
                            @can('transaction_download')
                            <button class="btn btn-default" id="download-report">Download</button>
                            @endcan
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="createModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title" id="createModalLabel">{{__('New Payment')}}</h4>
            </div>
            <div class="modal-body" id="form-create">
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">{{__('Close')}}</button>
            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="editModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title" id="editModalLabel">Edit Data Pembayaran</h4>
            </div>
            <div class="modal-body" id="form-edit">
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">{{__('Close')}}</button>
            </div>
        </div>
    </div>
</div>
@endsection

@push('style')
<link href="{{ asset('vendor/datepicker/datepicker.min.css') }}" rel="stylesheet">
<script src="{{ asset('vendor/datepicker/datepicker.min.js') }}"></script>
@endpush

@push('scripts')
<script>
$.ajaxSetup({
    headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    }
});
$('#download-report').click(function(){
    var notfound = $('.data-not-found');
    if(notfound.length) {
        var errorString = '<div class="alert alert-danger alert-dismissable">'+
            '<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button><ul><li>{{__("Data not found")}}</li></ul></div>';
        $('.alert-download-form').html(errorString);
        return;
    }
    var formserialize = $('#daily-payment-form').serialize();
    window.location.href = '/payment/payment-report-download?'+formserialize;
});

$('.payment-edit').click(function (e){
    e.preventDefault(e);
    var id = $(this).data('id');
    $.get('/payment/payment-report/edit/'+id+'', function(data) {
        $('#form-edit').html(data);
        $('#editModal').modal('show');
    });
    });

    
</script>
@include('scripts.datepicker')
@endpush
