@extends('layouts.blank')
@section('content')
<div class="alert-merk-frame-form">
</div>
@if(session()->has('status'))
    @component('components.alertsuccess')
        {{ session()->get('status') }}
    @endcomponent
@endif
<div class="row">
    <div class="col-md-8" style="padding-bottom:10px">
        <form id="add-merk-frame" class="form-inline" action="" >
            <div class="form-group input-group">
                <input type="text" name="{{__('merk_name')}}" class="form-control" placeholder="{{__('Merk')}}">
                <span class="input-group-btn">
                    <button class="btn btn-default" type="submit">{{__('Add new')}}</button>
                </span>
            </div>
        </form>   
    </div>
    <div class="col-md-4" style="padding-bottom:10px">
        <form id="search-merk-frame" class="form-inline" action="" >
            <div class="form-group input-group">
                <input type="text" name="keyword" class="form-control" placeholder="{{__('Search')}}" value="{{$keyword or ''}}">
                <span class="input-group-btn">
                    <button class="btn btn-primary" type="submit"><i class="fa fa-search"></i></button>
                </span>
            </div>
        </form>                
    </div>
    <div class="col-md-12 table-responsive">
        <table id="frame-table" class="table table-striped table-bordered table-hover dataTable">
            <thead>
                <tr>
                    <th class="col-md-6">Merk</th>
                    <th class="col-md-6">Action</th>
                </tr>
            </thead>
            <tbody id="row-data-table">
                @if($merkframes->count())
                    @foreach($merkframes as $merkframe)
                        <tr>
                            <td>{{$merkframe->name}}</td>
                            <td>
                                <form class="form-delete-merk-frame" data-id="{{$merkframe->id}}">
                                    <div class="merk-frame-delete-confirmation" data-id="{{$merkframe->id}}" style="display:none">
                                        <span>{{ __('Are you sure?') }}</span>
                                        <input type="hidden" name="_method" value="DELETE">
                                        <input type="hidden" name="id" value="{{$merkframe->id}}">
                                        <button class="btn btn-danger do-delete-merk-frame" data-id="{{$merkframe->id}}">{{__('Yes')}}</button>
                                        <button class="btn btn-default cancel-delete-merk-frame">{{__('Cancel')}}</button>
                                    </div>
                                    <button class="btn btn-danger delete-merk-frame" data-id="{{$merkframe->id}}">{{__('Delete')}}</button>
                                </form>
                            </td>
                        </tr>
                    @endforeach
                @else
                    <tr>
                        <td colspan="10">{{__('Data not found')}}</td>
                    </tr>
                @endif
            </tbody>
        </table>
    </div>
</div>
<script>
    $(document).ready(function(){
        App.removeAlertSuccess();
    });

    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });

    $('#search-merk-frame').submit(function (e){
        e.preventDefault(e);
        $.get('/merk-frame', $(this).serialize(), function(data) {
            $('#form-manage-merk-frame').html(data);
        });
    });

    $('#add-merk-frame').submit(function (e){
        e.preventDefault(e);
        $.post('/merk-frame',$(this).serialize(), function(data){
            $.get('/merk-frame', $(this).serialize(), function(data) {
                $('#form-manage-merk-frame').html(data);
            });
        }).fail(function(data) {
            var response = JSON.parse(data.responseText);
            if (typeof(response.errors) != "undefined") {
                var errorString = '<div class="alert alert-danger alert-dismissable">'+
                    '<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button><ul>';
                $.each(response.errors, function(key, val){
                    errorString += '<li>' + val[0] + '</li>';
                });
                errorString += '</ul></div>';
                $('.alert-merk-frame-form').html(errorString);
                $("#createModal").animate({ scrollTop: 0 }, "slow");
            }
        });
    });

    $('.delete-merk-frame').click(function (e){
        e.preventDefault(e);
        var id = $(this).data('id');
        $('.delete-merk-frame').show();
        $('.merk-frame-delete-confirmation').hide();
        $('.merk-frame-delete-confirmation[data-id='+id+']').show();
        $(this).hide();         
    });

    $('.cancel-delete-merk-frame').click(function(e){
        e.preventDefault(e);
        $('.merk-frame-delete-confirmation').hide();
        $('.delete-merk-frame').show();
    });

    $('.do-delete-merk-frame').click(function(e){
        e.preventDefault(e);
        var id = $(this).data('id');
        $.post('/merk-frame/'+id, $('.form-delete-merk-frame[data-id='+id+']').serialize(), function(data){
            $.get('/merk-frame', $(this).serialize(), function(data) {
                $('#form-manage-merk-frame').html(data);
            });
        }).fail(function(data) {
            var response = JSON.parse(data.responseText);
            if (typeof(response.errors) != "undefined") {
                var errorString = '<div class="alert alert-danger alert-dismissable">'+
                    '<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button><ul>';
                $.each(response.errors, function(key, val){
                    errorString += '<li>' + val[0] + '</li>';
                });
                errorString += '</ul></div>';
                $('.alert-merk-frame-form').html(errorString);
                $("#createModal").animate({ scrollTop: 0 }, "slow");
            }
        });
    });
</script>
@endsection