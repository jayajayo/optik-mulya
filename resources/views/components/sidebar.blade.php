<div class="navbar-default sidebar" role="navigation">
    <div class="sidebar-nav navbar-collapse collapse" aria-expanded="false">
        <ul class="nav" id="side-menu">
            @can('manage_user')
            <li ><a href="{{url('user')}}" class="{{ ($active == 'user') ? 'active' : '' }}"><i class="fas fa-users fa-fw"></i> {{__('Users')}}</a></li>
            @endcan
            @can('manage_frame')
            <li ><a href="{{url('frame')}}" class="{{ ($active == 'frame') ? 'active' : '' }}"><i class="fas fa-glasses fa-fw"></i> {{__('Frame')}}</a></li>
            @endcan
            @can('transaction_show')
            <li ><a href="{{url('transaction')}}" class="{{ ($active == 'transaction') ? 'active' : '' }}"><i class="fas fa-users fa-fw"></i> {{__('Transaction ')}}</a></li>
            @endcan
            @if(auth()->user()->can('main_deposit') || auth()->user()->can('manage_deposit'))
            <li class="{{ ($active == 'deposit/bill-list') || $active == 'deposit/payment-list' ? 'active' : '' }}">
                <a href="#"><i class="fas fa-money-bill fa-fw"></i> {{__('Deposit')}}<span class="fa arrow"></span></a>
                <ul class="nav nav-second-level collapse">
                    @can('main_deposit')
                    <li ><a href="{{url('deposit/bill-list')}}" class="{{ ($active == 'deposit/bill-list') ? 'active' : '' }}"><i class="fas fa-list-ul fa-fw"></i> {{__('Bill list')}}</a></li>
                    @endcan
                    @can('manage_deposit')
                    <li ><a href="{{url('deposit/payment-list')}}" class="{{ ($active == 'deposit/payment-list') ? 'active' : '' }}"><i class="fas fa-exchange-alt fa-fw"></i> {{__('Receive list')}}</a></li>
                    @endcan
                </ul>
            </li>
            @endif
            @can('manage_payment')
            <li class="{{ ($active == 'payment/daily-payment') || $active == 'payment/payment-report' ? 'active' : '' }}">
                <a href="#" ><i class="fas fa-clipboard-check fa-fw"></i> {{__('Payment')}}<span class="fa arrow"></span></a>
                <ul class="nav nav-second-level collapse">
                    @can('acc_payment')
                    <li ><a href="{{url('payment/daily-payment')}}" class="{{ ($active == 'payment/daily-payment') ? 'active' : '' }}"><i class="fas fa-clock fa-fw"></i> {{__('Daily payment')}}</a></li>
                    @endcan
                    @can('manage_payment')
                    <li ><a href="{{url('payment/payment-report')}}"  class="{{ ($active == 'payment/payment-report') ? 'active' : '' }}"><i class="fas fa-file-alt fa-fw"></i> {{__('Payment report')}}</a></li>
                    @endcan
                    @can('delete_user')
                    <li ><a href="{{url('payment/payment-info')}}"  class="{{ ($active == 'payment/payment-info') ? 'active' : '' }}"><i class="fas fa-file-alt fa-fw"></i> {{__('Keterangan pembayaran')}}</a></li>
                    @endcan
                </ul>
            </li>
            @endcan
            @can('debt_show')
            <li>
                <a href="{{url('debt-list')}}" class="{{ ($active == 'debt-list') ? 'active' : '' }}"><i class="fas fa-credit-card fa-fw"></i> {{__('Debt list')}}</a>
            </li>
            @endcan
            @can('debt_show')
            <li>
            <a href="{{url('omset/omset-report')}}" class="{{ ($active == 'omset/omset-report') ? 'active' : '' }}"><i class="fas fa-credit-card fa-fw"></i> {{__('Data Omset')}}</a>
            </li>
            @endcan
            {{-- @can('debt_show') --}}
            @can('delete_user')
            <li>
                <a href="{{url('financial/')}}" class="{{ ($active == 'financial/income-report') || $active == 'financial/income-report' ? 'active' : '' }}"><i class="fas fa-credit-card fa-fw"></i> {{__('Data Keuangan')}}<span class="fa arrow"></span></a>
                <ul class="nav nav-second-level collapse">
                    @can('delete_user')
                    <li ><a href="{{url('financial/income-report')}}" class="{{ ($active == 'financial/income-report') ? 'active' : '' }}"><i class="fas fa-clock fa-fw"></i> {{__('Laporan pemasukan')}}</a></li>
                    @endcan
                    @can('delete_user')
                    <li ><a href="{{url('financial/out-report')}}"  class="{{ ($active == 'financial/out-report') ? 'active' : '' }}"><i class="fas fa-file-alt fa-fw"></i> {{__('Laporan pengeluaran')}}</a></li>
                    @endcan
                    @can('delete_user')
                    <li ><a href="{{url('financial/financial-report')}}"  class="{{ ($active == 'financial/financial-report') ? 'active' : '' }}"><i class="fas fa-file-alt fa-fw"></i> {{__('Laporan keuangan')}}</a></li>
                    @endcan
                </ul>
            </li>
            @endcan
            {{-- @endcan --}}
            @if(auth()->user()->can('manage_complaint') || auth()->user()->can('action_complaint'))
            <li>
                <a href="{{url('complaint')}}" class="{{ ($active == 'complaint') ? 'active' : '' }}"><i class="fas fa-bullhorn fa-fw"></i> {{__('Complaint management')}}</a>
            </li>
            @endif
            @can('show_status')
            <li>
                <a href="{{url('status-management')}}" class="{{ ($active == 'status-management') ? 'active' : '' }}"><i class="fas fa-tasks fa-fw"></i> {{__('Status management')}}</a>
            </li>
            @endcan
        </ul>
    </div>
</div>