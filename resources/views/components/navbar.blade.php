<nav class="navbar navbar-default navbar-static-top" role="navigation" style="margin-bottom: 0">
    <div class="navbar-header">
        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
        </button>
        <a class="navbar-brand" href="/home"><img src="{{asset('logo.jpeg')}}" alt="{{ config('app.name', 'Laravel') }}" height="35px" style="margin-top:-6px"></a>
    </div>
    @include('components.toplinks', ['active' => $active])
    @include('components.sidebar', ['active' => $active])
</nav>