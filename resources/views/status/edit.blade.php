@extends('layouts.blank')
@section('content')

<div class="alert-status-form">
</div>
<form class="form-horizontal" id="edit-status" role="form">
    @method('PUT')
    <div class="form-group">
        <label class="control-label col-sm-2">{{__('Status')}}</label>
        <div class="col-sm-10">
            <label><input type="checkbox" name="{{__('order_status')}}" value="1" {{ ($transaction->order_status) ? 'checked' : '' }}> {{__('Lens order')}}</label>
        </div>
    </div>
    <div class="form-group">
        <label class="control-label col-sm-2"></label>
        <div class="col-sm-10">
            <label><input type="checkbox" name="{{__('faset_status')}}" value="1" {{ ($transaction->faset_status) ? 'checked' : '' }}> {{__('Faset')}}</label>
        </div>
    </div>
    <div class="form-group">
        <label class="control-label col-sm-2"></label>
        <div class="col-sm-10">
            <label><input type="checkbox" name="{{__('packing_status')}}" value="1" {{ ($transaction->packing_status) ? 'checked' : '' }}> {{__('Packing')}}</label>
        </div>
    </div>
    <div class="form-group">
        <label class="control-label col-sm-2">{{__('Action')}}</label>
        <div class="col-sm-10">
            <button type="submit" class="btn btn-primary">Submit</button>
            <button type="reset" class="btn btn-default">Reset</button>
        </div>
    </div>
</form>
<script>
$.ajaxSetup({
    headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    }
});

$('#edit-status').submit(function(e){
    e.preventDefault(e);
    $('.alert-status-form').html("");

    $.post('/status-management/{{$transaction->id}}', $(this).serialize(), function(data){
        window.location.reload();
    }).fail(function(data) {
        var response = JSON.parse(data.responseText);
        if (typeof(response.errors) != "undefined") {
            var errorString = '<div class="alert alert-danger alert-dismissable">'+
                '<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button><ul>';
            $.each(response.errors, function(key, val){
                errorString += '<li>' + val[0] + '</li>';
            });
            errorString += '</ul></div>';
            $('.alert-status-form').html(errorString);
            $("#editModal").animate({ scrollTop: 0 }, "slow");
        }
    });
});
</script>
@endsection