@extends('layouts.admin')

@section('content')
@include('components.navbar', ['active' => 'status-management'])

<div id="page-wrapper">
    @component('components.headerpage')
    {{__('Status management')}}
    @endcomponent
    
    <div class="row">
        <div class="col-md-9" style="padding-bottom:10px">
        </div>
        <div class="col-md-3" style="padding-bottom:10px">
            <form class="form-inline" action="" >
                <div class="form-group input-group">
                    <input type="text" name="keyword" class="form-control" placeholder="{{__('Search')}}" value="{{request()->keyword}}">
                    <span class="input-group-btn">
                        <button class="btn btn-primary" type="submit"><i class="fa fa-search"></i></button>
                    </span>
                </div>
            </form>                
        </div>
        <div class="col-md-12">
            @if(session()->has('status'))
                @component('components.alertsuccess')
                {{ session()->get('status') }}
                @endcomponent
            @endif
            <div class="panel panel-default">
                <div class="panel-heading dt-bootstrap">
                    {{__('List transaction')}}
                </div>
                <div class="panel-body">
                    <div class="row">
                        <div class="col-md-12 table-responsive">
                            <table id="users-table" class="table table-striped table-bordered table-hover dataTable">
                                <thead>
                                    <tr>
                                        <th>{{__('Order date')}} {!! \App\Libraries\SortHelper::header(request(), 'order_date') !!}</th>
                                        <th>{{__('Invoice number')}} {!! \App\Libraries\SortHelper::header(request(), 'code') !!}</th>
                                        <th>{{__('Nama')}} {!! \App\Libraries\SortHelper::header(request(), 'name') !!}</th>
                                        <th>{{__('Address')}} {!! \App\Libraries\SortHelper::header(request(), 'address') !!}</th>
                                        <th>{{__('Phone')}} {!! \App\Libraries\SortHelper::header(request(), 'phone') !!}</th>
                                        <th>{{__('Status')}} </th>
                                        @can('manage_status')
                                        <th>{{__('Action')}} </th>
                                        @endcan
                                    </tr>
                                </thead>
                                <tbody id="row-data-table">
                                    @if($transactions->count())
                                    @foreach($transactions as $trans)
                                    <tr>
                                        <td>{{$trans->order_date->format('d-m-Y')}}</td>
                                        <td>{{$trans->code or '-'}}</td>
                                        <td>{{$trans->name}}</td>
                                        <td>{{$trans->address}}</td>
                                        <td>{{$trans->phone}}</td>
                                        <td>  
                                            <span class="nowrap-t">{!! ($trans->order_status) ? '<i class="fas fa-check"></i>' : '<i class="fas fa-times"></i>&nbsp;' !!} {{__('Lens order')}}</span>
                                            <span class="nowrap-t">{!! ($trans->faset_status) ? '<i class="fas fa-check"></i>' : '<i class="fas fa-times"></i>&nbsp;' !!} {{__('Faset')}}</span>
                                            <span class="nowrap-t">{!! ($trans->packing_status) ? '<i class="fas fa-check"></i>' : '<i class="fas fa-times"></i>&nbsp;' !!} {{__('Packing')}}</span>
                                        </td>
                                        @can('manage_status')
                                        <th>
                                            <button class="btn btn-default edit-status" data-id="{{$trans->id}}">{{__('Edit')}}</button>
                                        </th>
                                        @endcan
                                    </tr>
                                    @endforeach
                                    @else
                                    <tr>
                                        <td colspan="14">{{__('Data not found')}}</td>
                                    </tr>
                                    @endif
                                </tbody>
                            </table>
                            {{ $transactions->links() }}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="editModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true" data-modalid="editModal" data-message="{{__('Are you sure?')}}">&times;</button>
                <h4 class="modal-title" id="editModalLabel">{{__('Edit status')}}</h4>
            </div>
            <div class="modal-body" id="status-edit">
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal" data-modalid="editModal" data-message="{{__('Are you sure?')}}">{{__('Close')}}</button>
            </div>
        </div>
    </div>
</div>
@endsection

@push('style')
@endpush
@push('scripts')
<script>
$.ajaxSetup({
    headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    }
});

$('.edit-status').click(function (e){
    e.preventDefault(e);
    var id = $(this).data('id')
    $.get('/status-management/'+id+'/edit/', function(data) {
        $('#status-edit').html(data);
        $('#editModal').modal('show');
    });
});
</script>
@include('scripts.closeconfirm')
@endpush
