@extends('layouts.admin')

@section('content')
@include('components.navbar', ['active' => 'omset/omset-report'])

<div id="page-wrapper">
    @component('components.headerpage')
        {{__('Laporan Omset')}}
    @endcomponent
    
    <form class="form-inline" action="" id="omset-report-form" >
    <div class="row">
        
            <div class="col-md-4">
                <div class="form-group input-group" style="padding-bottom:10px">
                    <span class="input-group-addon">{{__('Date')}}</span>
                    <input type="text" readonly="true" data-toggle="datepicker" name="start_date" class="form-control" value="{{ \Carbon\Carbon::createFromFormat('Y-m-d', $start)->format('d-m-Y')}}">
                </div>
                <div class="form-group input-group" style="padding-bottom:10px">
                    <span class="input-group-addon">{{__('To')}}</span>
                    <input type="text" readonly="true" data-toggle="datepicker" name="end_date" class="form-control" value="{{ \Carbon\Carbon::createFromFormat('Y-m-d', $end)->format('d-m-Y')}}">
                    
                </div>
                <div class="form-group input-group" style="padding-bottom:10px">
                    <span class="input-group-addon">{{__('Team')}}</span>
                    <select type="text" name="team_id" class="form-control">
                        <option value="0">-- {{__('All')}} --</option>
                        @foreach($teams as $teamid)
                        <option {{ (Request::get('team_id')== $teamid->id) ? 'selected' : '' }} value="{{$teamid->id}}">{{$teamid->name}}</option>
                        @endforeach
                    </select>
                    <span class="input-group-btn">
                            <button class="btn btn-primary" type="submit"><i class="fa fa-search"></i></button>
                    </span>
                </div>                                                      
            </div>
            <div class="col-md-2 text-right">
            </div>
            <div class="col-md-6 text-right">
                @foreach($teams as $team)
                <div class="form-group input-group" style="padding-bottom:10px">
                        <span class="input-group-addon">{{$team->name}}</span>
                        <p type="text" class="form-control" value="">{{ number_format($total[$team->name]-$minus[$team->name],2,',','.')}}</p>
                </div>
                @endforeach
            </div>
       
    </div>
    <div class="row">
        <div class=col-md-3>
                        
        </div>
        @can('manage_user')
        <div class="col-md-9 text-right" style="padding-bottom:10px">
                <form class="form-inline" action="" >
                    <div class="form-group">
                        <button class="btn btn-primary add-new-omset">Pengurangan Omset</button>
                    </div>
                </form>
        </div>
        @endcan
    </div>
    </form>
    <div class="row">
        <div class="col-md-12">
            @if(session()->has('status'))
                @component('components.alertsuccess')
                    {{ session()->get('status') }}
                @endcomponent
            @endif
            <div class="alert-download-form">
            </div>
            <div class="panel panel-default">
                <div class="panel-heading dt-bootstrap">
                    {{__('Laporan Omset')}}
                </div>
                <div class="panel-body">
                    <div class="row">
                        <div class="col-md-12 table-responsive">
                            <table id="users-table" class="table table-striped table-bordered table-hover dataTable">
                                <thead>
                                    <tr>
                                        {{-- <th>{{__('Date')}}</th> --}}
                                        <th>Tanggal</th>
                                        <th>No. Nota</th>
                                        <th>Nama</th>
                                        <th>Alamat</th>
                                        <th>{{__('Team')}}</th>
                                        <th>{{__('Plus')}}</th>
                                        <th>{{__('Minus')}}</th>
                                        <th>{{__('Memo')}}</th>
                                        @can('manage_user')
                                        <th>{{__('Action')}}</th>
                                        @endcan
                                    </tr>
                                </thead>
                                <tbody id="row-data-table">
                                    @if($data->count())
                                    @php($total=0)
                                    @foreach($data as $key)
                                    <tr>
                                        {{-- <td>{{$key->code}}</td> --}}
                                        <td>{{date('d-m-Y',strtotime($key->created_at))}}</td>
                                        <td>{{@$key->transaction->code}}</td>
                                        <td>{{@$key->transaction->name}}</td>
                                        <td>{{@$key->transaction->address}}</td>
                                        <td>{{$key->team->code}}</td>
                                        @if($key['transaction_id']!='0')
                                        <td class="text-right">Rp {{number_format($key->transaction['total_payment']-$key->transaction['rx_price'],2,',','.')}}</td>
                                        <td></td>
                                        @else
                                        <td></td>
                                        <td class="text-right">Rp {{number_format($key->minus,2,',','.')}}</td>
                                        @endif
                                        <td>{{$key->information}}</td>
                                        @can('manage_user')
                                        @if(@$key->transaction->code==null)
                                        <td>
                                            <form method="post" action="{{route('omset.deleteomset',$key['id'])}}">
                                                <button class="btn btn-default omset-edit" data-id="{{$key['id']}}">{{__('Edit')}}</button> 
                                                @csrf
                                                @method('DELETE')
                                                <button type="submit" class="btn btn-danger" onclick="return confirm('{{ __('Are you sure?') }}')">{{__('Delete')}}</button> 
                                            </form>
                                        </td>
                                        @else
                                        <td></td>
                                        @endif
                                        @endcan
                                    </tr>
                                    @endforeach
                                    @else 
                                    <tr class="data-not-found">
                                        <td colspan="9">{{__('Data not found')}}</td>
                                    </tr>
                                    @endif
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12 text-right">
                            @can('transaction_download')
                            <button class="btn btn-default" id="download-report">Download</button>
                            @endcan
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="createModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title" id="createModalLabel">Tambah Omset Minus</h4>
            </div>
            <div class="modal-body" id="form-create">
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">{{__('Close')}}</button>
            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="editModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title" id="editModalLabel">Edit Omset</h4>
            </div>
            <div class="modal-body" id="form-edit">
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">{{__('Close')}}</button>
            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="createModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title" id="createModalLabel">{{__('Add Income')}}</h4>
            </div>
            <div class="modal-body" id="form-create">
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">{{__('Close')}}</button>
            </div>
        </div>
    </div>
</div>
@endsection

@push('style')
<link href="{{ asset('vendor/datepicker/datepicker.min.css') }}" rel="stylesheet">
<script src="{{ asset('vendor/datepicker/datepicker.min.js') }}"></script>
@endpush

@push('scripts')
<script>
$.ajaxSetup({
    headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    }
});

$('.add-new-omset').click(function(e){
    e.preventDefault(e);
    $.get('/omset/omset-report/create',function(data){
        $('#form-create').html(data);
        $('#createModal').modal('show');
    });
});

$('#download-report').click(function(){
    var notfound = $('.data-not-found');
    if(notfound.length) {
        var errorString = '<div class="alert alert-danger alert-dismissable">'+
            '<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button><ul><li>{{__("Data not found")}}</li></ul></div>';
        $('.alert-download-form').html(errorString);
        return;
    }
    var formserialize = $('#omset-report-form').serialize();
    window.location.href = '/omset/omset-report-download?'+formserialize;
});

$('.omset-edit').click(function (e){
    e.preventDefault(e);
    var id = $(this).data('id');
    $.get('/omset/omset-report/edit/'+id+'', function(data) {
        $('#form-edit').html(data);
        $('#editModal').modal('show');
    });
    });
</script>
@include('scripts.datepicker')
@endpush