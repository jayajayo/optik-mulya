@extends('layouts.blank')
@section('content')
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.12.4/css/bootstrap-select.min.css">

@if(session()->has('status'))
                @component('components.alertsuccess')
                {{ session()->get('status') }}
                @endcomponent
            @endif
<div class="alert-omset-form">
</div>
<form class="form-horizontal" action="{{ route("omset.update-omset",['id' => $data->id]) }}" id="create-omset" role="form" method="POST">
    <input type="hidden" class="form-control" name="_method" value="POST" >
    @csrf

    <input type="hidden" name="trans_id" value="0">

                                <div class="row">
                                    <div class="col-md-8">
                                        <div class="form-group">
                                            <label class="control-label col-sm-2">{{__('Team')}}</label>
                                            <div class="col-sm-10">
                                                <select class="form-control" name="{{__('team')}}">
                                                    @foreach($team as $key)
                                                        @if ($data->team_id == $key->id)
                                                        <option data-tokens="{{$key->name}}" value="{{$key->id}}" selected>{{$key->code}}</option>
                                                        @else
                                                            <option data-tokens="{{$key->name}}" value="{{$key->id}}">{{$key->code}}</option>
                                                        @endif
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="control-label col-sm-2">Minus</label>
                                            <div class="col-sm-10">
                                            <input type="number" class="form-control money-mask" data-t="finish" name="minus" value="{{(int)$data->minus}}">
                                            </div>   
                                        </div>
                                        <div class="form-group">
                                            <label class="control-label col-sm-2">{{__('Memo')}}</label>
                                            <div class="col-sm-10">
                                                <textarea type="text" class="form-control" rows="3" name="{{__('memo')}}" >{{$data->memo}}</textarea>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <br>
                                    <div class="col-md-12 text-center">
                                        <button type="submit" class="btn btn-primary">Update</button>
                                        <a href="{{route('omset.omset-report')}}" class="btn btn-default">Kembali</a>
                                    </div>
                                </div>
                            </form>
@include('scripts.moneymask')
<script>
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
    
    $('#create-omset').submit(function(e){
        e.preventDefault(e);
        $('.alert-omset-form').html("");
        $.post({{ route('omset.inputomset') }}, $(this).serialize(), function(data){
            window.location.replace('/omset/omset-report');
        }).fail(function(data) {
            var response = JSON.parse(data.responseText);
            if (typeof(response.errors) != "undefined") {
                var errorString = '<div class="alert alert-danger alert-dismissable">'+
                    '<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button><ul>';
                $.each(response.errors, function(key, val){
                    errorString += '<li>' + val[0] + '</li>';
                });
                errorString += '</ul></div>';
                $('.alert-omset-form').html(errorString);
                $("#createModal").animate({ scrollTop: 0 }, "slow");
            }
        });
    });
</script>
@endsection