@extends('layouts.blank')
@section('content')
<div class="alert-omset-form">
</div>
<form class="form-horizontal" action="{{ route("omset.inputomset") }}" id="create-omset" role="form" method="POST">
    <input type="hidden" class="form-control" name="_method" value="POST" >
    @csrf
    {{-- <div class="form-group">
        <label class="control-label col-sm-2">{{__('Tanggal')}}</label>
        <div class="col-sm-10">
        <input class="form-control" disabled="disabled" value="1M" name="tanggal">
        </div>
    </div> --}}
    <input type="hidden" name="trans_id" value="0">
    <div class="form-group">
        <label class="control-label col-sm-2">{{__('Team')}}</label>
        <div class="col-sm-10">
            <select class="form-control" name="{{__('team')}}" title="-- pilih status customer --">
            @foreach ($teams as $team)
                <option value="{{$team->id}}">{{$team->code}}</option>
            @endforeach
            </select>
        </div>
    </div>
    <div class="form-group">
        <label class="control-label col-sm-2">Minus</label>
        <div class="col-sm-10">
            <input type="number" class="form-control money-mask" data-t="finish" name="minus" value="0">
            
        </div>   
    </div>
    <div class="form-group">
        <label class="control-label col-sm-2">{{__('Memo')}}</label>
        <div class="col-sm-10">
                <textarea class="form-control" rows="3" name="{{__('memo')}}" ></textarea>
        </div>
    </div>
    <div class="form-group">
        <label class="control-label col-sm-2">{{__('Action')}}</label>
        <div class="col-sm-10">
            <button type="submit" class="btn btn-primary">Submit</button>
            <button type="reset" class="btn btn-default">Reset</button>
        </div>
    </div>
</form>
@include('scripts.moneymask')
<script>
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
    
    $('#create-omset').submit(function(e){
        e.preventDefault(e);
        $('.alert-omset-form').html("");
        $.post({{ route('omset.inputomset') }}, $(this).serialize(), function(data){
            window.location.replace('/omset/omset-report');
        }).fail(function(data) {
            var response = JSON.parse(data.responseText);
            if (typeof(response.errors) != "undefined") {
                var errorString = '<div class="alert alert-danger alert-dismissable">'+
                    '<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button><ul>';
                $.each(response.errors, function(key, val){
                    errorString += '<li>' + val[0] + '</li>';
                });
                errorString += '</ul></div>';
                $('.alert-omset-form').html(errorString);
                $("#createModal").animate({ scrollTop: 0 }, "slow");
            }
        });
    });
</script>
@endsection