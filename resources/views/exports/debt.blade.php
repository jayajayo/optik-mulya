<table>
    <thead>
        <tr>
            <th colspan="10">
                @php(setlocale(LC_TIME, 'id_ID'))
                @php(\Carbon\Carbon::setLocale('id'))
                <h1><strong><b>{{__('Debt list')}}</b></strong> {{$start->formatLocalized("%A, %d %B %Y")}} - {{$end->formatLocalized("%A, %d %B %Y")}}</h1>
            </th>
        <tr>
    </thead>
</table>
<table id="users-table" class="table table-striped table-bordered table-hover dataTable">
    <thead>
        <tr>
            <th>{{__('Order date')}}</th>
            <th>{{__('Code')}}</th>
            <th>{{__('Name')}}</th>
            <th>{{__('Address')}}</th>
            <th>{{__('Total')}}</th>
            @foreach($payments as $payment)
            <th>{{ucfirst($payment)}}</th>
            @endforeach
            <th>{{__('Remaining')}}</th>
        </tr>
    </thead>
    <tbody id="row-data-table">
        @if($transactions->count())
        @foreach($transactions as $transaction)
        <tr>
            <td>{{$transaction->order_date->format('d-m-Y')}}</td>
            <td>{{$transaction->code}}</td>
            <td>{{$transaction->name}}</td>
            <td>{{$transaction->address}}</td>
            <td class="text-right"><span class="nowrap-t">Rp {{number_format($transaction->total_payment, 2, ',', '.')}}</span></td>
            @foreach($transaction->payments as $paylist)
            <td class="text-right"><span class="nowrap-t">{{($paylist->nomine != 0 && $paylist->status == 'accepted') ? 'Rp '.number_format($paylist->nomine, 2, ',', '.') : '-'}}</span></td>
            @endforeach
            <td class="text-right"><span class="nowrap-t">Rp {{number_format($transaction->total_payment - $transaction->payments()->where('status','accepted')->sum('nomine'), 2, ',', '.')}}</span></td>
        </tr>
        @endforeach
        @endif
    </tbody>
</table>
