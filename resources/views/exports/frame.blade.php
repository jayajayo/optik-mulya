<table>
    <thead>
        <tr>
            <th colspan="10">
                <h1>Daftar frame</h1>
            </th>
        <tr>
    </thead>
</table>
<table id="users-table" class="table table-striped table-bordered table-hover dataTable">
    <thead>
        <tr>
            <th>{{__('Merk')}}</th>
            <th>{{__('Code')}}</th>
            @foreach($teams as $team)
            <th>{{$team->name}}</th>
            @endforeach
            <th>Stock {{ __('Stock') }}</th>
            <th>Total {{ __('Total') }}</th>
        </tr>
    </thead>
    <tbody id="row-data-table">
        @if($frames->count())
        @foreach($frames as $frame)
        <tr>
            <td>{{$frame->merkFrame()->first()->name}}</td>
            <td>{{$frame->name}}</td>
            @foreach($teams as $team)
            <td>{{$frame->{$team->code} }}</td>
            @endforeach
            <td>{{$frame->stock}}</td>
            <td>{{$frame->total}}</td>
        </tr>
        @endforeach
        @endif
    </tbody>
</table>