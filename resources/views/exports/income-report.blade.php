<table>
    <thead>
        <tr>
            <th colspan="10">
                <h1><strong><b>Laporan Pemasukan</b></strong></h1>
            </th>
        <tr>
    </thead>
</table>
<table id="users-table" class="table table-striped table-bordered table-hover dataTable">
    <thead>
        <tr>
            {{-- <th>{{__('Date')}}</th> --}}
            <th>Tanggal</th>
            <th>{{__('Code')}}</th>
            <th>{{__('Keterangan')}}</th>
            <th>{{__('Nomine')}}</th>
            <th>{{__('Memo')}}</th>
        </tr>
    </thead>
    <tbody id="row-data-table">
        @if($sorted->count())
        @php($total=0)
        @foreach($sorted as $key)
        <tr>
            {{-- <td>{{$key->code}}</td> --}}
            <td>{{date('d-m-Y',strtotime($key['payment_date']))}}</td>
            <td>{{!empty($key['code']) ? $key['code'] : 'Uang Masuk Harian'}}</td>
            <td>{{@$key['information']}}</td>
            @php($total+=@$key['nomine'])
            <td class="text-right">Rp {{number_format(@$key['nomine'],2,',','.')}}</td>
            <td>{{@$key['memo']}}</td>
            @if(@$key['code']=='Lainnya')
            @else
            <td></td>
            @endif
        </tr>
        @endforeach
        <tr>
            <td colspan="2"></td>
            <td><b>{{__('Total')}}</b></td>
            <td class="text-right">Rp {{number_format($total,2,'.','.')}}</td>
            <td colspan="2"></td>
        </tr>
        @else
        <tr>
            <td colspan="6">{{__('Data not found')}}</td>
        </tr>
        @endif
    </tbody>
</table>