<table>
    <thead>
        <tr>
            <th colspan="10">
                @php(setlocale(LC_TIME, 'id_ID'))
                @php(\Carbon\Carbon::setLocale('id'))
                <h1><strong><b>{{ __('Report') }} {{ __('Transaction') }}</b></strong> {{$start->formatLocalized("%A, %d %B %Y")}} - {{$end->formatLocalized("%A, %d %B %Y")}}</h1>
            </th>
        <tr>
    </thead>
</table>
<table>
    <thead>
        <tr>
            <th>{{__('Order date')}}</th>
            <th>{{__('Charge date')}}</th>
            <th>{{__('Invoice number')}}</th>
            <th>{{__('Nama')}}</th>
            <th>{{__('Address')}}</th>
            <th>{{__('Phone')}}</th>
            <th>{{__('Frame')}}</th>
            <th>{{__('Lens')}}</th>
            <th>{{__('Rx')}}</th>
            <th>{{__('SPH')}}</th>
            <th>{{__('Cyl')}}</th>
            <th>{{__('Axis')}}</th>
            <th>Add</th>
            <th>{{__('Paydeal')}}</th>
            <th>{{__('Total price')}}</th>
            <th>{{__('Memo')}}</th>
            <th>{{__('Information')}}</th>
            <th>{{__('status')}}</th>
        </tr>
    </thead>
    <tbody id="row-data-table">
        @if($transactions->count())
        @foreach($transactions as $trans)
        <tr>
            <td>{{$trans->order_date->format('d-m-Y')}}</td>
            <td>{{$trans->charge_date->format('d-m-Y')}}</td>
            <td>{{$trans->code or '-'}}</td>
            <td>{{$trans->name}}</td>
            <td>{{$trans->address}}</td>
            <td>{{$trans->phone}}</td>
            <td>
                {{$trans->frame or ''}}
                {{isset($trans->frame()->first()->merkframe->name) ? '('.$trans->frame()->first()->merkframe->name.')' : '-'}}
            </td>
            <td>{{$trans->lens or '-'}}</td>
            <td><span class="nowrap-t">{{!empty($trans->rx_price) ? 'Rp '.number_format($trans->rx_price,2,',','.') : '-'}}</span></td>
            <td>
                <span class="nowrap-t">{{empty($trans->sph_r) ? '-' : 'R: '.$trans->sph_r}} </span>
                <span class="nowrap-t">{{empty($trans->sph_l) ? '-' : 'L: '.$trans->sph_l}} </span>
            </td>
            <td>
                <span class="nowrap-t">{{empty($trans->cyl_r) ? '-' : 'R: '.$trans->cyl_r}} </span>
                <span class="nowrap-t">{{empty($trans->cyl_l) ? '-' : 'L: '.$trans->cyl_l}}  </span>
            </td>
            <td>
                <span class="nowrap-t">{{empty($trans->axis_r) ? '-' : 'R: '.$trans->axis_r}} </span> 
                <span class="nowrap-t">{{empty($trans->axis_l) ? '-' : 'L: '.$trans->axis_l}} </span>
            </td>
            <td>
                <span class="nowrap-t">{{empty($trans->add_r) ? '-' : 'R: '.$trans->add_r}} </span>
                <span class="nowrap-t">{{empty($trans->add_l) ? '-' : 'L: '.$trans->add_l}} </span> 
            </td>
            <td>{{$trans->pd}}</td>
            <td><span class="nowrap-t">{{!empty($trans->total_payment) ? 'Rp '.number_format($trans->total_payment,2,',','.') : '-'}}</span></td>
            <td>{{$trans->memo or '-'}}</td>
            <td>{{ __($trans->paid_off) }}</td>
            <td>{{ __($trans->status) }}</td>
        </tr>
        @endforeach
        @else
        <tr>
            <td colspan="14">{{__('Data not found')}}</td>
        </tr>
        @endif
    </tbody>
</table>