<table>
    <thead>
        <tr>
            <th colspan="10">
                <h1><strong><b>Laporan Omset</b></strong></h1>
            </th>
        <tr>
    </thead>
</table>
<table id="users-table" class="table table-striped table-bordered table-hover dataTable">
    <thead>
        <tr>
            {{-- <th>{{__('Date')}}</th> --}}
            <th>Tanggal</th>
            <th>No. Nota</th>
            <th>Nama</th>
            <th>Alamat</th>
            <th>{{__('Team')}}</th>
            <th>{{__('Plus')}}</th>
            <th>{{__('Minus')}}</th>
            <th>{{__('Memo')}}</th>
        </tr>
    </thead>
    <tbody id="row-data-table">
        @if($omset->count())
        @php($total=0)
        @foreach($omset as $key)
        <tr>
            {{-- <td>{{$key->code}}</td> --}}
            <td>{{date('d-m-Y',strtotime($key->created_at))}}</td>
            <td>{{@$key->transaction->code}}</td>
            <td>{{@$key->transaction->name}}</td>
            <td>{{@$key->transaction->address}}</td>
            <td>{{$key->team->code}}</td>
            @if($key['transaction_id']!='0')
            <td class="text-right">Rp {{number_format($key->transaction['total_payment']-$key->transaction['rx_price'],2,',','.')}}</td>
            <td></td>
            @else
            <td></td>
            <td class="text-right">Rp {{number_format($key->minus,2,',','.')}}</td>
            @endif
            <td>{{$key->information}}</td>
            </tr>
        @endforeach
        @else 
        <tr class="data-not-found">
            <td colspan="9">{{__('Data not found')}}</td>
        </tr>
        @endif
    </tbody>
</table>