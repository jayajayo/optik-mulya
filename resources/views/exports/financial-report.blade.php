<table>
    <thead>
        <tr>
            <th colspan="10">
                <h1><strong><b>Laporan Keuangan</b></strong></h1>
            </th>
        <tr>
    </thead>
</table>
<table id="users-table" class="table table-striped table-bordered table-hover dataTable">
    <thead>
        <tr>
            {{-- <th>{{__('Date')}}</th> --}}
            <th>Tanggal</th>
            <th>Keterangan</th>
            <th>{{__('Code')}}</th>
            <th>{{__('Masuk')}}</th>
            <th>{{__('Keluar')}}</th>
            <th>{{__('Memo')}}</th>
            {{-- <th>{{__('Action')}}</th> --}}
        </tr>
    </thead>
    <tbody id="row-data-table">
        @if($sorted->count())
        @php($total=0)
        @php($out=0)
        @foreach($sorted as $key)
        <tr>
            {{-- <td>{{$key->code}}</td> --}}
            <td>{{date('d-m-Y',strtotime($key['payment_date']))}}</td>
            <td>{{!empty($key['code']) ? $key['code'] : 'Uang Masuk Harian'}}</td>
            <td>{{@$key['information']}}</td>
            @if(@$key['jenis']=='income')
            @php($total+=@$key['nomine'])
            <td class="text-right">Rp {{number_format(@$key['nomine'],2,',','.')}}</td>
            <td></td>
            @else
            <td></td>
            @php($out+=$key['nomine'])
            <td class="text-right">Rp {{number_format(@$key['nomine'],2,',','.')}}</td>
            @endif
            <td>{{@$key['memo']}}</td>
        </tr>
        @endforeach
        <tr>
            <td colspan="2"></td>
            <td><b>{{__('Total')}}</b></td>
            <td class="text-right">Rp {{number_format($total,2,'.','.')}}</td>
            <td class="text-right">Rp {{number_format($out,2,'.','.')}}</td>
            <td colspan="2"></td>
        </tr>
        @else
        <tr class="data-not-found">
            <td colspan="6">{{__('Data not found')}}</td>
        </tr>
        @endif 
    </tbody>
</table>