<table>
    <thead>
        <tr>
            <th colspan="10">
                @php(setlocale(LC_TIME, 'id_ID'))
                @php(\Carbon\Carbon::setLocale('id'))
                <h1><strong><b>{{ __('Report') }} {{ __('Daily payment') }}</b></strong> {{$date->formatLocalized("%A, %d %B %Y")}}</h1>
            </th>
        <tr>
    </thead>
</table>
<table id="users-table" class="table table-striped table-bordered table-hover dataTable">
    <thead>
        <tr>
            <th>{{__('Invoice number')}}</th>
            <th>Nama</th>
            <th>Alamat</th>
            <th>{{__('Code')}}</th>
            <th>{{__('Nomine')}}</th>
        </tr>
    </thead>
    <tbody id="row-data-table">
        @if($payments->count())
        @php($total=0)
        @foreach($payments as $payment)
        <tr>
            <td>{{$payment->transaction->code}}</td>
            <td>{{$payment->transaction->name}}</td>
            <td>{{$payment->transaction->address}}</td>
            <td>{{$payment->name}}</td>
            @php($total+=$payment->nomine)
            <td>{{number_format($payment->nomine,2,',','.')}}</td>
        </tr>
        @endforeach
        <tr>
            <td colspan="4" class="text-right"><b>{{__('Total')}}</b></td>
            <td>{{number_format($total,2,',','.')}}</td>
        </tr>
        @endif
    </tbody>
</table>