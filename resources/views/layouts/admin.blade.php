<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="viewport" content="width=device-width, initial-scale=1" />
    <meta name="csrf-token" content="{{ csrf_token() }}" />
    <title>{{ config('app.name', 'Laravel') }}</title>
    <link rel="dns-prefetch" href="https://fonts.gstatic.com" />
    <link href="{{asset('/vendor/bootstrap/css/bootstrap.min.css')}}" rel="stylesheet" />
    <link href="{{asset('/vendor/metisMenu/metisMenu.min.css')}}" rel="stylesheet" />
    <link href="{{asset('/dist/css/sb-admin-2.css')}}" rel="stylesheet" />
    <link href="{{asset('/vendor/font-awesome5/css/fontawesome-all.min.css')}}" rel="stylesheet" type="text/css" />
    <link href="{{asset('/css/main.css')}}" rel="stylesheet" type="text/css" />
    {{-- <link href="{{asset('/vendor/bootstrap/css/jquery.dataTables.min.css')}}" rel="stylesheet" type="text/css" /> --}}
    <link href="{{asset('/vendor/bootstrap/css/fixedColumns.dataTables.min.css')}}" rel="stylesheet" type="text/css" />
    <link rel="stylesheet" href="https://cdn.datatables.net/1.10.19/css/jquery.dataTables.min.css">
    <link rel="stylesheet" href="https://cdn.datatables.net/buttons/1.5.6/css/buttons.dataTables.min.css">
    <link rel="stylesheet" href="https://cdn.datatables.net/responsive/2.2.3/css/responsive.dataTables.min.css">
    <script src="{{asset('/vendor/moment/min/moment-with-locales.min.js')}}"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.0/jquery.min.js"></script>
    {{-- <script src="{{asset('/vendor/jquery/jquery.min.js')}}"></script> --}}
    <script src="{{asset('/vendor/bootstrap/js/bootstrap.min.js')}}"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.12.4/js/bootstrap-select.min.js"></script>
    <script src="{{asset('/vendor/metisMenu/metisMenu.min.js')}}"></script>
    <script src="{{asset('/dist/js/sb-admin-2.js')}}"></script>
    <script src="{{asset('/js/main.js')}}"></script>
    {{-- <script src="{{asset('/vendor/jquery/jquery.dataTables.min.js')}}"></script> --}}
    <script src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
    <script src="{{asset('/vendor/jquery/dataTables.fixedColumns.min.js
    ')}}"></script>
    <script src="https://cdn.datatables.net/buttons/1.5.6/js/dataTables.buttons.min.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.5.6/js/buttons.flash.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/pdfmake.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/vfs_fonts.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.5.6/js/buttons.html5.min.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.5.6/js/buttons.print.min.js"></script>
    <script src="https://cdn.datatables.net/responsive/2.2.3/js/dataTables.responsive.min.js"></script>
    @stack('style')
</head>
<body>
    <div id="wrapper">
        @yield('content')
    </div>
    @stack('scripts')
</body>
</html>
