@extends('layouts.blank')
@section('content')
<div class="alert-frame-form">
</div>
<form class="form-horizontal" id="create-frame" role="form">
    <div class="form-group">
        <label class="control-label col-sm-2">{{__('Merk')}}</label>
        <div class="col-sm-10">
            <select class="form-control" name="{{__('merk')}}" >
                @foreach($merkframes as $merkframe)
                    <option value="{{$merkframe->id}}">{{ucfirst($merkframe->name)}}</option>
                @endforeach
            </select>
        </div>
    </div>
    <div class="form-group">
        <label class="control-label col-sm-2">{{__('Code')}}</label>
        <div class="col-sm-10">
            <input type="text" class="form-control" name="{{__('code_frame')}}" >
        </div>
    </div>
    @foreach($teams as $team)
    <div class="form-group">
        <label class="control-label col-sm-2">{{$team->name}}</label>
        <div class="col-sm-10">
            <input type="number" class="form-control" name="team[{{$team->id}}]" min="0" value="0">
        </div>
    </div>
    @endforeach
    <div class="form-group">
        <label class="control-label col-sm-2">{{__('Stock')}}</label>
        <div class="col-sm-10">
            <input type="number" class="form-control" name="stock" min="0" value="0">
        </div>
    </div>
    <div class="form-group">
        <label class="control-label col-sm-2">{{__('Action')}}</label>
        <div class="col-sm-10">
            <button type="submit" class="btn btn-primary">Submit</button>
            <button type="reset" class="btn btn-default">Reset</button>
        </div>
    </div>
</form>
<script>
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
    
    $('#create-frame').submit(function(e){
        e.preventDefault(e);
        $('.alert-frame-form').html("");

        $.post('/frame',$(this).serialize(), function(data){
            window.location.replace('/frame');
        }).fail(function(data) {
            var response = JSON.parse(data.responseText);
            if (typeof(response.errors) != "undefined") {
                var errorString = '<div class="alert alert-danger alert-dismissable">'+
                    '<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button><ul>';
                $.each(response.errors, function(key, val){
                    errorString += '<li>' + val[0] + '</li>';
                });
                errorString += '</ul></div>';
                $('.alert-frame-form').html(errorString);
                $("#createModal").animate({ scrollTop: 0 }, "slow");
            }
        });
    });
</script>
@endsection