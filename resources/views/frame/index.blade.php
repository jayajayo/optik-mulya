@extends('layouts.admin')

@section('content')
    @include('components.navbar', ['active' => 'frame'])

    <div id="page-wrapper">
        @component('components.headerpage')
            {{__('Frame management')}}
        @endcomponent
        
        <div class="row">
            <div class="col-md-9" style="padding-bottom:10px">
                <button class="btn btn-primary" id="new-frame">{{__('Add frame')}}</button>
                <button class="btn btn-primary" id="manage-merk-frame">{{__('Manage merk frame')}}</button>
            </div>
            <div class="col-md-3" style="padding-bottom:10px">
                <form class="form-inline" action="" >
                    <div class="form-group input-group">
                    <input type="text" name="keyword" class="form-control" placeholder="{{__('Search')}}" value="{{$keyword}}">
                        <span class="input-group-btn">
                            <button class="btn btn-primary" type="submit"><i class="fa fa-search"></i></button>
                        </span>
                    </div>
                </form>                
            </div>
            <div class="col-md-12">
                @if(session()->has('status'))
                    @component('components.alertsuccess')
                        {{ session()->get('status') }}
                    @endcomponent
                @endif
                <div class="panel panel-default">
                    <div class="panel-heading dt-bootstrap">
                        {{__('List frame')}}
                    </div>
                    <div class="panel-body">
                        <div class="row">
                            <div class="col-md-12 table-responsive">
                                <table id="users-table" class="table table-striped table-bordered table-hover dataTable">
                                    <thead>
                                        <tr>
                                            <th class="table-sort">Merk {!! \App\Libraries\SortHelper::header(request(), 'merk') !!}</th>
                                            <th class="table-sort">Kode {!! \App\Libraries\SortHelper::header(request(), 'name') !!}</th>
                                            @foreach($teams as $team)
                                            <th class="table-sort">{{$team->name}} {!! \App\Libraries\SortHelper::header(request(), strtolower($team->name)) !!}</th>
                                            @endforeach
                                            <th class="table-sort">Stock {!! \App\Libraries\SortHelper::header(request(), 'stock') !!}</th>
                                            <th class="table-sort">Total {!! \App\Libraries\SortHelper::header(request(), 'total') !!}</th>
                                            <th class="table-sort">Action</th>
                                        </tr>
                                    </thead>
                                    <tbody id="row-data-table">
                                        @if($frames->count())
                                            @foreach($frames as $frame)
                                                <tr>
                                                    <td>{{$frame->merkFrame()->first()->name}}</td>
                                                    <td>{{$frame->name}}</td>
                                                    @foreach($teams as $team)
                                                    <td>{{$frame->{$team->code} }}</td>
                                                    @endforeach
                                                    <td>{{$frame->stock}}</td>
                                                    <td>{{$frame->total}}</td>
                                                    <td>
                                                        <button class="btn btn-default frame-edit" data-id="{{$frame->id}}">{{__('Edit')}}</button> 
                                                        @can('manage_frame')
                                                        <form method="post" action="{{url('/frame/'.$frame->id)}}">
                                                            @csrf
                                                            @method('DELETE')
                                                            <button type="submit" class="btn btn-danger frame-delete" data-id="{{$frame->id}}" onclick="return confirm('{{ __('Are you sure?') }}')">{{__('Delete')}}</button> 
                                                        </form>
                                                        @endcan
                                                    </td>
                                                </tr>
                                            @endforeach
                                        @else
                                            <tr>
                                                <td colspan="10">{{__('Data not found')}}</td>
                                            </tr>
                                        @endif
                                    </tbody>
                                </table>
                                {{ $frames->links() }}
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12 text-right">
                                <a class="btn btn-default" href="{{route('frame.download')}}">Download</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="modal fade" id="createModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title" id="createModalLabel">{{__('New Frame')}}</h4>
                </div>
                <div class="modal-body" id="form-create">
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">{{__('Close')}}</button>
                </div>
            </div>
        </div>
    </div>
    <div class="modal fade" id="editModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title" id="editModalLabel">{{__('Edit frame')}}</h4>
                </div>
                <div class="modal-body" id="form-edit">
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">{{__('Close')}}</button>
                </div>
            </div>
        </div>
    </div>
    <div class="modal fade" id="deleteModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title" id="deleteModalLabel">{{__('Delete user')}}</h4>
                </div>
                <div class="modal-body" id="">
                    <div class="alert alert-warning">
                        <span class="alert-link">{{__("Warning")}}!</span> {{__("We not recomended to delete user. Deleting user may caused reporting issue. The same username still can't be used even you have deleted the user.")}}</p>
                    </div>
                </div>
                <div class="modal-footer">
                    <form id="delete-user" method="post" action="">
                        @csrf
                        <input type="hidden" name="_method" value="DELETE">
                        <input class="btn btn-danger" type="submit" value="{{__('Delete')}}" >
                        <button type="button" class="btn btn-default" data-dismiss="modal">{{__('Cancel')}}</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <div class="modal fade" id="manageMerkModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title" id="manageMerkModalLabel">{{__('Manage merk frame')}}</h4>
                </div>
                <div class="modal-body" id="form-manage-merk-frame">
                </div>
                <div class="modal-footer">
                    <form id="delete-user" method="post" action="">
                        @csrf
                        <input type="hidden" name="_method" value="DELETE">
                        <button type="button" class="btn btn-default" data-dismiss="modal">{{__('Close')}}</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection

@push('style')
@endpush
@push('scripts')
<script>
$.ajaxSetup({
    headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    }
});

$('#new-frame').click(function (e){
    e.preventDefault(e);
    $.get('/frame/create', function(data) {
        $('#form-create').html(data);
        $('#createModal').modal('show');
    });
});

$('.frame-edit').click(function (e){
    e.preventDefault(e);
    var id = $(this).data('id');
    $.get('/frame/'+id+'/edit', function(data) {
        $('#form-edit').html(data);
        $('#editModal').modal('show');
    });
    });

$('#go-manage-merk-frame, #manage-merk-frame').click(function (e){
    e.preventDefault(e);
    $.get('/merk-frame', function(data) {
        $('#form-manage-merk-frame').html(data);
        $('#manageMerkModal').modal('show');
    });
});

$('#manageMerkModal').on('hidden.bs.modal', function () {
    location.reload();
});
</script>
@endpush
