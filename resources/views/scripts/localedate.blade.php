<script>
$(document).ready(function(){
    el = $('.localize-date');
    if (!el.length) return;
    moment.locale('id');
    var now = moment();
    el.html(now.format("dddd, DD-MM-YYYY"));
});
</script>