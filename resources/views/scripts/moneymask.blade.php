<script>
    $('.money-mask').on('input change',function(e){
        var t = $(this).data('t');
        updatemask(t, $(this).val());
    });
    function updatemask(t, val) {
        var mask = (val == 0 || isNaN(val)) ? 0 : Number(val).toLocaleString('id');
        $('.target-mask[data-t='+t+']').html( mask );
    }
</script>