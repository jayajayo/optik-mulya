@extends('layouts.blank')
@section('content')
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.12.4/css/bootstrap-select.min.css">
</div>
<form class="form-horizontal" id="create-payment" role="form">
    <div class="form-group">
        <label class="control-label col-sm-2">{{__('Invoice number')}}</label>
        <div class="col-sm-10">
            <select title="-- {{__('select')}} --" class="form-control  selectpicker select-transaction" name="{{__('transaction')}}" data-live-search="true">
                @foreach($transactions as $transaction)
                <option  data-tokens="{{$transaction->code}}" value="{{$transaction->id}}">{{$transaction->code}}</option>
                @endforeach
            </select>
        </div>
    </div>
    <div class="form-group">
        <label class="control-label col-sm-2" required="required">{{__('Code')}}</label>
        <div class="col-sm-10">
            <select class="form-control select-code" name="{{__('code')}}" required>
                <option disabled selected value>-- {{__('select')}} --</option>
            </select>
        </div>
    </div>
    <div class="form-group">
        <label class="control-label col-sm-2">{{__('Name')}}</label>
        <div class="col-sm-10" >
            <input name="{{__('name')}}" id="name" disabled="disabled" class="form-control">
        </div>
    </div>
    <div class="form-group">
        <label class="control-label col-sm-2">{{__('Address')}}</label>
        <div class="col-sm-10">
            <input name="{{__('address')}}" id="address" disabled="disabled" class="form-control">
        </div>
    </div>
    <div class="form-group">
        <label class="control-label col-sm-2">{{__('Nomine')}}</label>
        <div class="col-sm-10">
            <input type="number" name="{{__('nomine')}}" class="form-control" min="0" step="100" value="0">
        </div>
    </div>
    <div class="form-group">
            <label class="control-label col-sm-2">{{__('Information')}}</label>
            <div class="col-sm-10">
                <select class="form-control" name="{{__('information')}}" required>
                    @foreach($types as $type)
                    <option value="{{$type->code}}">{{$type->code}}</option>
                    @endforeach
                </select>
            </div>
        </div>
    <div class="form-group">
            <label class="control-label col-sm-2">{{__('Memo')}}</label>
            <div class="col-sm-10">
                <textarea type="text" class="form-control" name="{{__('memo')}}" ></textarea>
                </div>
    </div>
    <div class="form-group">
        <label class="control-label col-sm-2">{{__('Action')}}</label>
        <div class="col-sm-10">
            <button type="submit" class="btn btn-primary">{{__('Deposit')}}</button>
            <button type="reset" class="btn btn-default">Reset</button>
        </div>
    </div>
</form>
<script>
$.ajaxSetup({
    headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    }
});

$('.select-transaction').change(function(e){
    $('.select-code').html('<option  disabled selected value>-- {{__('select')}} --</option>');
    $.post('/deposit/get-payment/'+$(this).val(), function(data){
        $.each(data, function(k,v) {
            $('.select-code').append('<option value="'+v.id+'">'+v.name+'</option>')
        })
    });
    
});

$('.select-transaction').change(function(e){
    $('#select-name').html('<input name="{{__('name')}}" disabled="disabled" class="form-control">');
    $.post('/deposit/get-transaction/'+$(this).val(), function(data){
        $('#name').val(data.name);
    });
});

$('.select-transaction').change(function(e){
    $('#select-name').html('<input name="{{__('name')}}" disabled="disabled" class="form-control">');
    $.post('/deposit/get-transaction/'+$(this).val(), function(data){
        $('#address').val(data.address);
    });
});

$('#create-payment').submit(function(e){
    e.preventDefault(e);
    $('.alert-frame-form').html("");
    $.post('/deposit/add-payment', $(this).serialize(), function(data){
        window.location.replace('/deposit/payment-list');
    }).fail(function(data) {
        var response = JSON.parse(data.responseText);
        if (typeof(response.errors) != "undefined") {
            var errorString = '<div class="alert alert-danger alert-dismissable">'+
                '<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button><ul>';
            $.each(response.errors, function(key, val){
                errorString += '<li>' + val[0] + '</li>';
            });
            errorString += '</ul></div>';
            $('.alert-payment-form').html(errorString);
            $("#createModal").animate({ scrollTop: 0 }, "slow");
        }
    });
});
$('.selectpicker').selectpicker();
</script>
@endsection