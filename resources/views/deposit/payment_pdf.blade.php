@extends('layouts.admin')

@section('content')
{{-- @include('components.navbar', ['active' => 'deposit/payment-list']) --}}

<div id="page-wrapper">
    @component('components.headerpage')
    <img src="{{asset('logo.jpeg')}}" class="show-print" alt="{{ config('app.name', 'Laravel') }}" height="35px"> <span class="dont-print">{{__('Deposit')}}</span>
    @endcomponent
    
    <div class="row">
        <div class="col-md-9" style="padding-bottom:10px">
        <h4>{{__('Date')}} : <span class="localize-date"></span></h4>
        </div>
        <div class="col-md-12">
            @if(session()->has('status'))
                @component('components.alertsuccess')
                {{ session()->get('status') }}
                @endcomponent
            @endif
            <div class="alert-payment-form">
            </div>
            <div class="panel panel-default">
                <div class="panel-heading dt-bootstrap">
                    {{__('Deposit list')}}
                </div>
                <div class="panel-body">
                    <div class="row">
                        <div class="col-md-12 table-responsive">
                            <table id="users-table" class="table table-striped table-bordered table-hover dataTable">
                                <thead>
                                    <tr>
                                        <th>{{__('Invoice number')}}</th>
                                        <th>{{__('Name')}}</th>
                                        <th>{{__('Nomine')}}</th>
                                        <th>{{__('Code')}}</th>
                                        <th>{{__('Information')}}</th>
                                        <th>{{__('Address')}}</th>
                                        <th>{{__('Tempo')}}</th>
                                    </tr>
                                </thead>
                                <tbody id="row-data-table">
                                    @if($payments->count())
                                    @php($total=0)
                                    @foreach($payments as $pay)
                                    <tr>
                                        <td>{{$pay->transaction->code}}</td>
                                        <td>{{$pay->transaction->name}}</td>
                                        @php($total+=$pay->nomine)
                                        <td class="text-right">Rp {{number_format($pay->nomine,2,',','.')}}</td>
                                        <td>{{$pay->name}}</td>
                                        <td>{{$pay->information or '-'}}</td>
                                        <td>{{$pay->transaction->address}}</td>
                                        <td>{{$pay->transaction->tempo}}</td>
                                    </tr>
                                    @endforeach
                                    <tr>
                                        <td></td>
                                        <td><b>{{__('Total')}}</b></td>
                                        <td class="text-right"><b>Rp {{number_format($total,2,'.','.')}}</b></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                    </tr>
                                    @else
                                    <tr>
                                        <td>{{__('Data not found')}}</td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                    </tr>
                                    @endif
                                </tbody>
                            </table>


                        </div>
                    </div>
                    <div class="row dont-print">
                        <div class="col-md-12 text-right" style="padding-top:2rem">
                            <button class="btn btn-primary add-new-payment">{{__('Add')}}</button>
                            <a href="/deposit/payment-list/cetak_pdf" target="_blank" class="btn btn-default">Print</a>
                            {{-- <button class="btn btn-default do-print" onclick="window.print()">{{__('Print')}}</button> --}}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@push('style')
@endpush
@push('scripts')
@include('scripts.localedate')
<script>
$.ajaxSetup({
    headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    }
});

$('.add-new-payment').click(function(e){
    e.preventDefault(e);
    $.post('/deposit/payment-form',function(data){
        $('#form-create').html(data);
        $('#createModal').modal('show');
    });
})

$('.delete-payment').click(function(e){
    e.preventDefault(e);
    if (!confirm("{{__('Are you sure?')}}")) return;

    var id = $(this).data('id');
    $.post('/deposit/delete-payment/'+id,{"_method":"DELETE" }, function(){
        location.reload();
    }).fail(function(data) {
        var response = JSON.parse(data.responseText);
        if (typeof(response.errors) != "undefined") {
            var errorString = '<div class="alert alert-danger alert-dismissable">'+
                '<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button><ul>';
            $.each(response.errors, function(key, val){
                errorString += '<li>' + val[0] + '</li>';
            });
            errorString += '</ul></div>';
            $('.alert-frame-form').html(errorString);
            $("#createModal").animate({ scrollTop: 0 }, "slow");
        }
    });
});
$(document).ready(function() {
    var table = $('#users-table').DataTable( {
        scrollCollapse: true,
        paging:         false,
        ordering:       false,
        searching:      false,
        pageLength:     50,
        info:           false,
    } );
    window.print()
});
</script>
@endpush
