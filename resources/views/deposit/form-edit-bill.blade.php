@extends('layouts.blank')
@section('content')
    <td>
        {{ $payment->transaction->code }}
    </td>
    <td>
        {{ $payment->transaction->name }}
    </td>
    <td>
        {{ $payment->transaction->address }}
    </td>
    <td>
        {{ $payment->transaction->tempo }}
    </td>
    <td>
        {{ $payment->name }}
    </td>
    <td>
        <div class="form-group editor-open">
            <input class="form-control" type="number" min="0" step="100" name="{{__('nomine')}}" value="{{$payment->nomine}}">
        </div>
    </td>
    <td>
        <div class="form-group editor-open">
            <select class="form-control" name="{{__('information')}}" required="required">
                <option disabled selected value>-- pilih --</option>
                @foreach($types as $type)
                <option {{$payment->information == $type->id ? 'selected' : ''}} value="{{$type->code}}">{{$type->code}}</option>
                @endforeach
            </select>
        </div>
    </td>
    <td>
        <button class="btn btn-primary save-edit-bill">Save</button>
        <button class="btn btn-default cancel-edit">Cancel</button>
    </td>
<script>
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
    $('.save-edit-bill').click(function(e){
        e.preventDefault(e);
        $('.alert-bill-form').html("");
        $.post('/deposit/update-bill/{{$payment->id}}', {"_method":"PUT","nominal":$('input[name=nominal]').val(), "information":$('select[name=information]').val()}, function(data) {
            location.reload();
        }).fail(function(data) {
            var response = JSON.parse(data.responseText);
            if (typeof(response.errors) != "undefined") {
                var errorString = '<div class="alert alert-danger alert-dismissable">'+
                    '<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button><ul>';
                $.each(response.errors, function(key, val){
                    errorString += '<li>' + val[0] + '</li>';
                });
                errorString += '</ul></div>';
                $('.alert-bill-form').html(errorString);
                $("#createModal").animate({ scrollTop: 0 }, "slow");
            }
        });
    })
    $('.cancel-edit').click(function(e){
        e.preventDefault(e);location.reload();
    })
</script>
@endsection