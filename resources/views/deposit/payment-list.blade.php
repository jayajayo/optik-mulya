@extends('layouts.admin')

@section('content')
@include('components.navbar', ['active' => 'deposit/payment-list'])

<div id="page-wrapper">
    @component('components.headerpage')
    <img src="{{asset('logo.jpeg')}}" class="show-print" alt="{{ config('app.name', 'Laravel') }}" height="35px"> <span class="dont-print">{{__('Deposit')}}</span>
    @endcomponent
    
    <div class="row">
        <div class="col-md-9" style="padding-bottom:10px">
        <h4>{{__('Date')}} : <span class="localize-date"></span></h4>
        </div>
        <div class="col-md-9" style="padding-bottom:10px">
            <div class="form-group input-group" style="padding-bottom:10px">
                    <span class="input-group-addon">Total Tunai</span>
            <p type="text" class="form-control" disabled="disable" value="">Rp {{number_format($tunai,2,',','.')}}</p>
            </div>
            <div class="form-group input-group" style="padding-bottom:10px">
                <span class="input-group-addon">Total Transfer</span>
            <p type="text" class="form-control" disabled="disable" value="">Rp {{number_format($transfer,2,',','.')}}</p>
            </div>
            @can('add_income')
            <div class="form-group input-group" style="padding-bottom:10px">
                <button class="btn btn-primary add-new-income">Tambah Data Pemasukan</button>
        </div>
        @endcan
        </div>
        <div class="col-md-3 dont-print" style="padding-bottom:10px">
            <form class="form-inline" action="" >
                <div class="form-group input-group">
                <input type="text" name="keyword" class="form-control" placeholder="{{__('Search')}}" value="{{$keyword}}">
                    <span class="input-group-btn">
                        <button class="btn btn-primary" type="submit"><i class="fa fa-search"></i></button>
                    </span>
                </div>
            </form>                
        </div>
        <div class="col-md-12">
            @if(session()->has('status'))
                @component('components.alertsuccess')
                {{ session()->get('status') }}
                @endcomponent
            @endif
            <div class="alert-payment-form">
            </div>
            <div class="panel panel-default">
                <div class="panel-heading dt-bootstrap">
                    {{__('Deposit list')}}
                </div>
                <div class="panel-body">
                    <div class="row">
                        <div class="col-md-12 table-responsive">
                            <table id="users-table" class="table table-striped table-bordered table-hover responsive dataTable">
                                <thead>
                                    <tr>
                                        <th>{{__('Invoice number')}}</th>
                                        <th>{{__('Name')}}</th>
                                        <th>{{__('Nomine')}}</th>
                                        <th>{{__('Code')}}</th>
                                        <th>{{__('Information')}}</th>
                                        <th>{{__('Address')}}</th>
                                        <th>{{__('Tempo')}}</th>
                                        <th class="dont-print">{{__('Action')}}</th>
                                    </tr>
                                </thead>
                                <tbody id="row-data-table">
                                    @if($payments->count())
                                    @php($total=0)
                                    @foreach($payments as $pay)
                                    <tr>
                                        <td>{{$pay->transaction->code}}</td>
                                        <td>{{$pay->transaction->name}}</td>
                                        @php($total+=$pay->nomine)
                                        <td class="text-right">Rp {{number_format($pay->nomine,2,',','.')}}</td>
                                        <td>{{$pay->name}}</td>
                                        <td>{{$pay->information or '-'}}</td>
                                        <td>{{$pay->transaction->address}}</td>
                                        <td>{{$pay->transaction->tempo}}</td>
                                        <td class="dont-print">
                                            <button class="btn btn-danger delete-payment" data-id="{{$pay->id}}">{{__('Delete')}}</button>
                                        </td>
                                    </tr>
                                    @endforeach
                                    <tr>
                                        <td colspan="1"></td>
                                        <td><b>{{__('Total')}}</b></td>
                                        <td class="text-right"><b>Rp {{number_format($total,2,'.','.')}}</b></td>
                                        <td colspan="5"></td>
                                    </tr>
                                    @else
                                    <tr>
                                        <td colspan="10">{{__('Data not found')}}</td>
                                    </tr>
                                    @endif
                                </tbody>
                            </table>

                            {{ $payments->links() }}
                        </div>
                    </div>
                    <div class="row dont-print">
                        <div class="col-md-6" style="padding-top:2rem"> 
                        </div>
                        <div class="col-md-6 text-right" style="padding-top:2rem">
                            <button class="btn btn-primary add-new-payment">{{__('Add')}}</button>
                            <a href="/deposit/payment-list/cetak_pdf" target="_blank" class="btn btn-default">Print</a>
                            {{-- <button class="btn btn-default do-print" onclick="window.print()">{{__('Print')}}</button> --}}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="createModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title" id="createModalLabel">Tambah Data Pemasukan</h4>
            </div>
            <div class="modal-body" id="form-create">
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">{{__('Close')}}</button>
            </div>
        </div>
    </div>
</div>
@endsection

@push('style')
@endpush
@push('scripts')
@include('scripts.localedate')
<script>
$.ajaxSetup({
    headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    }
});

$('.add-new-income').click(function(e){
    e.preventDefault(e);
    $.get('/deposit/payment-list/create',function(data){
        $('#form-create').html(data);
        $('#createModal').modal('show');
    });
});

$('.add-new-payment').click(function(e){
    e.preventDefault(e);
    $.post('/deposit/payment-form',function(data){
        $('#form-create').html(data);
        $('#createModal').modal('show');
    });
})

$('.delete-payment').click(function(e){
    e.preventDefault(e);
    if (!confirm("{{__('Are you sure?')}}")) return;

    var id = $(this).data('id');
    $.post('/deposit/delete-payment/'+id,{"_method":"DELETE" }, function(){
        location.reload();
    }).fail(function(data) {
        var response = JSON.parse(data.responseText);
        if (typeof(response.errors) != "undefined") {
            var errorString = '<div class="alert alert-danger alert-dismissable">'+
                '<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button><ul>';
            $.each(response.errors, function(key, val){
                errorString += '<li>' + val[0] + '</li>';
            });
            errorString += '</ul></div>';
            $('.alert-frame-form').html(errorString);
            $("#createModal").animate({ scrollTop: 0 }, "slow");
        }
    });
});



$(document).ready(function() {
    var table = $('#users-table').DataTable( {
        
        scrollCollapse: true,
        paging:         true,
        ordering:       false,
        searching:      false,
        pageLength:     50,
        info:           false,
    } );
});
</script>
@endpush
