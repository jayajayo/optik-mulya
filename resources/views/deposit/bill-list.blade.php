@extends('layouts.admin')

@section('content')
@include('components.navbar', ['active' => 'deposit/bill-list'])

<div id="page-wrapper">
    @component('components.headerpage')
    <img src="{{asset('logo.jpeg')}}" class="show-print" alt="{{ config('app.name', 'Laravel') }}" height="35px"> <span class="dont-print">{{__('Deposit')}}</span>
    @endcomponent
    
    <div class="row">
        <div class="col-md-9" style="padding-bottom:10px">
        <h4>{{__('Date')}} : <span class="localize-date"></span></h4>
        </div>
        <div class="col-md-3 dont-print" style="padding-bottom:10px">
            <form class="form-inline" action="" >
                <div class="form-group input-group">
                <input type="text" name="keyword" class="form-control" placeholder="{{__('Search')}}" value="{{$keyword}}">
                    <span class="input-group-btn">
                        <button class="btn btn-primary" type="submit"><i class="fa fa-search"></i></button>
                    </span>
                </div>
            </form>                
        </div>
        <div class="col-md-12">
            @if(session()->has('status'))
                @component('components.alertsuccess')
                    {{ session()->get('status') }}
                @endcomponent
            @endif
            <div class="alert-bill-form">
            </div>
            <div class="panel panel-default">
                <div class="panel-heading dt-bootstrap">
                    {{__('Bill list')}}
                </div>
                <div class="panel-body">
                    <div class="row">
                        <div class="col-md-12 table-responsive">
                            <table id="users-table" class="table table-striped table-bordered table-hover dataTable">
                                <thead>
                                    <tr>
                                        <th>{{__('Invoice number')}}</th>
                                        <th>{{__('Name')}}</th>
                                        <th>{{__('Address')}}</th>
                                        <th>{{__('Tempo')}}</th>
                                        <th>{{__('Code')}}</th>
                                        <th>{{__('Nomine')}}</th>
                                        <th>{{__('Information')}}</th>
                                        <th class="dont-print">{{__('Action')}}</th>
                                    </tr>
                                </thead>
                                <tbody id="row-data-table">
                                    @if($payments->count())
                                    @php($total=0)
                                    @foreach($payments as $pay)
                                    <tr class="bill-list-edit" data-id="{{$pay->id}}">
                                        <td>{{$pay->transaction->code}}</td>
                                        <td>{{$pay->transaction->name}}</td>
                                        <td>{{$pay->transaction->address}}</td>
                                        <td>{{$pay->transaction->tempo}}</td>
                                        <td>{{$pay->name}}</td>
                                        @php($total+=$pay->nomine)
                                        <td class="text-right">Rp  {{number_format($pay->nomine,2,',','.')}}</td>
                                        <td>{{isset($pay->information) ? __($pay->information) : '-'}}</td>
                                        <td class="dont-print"><button class="btn btn-default edit-payment" data-id="{{$pay->id}}" data-payment="{{$pay->id}}">{{__('Edit')}}</button></td>
                                    </tr>
                                    @endforeach
                                    <tr>
                                        <td colspan="4"></td>
                                        <td><b>{{__('Total')}}</b></td>
                                        <td class="text-right">Rp {{number_format($total,2,'.','.')}}</td>
                                        <td colspan="2"></td>
                                    </tr>
                                    @else
                                    <tr>
                                        <td colspan="10">{{__('Data not found')}}</td>
                                    </tr>
                                    @endif
                                </tbody>
                            </table>
                            {{ $payments->links() }}
                        </div>
                    </div>
                    <div class="row dont-print">
                        <!--admin tambah data pemasukan-->
                        
                        <div class="col-md-6"> 

                        </div>
                        <div class="col-md-6 text-right">
                            <button class="btn btn-primary do-deposit">{{__('Deposit')}}</button>
                            <button class="btn btn-default do-print" onclick="window.print()">{{__('Print')}}</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@push('style')
@endpush
@push('scripts')
@include('scripts.localedate')
<script>
$.ajaxSetup({
    headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    }
});

function initEditBill() {
    $('.edit-payment').off();
    $('.edit-payment').click(function(e){
        e.preventDefault(e);
        var open = $('.editor-open');
        if (open.length) {
            $('.editor-open').addClass('has-error').find("input").focus();
            return;
        } 
        var id = $(this).data('id');
        var payment = $(this).data('payment');
        $.get('/deposit/form-edit-bill/'+payment, function(data){
            $('.bill-list-edit[data-id='+id+']').html(data);
        });
    });
}

$('.do-deposit').click(function(e){
    e.preventDefault(e);
    if(confirm("{{__('Are you sure?')}}")) {
        $.post('/deposit/process', function(e){
            window.location.replace('/deposit/payment-list');
        });
    }
});


$(document).ready(function(){
    initEditBill();
});
</script>
@endpush
