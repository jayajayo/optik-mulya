@extends('layouts.blank')
@section('content')
<div class="alert-income-form">
</div>
<form class="form-horizontal" action="{{ route("deposit.input-income") }}" id="create-income" role="form" method="POST">
    <input type="hidden" class="form-control" name="_method" value="POST" >
    @csrf
    <div class="form-group">
        <label class="control-label col-sm-2">{{__('Date')}}</label>
        <div class="col-sm-10">
        <input type="text" readonly="true" name="payment_date" class="form-control" value="{{ !empty(request()->payment_date) ? request()->payment_date : \Carbon\Carbon::today()->format('Y-m-d')}}">
        </div>
    </div>
    <div class="form-group">
        <label class="control-label col-sm-2">{{__('Code')}}</label>
        <div class="col-sm-10">
            <input type="text" readonly="true" name="{{__('code')}}" class="form-control" value="Lainnya">
        </div>
    </div>
    <div class="form-group">
        <label class="control-label col-sm-2">{{__('Information')}}</label>
        <div class="col-sm-10">
            <select class="form-control" name="{{__('information')}}" title="-- pilih status customer --">
                <option value="Cash">Cash</option>
                <option value="Transfer">Transfer</option>
            </select>
        </div>
    </div>
    <div class="form-group">
        <label class="control-label col-sm-2">{{__('Nomine')}}</label>
        <div class="col-sm-10">
            <input type="number" name="{{__('nomine')}}" class="form-control" min="0" step="100" value="0">
        </div>
    </div>
    <div class="form-group">
        <label class="control-label col-sm-2">{{__('Memo')}}</label>
        <div class="col-sm-10">
                <textarea class="form-control" rows="3" name="{{__('memo')}}" ></textarea>
        </div>
    </div>
    <div class="form-group">
        <label class="control-label col-sm-2">{{__('Action')}}</label>
        <div class="col-sm-10">
            <button type="submit" class="btn btn-primary">Submit</button>
            <button type="reset" class="btn btn-default">Reset</button>
        </div>
    </div>
</form>
<script>
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
    
    $('#create-income').submit(function(e){
        e.preventDefault(e);
        $('.alert-income-form').html("");
        $.post('/deposit/payment-list/input', $(this).serialize(), function(data){
            window.location.replace('/deposit/payment-list');
        }).fail(function(data) {
            var response = JSON.parse(data.responseText);
            if (typeof(response.errors) != "undefined") {
                var errorString = '<div class="alert alert-danger alert-dismissable">'+
                    '<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button><ul>';
                $.each(response.errors, function(key, val){
                    errorString += '<li>' + val[0] + '</li>';
                });
                errorString += '</ul></div>';
                $('.alert-income-form').html(errorString);
                $("#createModal").animate({ scrollTop: 0 }, "slow");
            }
        });
    });
</script>
@endsection