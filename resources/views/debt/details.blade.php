@extends('layouts.admin')

@section('content')
@include('components.navbar', ['active' => 'debt-list'])

<div id="page-wrapper">
    @component('components.headerpage')
        {{__('Detail Piutang')}}
    @endcomponent
    
    <div class="row">
        <div class="col-md-9" style="padding-bottom:10px">
        </div>
        <div class="col-md-3 text-right" style="padding-bottom:10px">
            <button class="btn btn-default" onclick="window.history.back();"><span class="fas fa-chevron-left"></span> kembali</button>        
        </div>
        <div class="col-md-12">
            @if(session()->has('status'))
                @component('components.alertsuccess')
                    {{ session()->get('status') }}
                @endcomponent
            @endif
            <div class="panel panel-default">
                <div class="panel-heading dt-bootstrap">
                    {{__('Detail Piutang')}} {{$transaction->code}}
                </div>
                <div class="panel-body">
                    <div class="row">
                        <div class="col-md-12 table-responsive">
                            <table id="users-table" class="table table-striped table-bordered table-hover dataTable">
                                <thead>
                                    <tr>
                                        <th><span class="nowrap-t">{{__('Order date')}} </span></th>
                                        <th><span class="nowrap-t">{{__('Code')}} </span></th>
                                        <th><span class="nowrap-t">{{__('Name')}} </span></th>
                                        <th><span class="nowrap-t">{{__('Address')}} </span></th>
                                        <th><span class="nowrap-t">{{__('Total')}} </span></th>
                                        <th><span class="nowrap-t">{{__('Tempo')}} </span></th>
                                        <th><span class="nowrap-t">{{__('Status')}} </span></th>
                                        @foreach($payments as $payment)
                                        <th><span class="nowrap-t">{{ucfirst($payment)}} </span></th>
                                        @endforeach
                                        <th><span class="nowrap-t">{{__('Remaining')}} </span></th>
                                    </tr>
                                </thead>
                                <tbody id="row-data-table">
                                    <tr>
                                        @php($remain=$transaction->total_payment)
                                        <td>{{$transaction->order_date->format('d-m-Y')}}</td>
                                        <td>{{$transaction->code}}</td>
                                        <td>{{$transaction->name}}</td>
                                        <td>{{$transaction->address}}</td>
                                        <td class="text-right"><span class="nowrap-t">Rp {{number_format($transaction->total_payment, 2, ',', '.')}}</span></td>
                                        <td><span class="nowrap-t">{{$transaction->tempo}}</span></td>
                                        <td>{{$transaction->status}}</td>
                                        @foreach($transaction->payments()->orderBy('id','asc')->get() as $payment)
                                        @if ($payment->status == "accepted")
                                        @php($remain -= $payment->nomine)
                                        @endif
                                        <td class="text-right"><span class="nowrap-t">{{($payment->nomine != 0 && $payment->status == "accepted") ? 'Rp '.number_format($payment->nomine, 2, ',', '.') : '-'}}</span></td>
                                        @endforeach
                                        <td class="text-right"><span class="nowrap-t">Rp {{number_format($remain, 2, ',', '.')}}</span></td>
                                    </tr>
                                    <tr>
                                        <td colspan="7">Petugas</td>
                                        @foreach($transaction->payments()->orderBy('id','asc')->get() as $payment)
                                        <td><span class="nowrap-t">{{$payment->recipient->name or '-'}}</span></td>
                                        @endforeach
                                        <td><span class="nowrap-t"></span></td>
                                    </tr>
                                    <tr>
                                        <td colspan="7">Tanggal</td>
                                        @foreach($transaction->payments()->orderBy('id','asc')->get() as $payment)
                                        <td><span class="nowrap-t">{{ isset($payment->payment_date) ? $payment->payment_date->format('d-m-Y') : '-'}}</span></td>
                                        @endforeach
                                        <td><span class="nowrap-t"></span></td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <div class="row">
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

