@extends('layouts.admin')

@section('content')
@include('components.navbar', ['active' => 'debt-list'])

<div id="page-wrapper">
    @component('components.headerpage')
        {{__('Debt list')}}
    @endcomponent
    
    <div class="row">
        <div class="col-md-9" style="padding-bottom:10px">
        </div>
        <div class="col-md-3" style="padding-bottom:10px">
            <form class="form-inline" action="" >
                <div class="form-group input-group">
                <input type="text" name="keyword" class="form-control" placeholder="{{__('Search')}}" value="{{request()->keyword}}">
                    <span class="input-group-btn">
                        <button class="btn btn-primary" type="submit"><i class="fa fa-search"></i></button>
                    </span>
                </div>
            </form>                
        </div>
        <div class="col-md-12">
            @if(session()->has('status'))
                @component('components.alertsuccess')
                    {{ session()->get('status') }}
                @endcomponent
            @endif
            <div class="panel panel-default">
                <div class="panel-heading dt-bootstrap">
                    {{__('Debt list')}}
                </div>
                <div class="panel-body">
                    <div class="row">
                        <div class="col-md-12 table-responsive">
                            <table id="users-table" class="table table-striped table-bordered table-hover dataTable">
                                <thead>
                                    <tr>
                                        <th><span class="nowrap-t">{{__('Order date')}} {!! \App\Libraries\SortHelper::header(request(), 'order_date') !!}</span></th>
                                        <th><span class="nowrap-t">{{__('Code')}} {!! \App\Libraries\SortHelper::header(request(), 'code') !!}</span></th>
                                        <th><span class="nowrap-t">{{__('Name')}} {!! \App\Libraries\SortHelper::header(request(), 'name') !!}</span></th>
                                        <th><span class="nowrap-t">{{__('Address')}} {!! \App\Libraries\SortHelper::header(request(), 'address') !!}</span></th>
                                        <th><span class="nowrap-t">{{__('Total')}} {!! \App\Libraries\SortHelper::header(request(), 'total_payment') !!}</span></th>
                                        <th><span class="nowrap-t">{{__('Tempo')}} {!! \App\Libraries\SortHelper::header(request(), 'tempo') !!}</span></th>
                                        @foreach($payments as $payment)
                                        <th><span class="nowrap-t">{{ucfirst($payment)}} {!! \App\Libraries\SortHelper::header(request(), $payment) !!}</span></th>
                                        @endforeach
                                        <th><span class="nowrap-t">{{__('Remaining')}} {!! \App\Libraries\SortHelper::header(request(), 'remain') !!}</span></th>
                                        <th><span class="nowrap-t">{{__('Status')}} {!! \App\Libraries\SortHelper::header(request(), 'status') !!}</span></th>
                                        <th>{{__('Action')}}</th>
                                    </tr>
                                </thead>
                                <tbody id="row-data-table">
                                    @if($transactions->count())
                                    @foreach($transactions as $transaction)
                                    <tr>
                                        <td>{{$transaction->order_date->format('d-m-Y')}}</td>
                                        <td>{{$transaction->code}}</td>
                                        <td>{{$transaction->name}}</td>
                                        <td>{{$transaction->address}}</td>
                                        <td class="text-right"><span class="nowrap-t">Rp {{number_format($transaction->total_payment, 2, ',', '.')}}</span></td>
                                        <td><span class="nowrap-t">{{$transaction->tempo}}</span></td>
                                        @foreach($payments as $payment)
                                        <td class="text-right"><span class="nowrap-t">{{($transaction->{$payment} != 0) ? 'Rp '.number_format($transaction->{$payment}, 2, ',', '.') : '-'}}</span></td>
                                        @endforeach
                                        <td class="text-right"><span class="nowrap-t">Rp {{number_format($transaction->remain, 2, ',', '.')}}</span></td>
                                        <td>{{$transaction->status}}</td>
                                        <td class="text-right"><a class="btn btn-default" href="{{url('debt-details/'.$transaction->id)}}">detail</a></td>
                                    </tr>
                                    @endforeach
                                    @else
                                    <tr>
                                        <td colspan="21">{{__('Data not found')}}</td>
                                    </tr>
                                    @endif
                                </tbody>
                            </table>
                            {{ $transactions->links() }}
                        </div>
                    </div>
                    <div class="row">
                        @can('debt_download')
                        <div class="col-md-12 text-right">
                            <button class="btn btn-default" data-toggle="modal" data-target="#downloadModal">Download</button>
                        </div>
                        @endcan
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="createModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title" id="createModalLabel">{{__('New Frame')}}</h4>
            </div>
            <div class="modal-body" id="form-create">
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">{{__('Close')}}</button>
            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="downloadModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title" id="downloadModallLabel">{{__('Download data piutang')}}</h4>
            </div>
            <div class="modal-body" id="download-modal">
                <div class="alert-download-form">
                </div>
                <form class="form-horizontal" id="transaction-download" role="form">
                    <div class="form-group">
                        <label class="control-label col-sm-2">{{__('Date')}}</label>
                        <div class="col-sm-10">
                            <input type="text" readonly="true" data-toggle="datepicker" class="form-control" name="startdate" value="{{ \Carbon\Carbon::today()->format('d-m-Y')}}">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-sm-2">{{__('To')}}</label>
                        <div class="col-sm-10">
                            <input type="text" readonly="true" data-toggle="datepicker" class="form-control" name="enddate" value="{{ \Carbon\Carbon::today()->format('d-m-Y')}}">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-sm-2">{{__('Action')}}</label>
                        <div class="col-sm-10">
                            <button type="submit" class="btn btn-primary">Download</button>
                        </div>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">{{__('Close')}}</button>
            </div>
        </div>
    </div>
</div>
@endsection

@push('style')
<link href="{{ asset('vendor/datepicker/datepicker.min.css') }}" rel="stylesheet">
<script src="{{ asset('vendor/datepicker/datepicker.min.js') }}"></script>
@endpush

@push('scripts')
<script>
$.ajaxSetup({
    headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    }
});

$('#transaction-download').submit(function (e){
    e.preventDefault(e);
    var formdata = $(this).serialize();
    $.post('/debt-request-download', formdata , function(data){
        window.location.href = '/debt-download?'+formdata;
    }).fail(function(data) {
        var response = data.responseJSON;
        if (typeof(response.errors) != "undefined") {
            var errorString = '<div class="alert alert-danger alert-dismissable">'+
                '<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button><ul>';
            $.each(response.errors, function(key, val){
                errorString += '<li>' + val[0] + '</li>';
            });
            errorString += '</ul></div>';
            $('.alert-download-form').html(errorString);
            $("#downloadModal").animate({ scrollTop: 0 }, "slow");
        }
    });
});
</script>
@include('scripts.datepicker')
@endpush
