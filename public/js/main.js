var App = {
    
    init: function() {
        App.removeAlertSuccess();
        App.inactivityTime();
    },

    removeAlertSuccess: function() {
        var el = $('.alert.alert-success.alert-dismissable');
        if (!el.length) return;
        setTimeout(function() {
            el.slideUp('slow', function(){ el.remove(); });
        }, 2000);
    },

    inactivityTime: function () {
        var t;
        window.onload = resetTimer;
        document.onmousedown = resetTimer;
        document.onmousemove = resetTimer;
        document.onclick = resetTimer;     // touchpad clicks
        document.onscroll = resetTimer;    // scrolling with arrow keys
        document.onkeypress = resetTimer;
    
        function logout() {
            alert("session expired.");
            document.getElementById('logout-form').submit();
        }
    
        function resetTimer() {
            clearTimeout(t);
            t = setTimeout(logout, 3000000)
            // 1000 milisec = 1 sec
        }
    }

};

$(document).ready(function(){
    App.init();
});
