<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTransactionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('transactions', function (Blueprint $table) {
            $table->increments('id');
            $table->string('code')->nullable();
            $table->string('sph_r')->nullable();
            $table->string('sph_l')->nullable();
            $table->string('cyl_r')->nullable();
            $table->string('cyl_l')->nullable();
            $table->string('axis_r')->nullable();
            $table->string('axis_l')->nullable();
            $table->string('add_r')->nullable();
            $table->string('add_l')->nullable();
            $table->string('pd')->nullable();
            $table->string('name');
            $table->text('address');
            $table->string('phone');
            $table->string('lens')->nullable();
            $table->text('memo')->nullable();
            $table->unsignedInteger('frame_id');
            $table->date('order_date');
            $table->date('finish_date');
            $table->date('charge_date');
            $table->unsignedDecimal('frame_price',19,2);
            $table->unsignedDecimal('lens_price',19,2);
            $table->unsignedDecimal('rx_price',19,2);
            $table->unsignedDecimal('down_payment',19,2);
            $table->unsignedDecimal('debt',19,2);
            $table->unsignedDecimal('total_payment',19,2);
            $table->unsignedDecimal('finish_payment',19,2);
            $table->boolean('order_status')->default(false);
            $table->boolean('faset_status')->default(false);
            $table->boolean('packing_status')->default(false);
            $table->boolean('paid_off')->default(false);
            $table->timestamps();
            $table->softDeletes();
        });

        Schema::create('payments', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('transaction_id');
            $table->foreign('transaction_id')->references('id')->on('transactions')->onDelete('cascade');
            $table->unsignedInteger('user_id');
            $table->unsignedInteger('recipient_id')->nullable();
            $table->string('name');
            $table->date('charge_date');
            $table->decimal('nomine',19,2)->nullable()->default(0);
            $table->date('payment_date')->nullable();
            $table->enum('information',['cash','bca','mandiri','bank jateng'])->nullable();
            $table->enum('status',['unpaid','paid','accepted']);
            $table->text('memo')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('payments', function (Blueprint $table) {
            $table->dropForeign('payments_transaction_id_foreign');
        });

        Schema::table('payments', function (Blueprint $table) {
            $table->enum('information',['cash','bca','mandiri','bank jateng'])->change();
        });

        Schema::dropIfExists('payments');
        Schema::dropIfExists('transactions');
    }
}
