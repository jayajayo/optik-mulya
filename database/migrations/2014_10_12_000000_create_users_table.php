<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('team_id')->nullable();
            $table->string('name');
            $table->string('nickname');
            $table->string('username')->unique();
            $table->string('email')->unique();
            $table->string('password');
            $table->text('address');
            $table->string('phone');       
            $table->boolean('status')->comment('0=inactive , 1=active');          
            $table->rememberToken();
            $table->timestamps();
            $table->softDeletes();
            $table->foreign('team_id')
                ->references('id')->on('teams')
                ->onDelete('cascade');
        });

        DB::table('users')->insert([
            'team_id' => 5,
            'name' => 'super admin',
            'nickname' => 'sudo',
            'username' => 'root',
            'email' => 'root@optik.com',
            'password' => bcrypt('@superadmin2018'),
            'address' => 'root',
            'phone' => 'root',
            'status' => 1,
        ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
