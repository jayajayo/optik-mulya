<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class FrameMigration extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('merk_frames', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->timestamps();
            $table->softDeletes();
        });

        Schema::create('frames', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('merk_frame_id');
            $table->string('name');
            $table->unsignedInteger('stock');
            $table->timestamps();
            $table->softDeletes();
        });

        Schema::create('frame_team', function (Blueprint $table) {
            $table->unsignedInteger('frame_id')->index();
            $table->foreign('frame_id')->references('id')->on('frames')->onDelete('cascade');
            $table->unsignedInteger('team_id')->index();
            $table->foreign('team_id')->references('id')->on('teams')->onDelete('cascade');
            $table->unsignedInteger('quantity');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('frame_team', function (Blueprint $table) {
            $table->dropForeign('frame_team_frame_id_foreign');
            $table->dropForeign('frame_team_team_id_foreign');
        });

        Schema::dropIfExists('merk_frames');
        Schema::dropIfExists('frames');
        Schema::dropIfExists('frame_team');
    }
}
