<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTeamsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('teams', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->string('code');
            $table->unsignedInteger('order');
            $table->timestamps();
        });

        $defaultTeams = [
            [
                'name' => 'A',
                'code' => 'A',
                'order' => 1,
            ],
            [
                'name' => 'B',
                'code' => 'B',
                'order' => 2,
            ],
            [
                'name' => 'C',
                'code' => 'C',
                'order' => 3,
            ],
            [
                'name' => 'D',
                'code' => 'D',
                'order' => 4,
            ],
            [
                'name' => 'Toko',
                'code' => 'T',
                'order' => 5,
            ]
        ];
        
        foreach ($defaultTeams as $team) {
            DB::table('teams')->insert($team);
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('teams');
    }
}
