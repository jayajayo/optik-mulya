<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateComplaintsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('complaints', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('creator_id')->nullable();
            $table->date('date');
            $table->string('name');
            $table->string('invoice_number');
            $table->text('address');
            $table->string('phone');
            $table->text('complaint');
            $table->unsignedInteger('user_id')->nullable();
            $table->text('action')->nullable();
            $table->boolean('waiting_status')->nullable()->default(0);
            $table->boolean('finish_status')->nullable()->dafault(0);
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('complaints');
    }
}
